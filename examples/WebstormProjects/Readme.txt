The Bootstrap_Highcharts project is an example using Highcharts to plot
Battery voltages and Battery Currents (4 y axises)

It uses jQuery to read a locally stored csv file containing plotting data. The buttons
are made using Bootstrap.

In order to render the plots this currently needs to be run from inside JetBrains Webstorm.

