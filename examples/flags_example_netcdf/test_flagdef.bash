#!/bin/bash

# hardcode a data file for the time being
curl -u 'username:password' -O 'https://cgoms.coas.oregonstate.edu/cg_proc/ce02shsm/D00004/buoy/pwrsys/20160926.pwrsys.json'
# Find any PSC flags.  Quote values
psc_flags=$(curl -u 'username:password' 'https://cgoms.coas.oregonstate.edu/oms-bin/view_syslog?pid=ce02shsm&deploy=D00004&rtf=fault_tables' | sed -n -e '/PSC Fault/,/^$/{/^ 0x/s/^ \(0x[^ ]*\) \(.*\)$/\1,"\2"/p;/#/p}')

# find "Begin flag/End flag" blocks and use these to blocks to separate
# the flag sections.
# separate files
for n in {1..3}; do
   echo "$psc_flags" | sed -n -e "/Begin flag${n}/,/End flag${n}/{/^0x/p}" > "error_flag${n}_defs.csv"
done; 

# generate the NetCDF file from the files created
python flag_generate.py
