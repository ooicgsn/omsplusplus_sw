import json
import pandas as pd
import xarray

# read datafile json
with open('./20160926.pwrsys.json') as json_data:
    import ipdb; ipdb.set_trace()
    d = json.load(json_data)

# get error flag columns and convert from hex string to int
error_data = {k: [int(s, 16) for s in d[k]] for k in
                         ('error_flag1', 'error_flag2', 'error_flag3')}

# convert the time to a usable index format for pandas
time_idx = pd.to_datetime(d['dcl_date_time_string'])
time_idx.name = 'time'


# create the Pandas DataFrame and then load it into xarray, as it can store
# metadata and then export to NetCDF
error_df = pd.DataFrame(error_data, time_idx)
ds = xarray.Dataset.from_dataframe(error_df)

# throw standard name onto time
ds.variables['time'].attrs['standard_name'] = 'time'

# get only error
error_names = (varname for varname in ds.variables.keys() if
                     varname.startswith('error_flag'))

def map_hex(ser):
    """
    Takes a series of hexadecimal strings and converts it to a comma separated
    string of base-10 integers
    """
    return ', '.join(ser.apply(lambda h: str(int(h, 16))))


for err_name in error_names:
    # get each variable corresponding to the error flag
    err_var = ds.variables[err_name]
    # read corresponding flag file
    flag_defs = pd.read_csv("{}_defs.csv".format(err_name), header=None)
    # return string of flag masks
    err_var.attrs['flag_masks'] = map_hex(flag_defs[0])
    # munge into format that will be acceptable for netcdf format
    # lowercase name with underscores since flag_meanings are separated by a
    # space
    meanings = ' '.join(flag_defs[1].str.lower().str.replace(' ', '_'))
    err_var.attrs['flag_meanings'] = meanings

# write out Dataset to NetCDF file
ds.to_netcdf('error_flag_out_example.nc')
