Author: Joe Padula
Date: 11/10/2016

This directory contains an example of a Python parser that parses
a cp03issm syslog file for vMain voltage and battery currents for bat1 - bat4.

It also contains the syslog file that is parsed and both csv and json output.

mi/dataset/driver/cg_cpm_eng/cg_eng_telemetered_psc_driver.py
mi/dataset/driver/cg_cpm_eng/resource/cp03issm_20160621.syslog.log
mi/dataset/parser/cg_eng_psc.py
output/cg_eng_c_power_sys.csv
output/cg_eng_c_power_sys.json

To run the example, from mi-dataset:
$ python -m utils.parse_file --fmt json mi/dataset/driver/cg_cpm_eng/cg_eng_telemetered_psc_driver.py mi/dataset/driver/cg_cpm_eng/resource/cp03issm_20160621.syslog.log

Note: for csv output, change "--fmt json" to "--fmt csv"


