#!/usr/bin/env python

"""
@package mi.dataset.driver.cg_cpm_eng
@file mi-dataset/mi/dataset/driver/cg_cpm_eng/cg_eng_telemetered_psc_driver.py
@author Joe Padula
@brief Driver for the cg_cpm_eng instrument

Release notes:

Initial Release
"""

from mi.dataset.dataset_parser import DataSetDriverConfigKeys
from mi.dataset.dataset_driver import SimpleDatasetDriver
from mi.dataset.parser.cg_eng_psc import CgEngPscParser, CgDclEngDclParticleClassTypes, \
    CgDclEngDclMsgCountsTelemeteredDataParticle, \
    CgDclEngDclCpuUptimeTelemeteredDataParticle, \
    CgDclEngDclCPowerSysTelemeteredDataParticle
    # CgDclEngDclErrorTelemeteredDataParticle, \
    # CgDclEngDclGpsTelemeteredDataParticle, \
    # CgDclEngDclPpsTelemeteredDataParticle, \
    # CgDclEngDclSupervTelemeteredDataParticle, \
    # CgDclEngDclDlogMgrTelemeteredDataParticle, \
    # CgDclEngDclDlogStatusTelemeteredDataParticle, \
    # CgDclEngDclStatusTelemeteredDataParticle, \
    # CgDclEngDclDlogAarmTelemeteredDataParticle
from mi.core.versioning import version


@version("15.6.0")
def parse(basePythonCodePath, sourceFilePath, particleDataHdlrObj):
    """
    This is the method called by Uframe
    :param basePythonCodePath This is the file system location of mi-dataset
    :param sourceFilePath This is the full path and filename of the file to be parsed
    :param particleDataHdlrObj Java Object to consume the output of the parser
    :return particleDataHdlrObj
    """

    with open(sourceFilePath, 'rb') as stream_handle:

        driver = CgEngBatteryCurrentTelemeteredDriver(basePythonCodePath, stream_handle, particleDataHdlrObj)
        driver.processFileStream()

    return particleDataHdlrObj


class CgEngBatteryCurrentTelemeteredDriver(SimpleDatasetDriver):
    """
    Derived flntu_x_mmp_cds driver class
    All this needs to do is create a concrete _build_parser method
    """

    def _build_parser(self, stream_handle):

        parser_config = {
            DataSetDriverConfigKeys.PARTICLE_MODULE: 'mi.dataset.parser.cg_eng_psc',
            DataSetDriverConfigKeys.PARTICLE_CLASS: None,
            DataSetDriverConfigKeys.PARTICLE_CLASSES_DICT: {
                CgDclEngDclParticleClassTypes.MSG_COUNTS_PARTICLE_CLASS:
                    CgDclEngDclMsgCountsTelemeteredDataParticle,
                CgDclEngDclParticleClassTypes.CPU_UPTIME_PARTICLE_CLASS:
                    CgDclEngDclCpuUptimeTelemeteredDataParticle,
                CgDclEngDclParticleClassTypes.C_POWER_SYS_PARTICLE_CLASS:
                    CgDclEngDclCPowerSysTelemeteredDataParticle,
                # CgDclEngDclParticleClassTypes.ERROR_PARTICLE_CLASS:
                #      CgDclEngDclErrorTelemeteredDataParticle,
                # CgDclEngDclParticleClassTypes.GPS_PARTICLE_CLASS:
                #  CgDclEngDclGpsTelemeteredDataParticle,
                # CgDclEngDclParticleClassTypes.PPS_PARTICLE_CLASS:
                #  CgDclEngDclPpsTelemeteredDataParticle,
                # CgDclEngDclParticleClassTypes.SUPERV_PARTICLE_CLASS:
                #      CgDclEngDclSupervTelemeteredDataParticle,
                #  CgDclEngDclParticleClassTypes.DLOG_MGR_PARTICLE_CLASS:
                #     CgDclEngDclDlogMgrTelemeteredDataParticle,
                #  CgDclEngDclParticleClassTypes.DLOG_STATUS_PARTICLE_CLASS:
                #     CgDclEngDclDlogStatusTelemeteredDataParticle,
                #  CgDclEngDclParticleClassTypes.STATUS_PARTICLE_CLASS:
                #      CgDclEngDclStatusTelemeteredDataParticle,
                #  CgDclEngDclParticleClassTypes.DLOG_AARM_PARTICLE_CLASS:
                #      CgDclEngDclDlogAarmTelemeteredDataParticle,
            }

        }

        parser = CgEngPscParser(parser_config,
                                   stream_handle,
                                   self._exception_callback)

        return parser


