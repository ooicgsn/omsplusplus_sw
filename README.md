# README #

### What is this repository for? ###

* This is the OMS++ software development repo. It should be used for sharing code examples (in 'examples'), sharing documentation & directions (in 'wiki'), and developing and releasing code (in 'code' and 'tests').
* This repo is NOT for example datasets. Anything but the smallest data file examples should be placed in the omsplusplus_data repo.

### How do I get set up? ###

* Clone both omsplusplus_sw and omsplusplus_data (if needed) under a common directory on your computer.
* [Configuration]
* [Dependencies]
* [How to run tests]
* [Deployment instructions]

### Contribution guidelines ###

* Writing tests - Please test your work! Post any pieces of testing code in the 'tests' folder, along with instructions.
* Code review - All development should be done in a branch and tested (if necessary) before submitting a Pull Request into Master (or other branch). Everyone in the OMS++ Dev group has "merge via pull request" permission. Use it carefully. Please include at least one Reviewer on your Pull Request, so someone is notified that it is ready to be reviewed for merging. Code and Tests must be fully functional before merging into Master. Only deployment-ready code should be merged into to master.
* Other guidelines - Please use 1 Tab = 4 Spaces

### Who do I talk to? ###

* For questions, please contact spetillo@whoi.edu
