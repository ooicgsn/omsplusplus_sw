# OMS++ Project and Code Structure

### Introduction ###

Coming Soon...

### Contact Information

The best person to contact varies depending on the area of the application you have questions on

* For general inquiries, contact  [Stephanie Petillo](mailto:spetillo@whoi.edu) 
* For data ingestion or ERDDAP questions, contact [Joe Futrelle](mailto:jfutrelle@whoi.edu)
* For questions on the UI, contact [Paul Duffy](mailto:Paul.Duffy@rpsgroup.com) or [Mike Chagnon](mailto:Michael.Chagnon@rpsgroup.com)

### Architecture

The project is composed of various separate, but related, systems. This includes:

* Parsers & Processors, a collection of Python scripts to parse through the raw data collected by instruments at sea and process that data (applying calibrations where appropriate) into an ERDDAP-readable NetCDF format
* ERDDAP, an application which serves all of the collected mooring/instrument data, as well as providing an interface to interact with that data
* A PostgreSQL database, which backs the UI and all data for alert triggers, plots, etc.
* Django, the framework used to generate the website
* Redmine, an externally hosted issue/tracking application
* Sage, externally hosted asset management system (coming soon)

Below is the latest architecture diagram that includes all components of the OMS++ system

![Architecture Diagram](architecture.png)

### Current Capabilities

* Moorings and Instruments (Assets)
** The hierarchy of a mooring an its instruments can be read and processed from the mooring's platform configuration file
** All assets can have triggers and plots plots
** There is a detailed overview page or each asset, and for each mooring, a full display of the instruments on that mooring and the status of each asset
** There is an array status page to show the current status of all moorings within an array

* User Management
** Users can be created and modified from within the application
** Roles can be assigned to limit the amount of access a particular user has

* Integrations
** A Redmine integration to allow the creation of support tickets within OMS++

* Plotting
** Plots are generated using a third-party library (Highcharts)
** Currently limited to only time on the x axis
** Plots can be cloned, along with any related L3 variables
** Both scatter and line plots are supported
** Up to 4 data streams can be put on a single plot
** The time range for a plot can be selected by the user

* User Customizations 
** Plot pages containing a list of predefined plots can be created by users for any asset (mooring or instrument) in the system
** Users can share plot pages with other users
** Users can customize their defaulting start page

* Alerts and Alarms (Triggers)
** Triggers can be cloned, along with any related L3 variables
** Notes can be entered for open alerts, and that data is timestamped
** Alerts can be resolved from within the application
** The hot list provides a list of currently open alerts
** Open alerts affect the overall status

* L3 (Custom) Variables
** Custom variables can be created from any variables already in ERDDAP
** Can be used within expressions for triggers and plots just like any other ERDDAP variable

* System Overview
** Provides an overview of each array of moorings
** Currently determines percent functional based on open alerts

* Config Tool
** Allows for the parsing of existing platform configuration files
** Provides validation of user input
** Generates a properly formatted platform configuration file based on use input

### In Development

* An integration with Sage to collect asset management data within the application
* Standalone laptop version of the application to troubleshoot and diagnose issues while at sea
* Additional reporting on the system overview including telemetry, data file and power system statistics
* Further enhancements to the configuration parser tool to assist with creation of and deployment of new moorings and configuration changes
* Support for additional moorings/assets, including SSMs, CSPPs and full support for gliders
* Increasing flexibility on data used for the x axis of plots
* Increasing the number of data streams that can be put on each plot

### Management Commands
The project contains a number of custom management commands (using the Django framework) to perform actions that are not directly available to users of the website. Any commands that are specific to the project are under the `core` directory

#### List All Available commands
Keep in mind that this list shows all commands, many of which are provided by Django by default and not necessarily used by the application

```
python manage.py --help
```


#### Update Assets
This will update all assets based on the configuration files included in `external_scripts/ConfigLogs`

```
python manage.py update_assets
```


#### Update Permissions
This will update all user/group permissions from the configuration file located in the `conf` directory

```
python manage.py update_permissions
```


#### Update Gliders
This will load all gliders from the configuration file located in the `conf` directory

```
python manage.py load_gliders
```


#### Run Alerts
Manually process all active triggers. This does exactly what the cron job does when processing alerts

```
python manage.py run_alerts
```


#### Update database migrations
Run any newly created database migration scripts

```
python manage.py migrate
```


#### Rebuild the database
_CAUTION:_ This will rebuild the PostgreSQL database from scratch and will destroy all existing data.  
The username and password flags can be omitted if the ERDDAP instance does not require them

```
python manage.py rebuild_database -es "ERDDAP-server-url" -eu Username -ep Password
```

#### Cron/scheduled jobs

On production or staging servers, it is recommended to periodically run jobs
scheduled through cron or similar means.  Below is an example cron job taken
from the OOI server on 2018-04-19:

```
# Field order:
# minute hour dayofmonth month dayofweek command
# specifications
# asterisk/star/* essentially means "every".  If followed by a slash, it can
# indicate an interval to process, i.e.
# */15 * * * * some_command
# means run "some_command" every 15 minutes
# See `man 5 crontab` on Unix systems for more information on time

# get the latest update time for active deployments on assets at 00:00 UTC
# every day
0 * * * * /home/ooiuser/.virtualenvs/omsplusplus_sw/bin/python3 /home/ooiuser/code/omsplusplus_sw/code/webapp/manage.py get_latest_updates > /dev/null

# run the alerts/triggers every 15 minutes, along with updating asset positions and gliders
*/15 * * * * /home/ooiuser/.virtualenvs/omsplusplus_sw/bin/python3 /home/ooiuser/code/omsplusplus_sw/code/webapp/manage.py run_alerts > /dev/null
*/15 * * * * /home/ooiuser/.virtualenvs/omsplusplus_sw/bin/python3 /home/ooiuser/code/omsplusplus_sw/code/webapp/manage.py get_latest_positions > /dev/null
*/15 * * * * /home/ooiuser/.virtualenvs/omsplusplus_sw/bin/python3 /home/ooiuser/code/omsplusplus_sw/code/webapp/manage.py get_latest_glider_updates > /dev/null

# run rsync (parsing and processing now done separately)
5,35 * * * * /home/ooiuser/code/omsplusplus_sw/code/data_ingest/cron_runscript_wrapper.sh
15,45 * * * * /home/ooiuser/code/omsplusplus_sw/code/data_ingest/pp_runscript.sh > /dev/null

# Initalize the most recent telemetered deployment of each glider for NetCDF processing
0 0,6,12,18 * * * /home/ooiuser/code/cgsn-gliders/scripts/cron_init_latest_deployments.sh;

# Process the most recent deployment of each glider to NetCDFs - johnkerfoot@gmail.com
# 2018-01-30: johnkerfoot@gmail.com - set to 22:40 for testing
40 22 * * * /home/ooiuser/code/cgsn-gliders/scripts/cron_process_latest_deployments.sh;

# Write ERDDAP xml files for newly processed deployments - johnkerfoot@gmail.com
# 2018-01-30: johnkerfoot@gmail.com - set to 22:55 for testing
55 22 * * * /home/ooiuser/code/cgsn-gliders/scripts/cron_process_latest_deployments_to_erddap.sh;
```

### Code/Folder Structure


#### /code/data_ingest
Scipts used to automate the process of rsync-ing new data to the OMS++ server, and then parsing/processing that data


#### /code/webapp
The root directory of the Django application that handles the UI

	
#### /code/webapp/conf
Configuration files for some of the different aspects of the application:

| File | Description |
| --- | ----------- |
| `alerts.json` | Configuration file to load in initial, default alerts. It can also be used by advanced users to quickly load in a large amount of triggers  |
| `gliders.json` | List of all gliders and deployments to link to. Unlike moorings, there is no platform.cfg file to import for gliders |
| `permissions.json` | List of user permissions based on groups. The file can be modified and then one of the management scripts can be run to update permissions in the database |
| `redmine.json` | Configuration of the Redmine integration. This includes associated projets, trackers and data fields. This also controls which project/tracker gets which fields |

	
#### /code/webapp/core
While Django supports the ability to have multiple apps within the same project, the approach used for OMS++ was to use a single app because of how intertwined each piece of the UI is. This also assists with database migrations, as many of the Postgres functions touch multiple parts of the project

The one app, called `core`, is broken out into several folders based on the standard Django structure:
  
| Folder | Description |
| ----              | ----------- |
| `forms` | Code used to generate user input forms, such as to create users, triggers, etc. |
| `management/commands ` | Custom commands that can be run with `python mange.py [command]`. Examples of these commands are ones to update user permissions or update assets based on new/modified platform configuration files |
| `middleware` | Code that gets added to the request pipeline for all page requests. This is currently only used to require authentication on all pages |
| `migrations` | Database migration scripts, including ones automatically generated by Django, as well as custom scripts | 
| `models` | Definition of the basic enties used in the application. These are then used by Django to generate the appropriate database schema in Postgres | 
| `templatetags` | Custom controls that have been separated out into their own feature set. This is due to it either being included on multiple pages, or to keep some of the more complex logic self contained | 
| `tests` | Unit tests | 
| `views` | Scripts used to take a page request from a user, process it, and output the proper template file from `/templates` | 
 
The root folder (`/core`) also contains some individual scripts that do not fit into the structure outlined above. Many of these are common routines. This is also a place for code that requires multiple, sometimes conflicting, dependencies  
	
			
#### /code/webapp/deploy
Sample configuration files that can be used to set up the application in a production environment

	
#### /code/webapp/external_scripts
Contains scripts and configuration files used to initially create the Postgres database and load in assets, plots and alerts. Some of these files are no longer used. Also included is the ConfigLogs folder, which contains the platform config files currently being loaded when updating the asset hierarchy
	
					
#### /code/webapp/media
Folder for all media files that are not directly related the layout of the UI. The primary use of this folder is in the `drawings` subfolder, which houses images for the various moorings included in the application


#### /code/webapp/static/admin
Contains all necessary CSS, Javascript, fonts and images needed by Django to render the admin area. These files are automatically generated and added to the project by Django


#### /code/webapp/static/css
Stylesheet files used by the UI

| File | Description |
| --- | ----------- |
| style.css | Application specific layout customizations |
| theme.css | Base CSS file used by the underlying Bootstrap theme the UI is built off of |
	

#### /code/webapp/static/images
Contains images used throughout the application, including a folder of the logos for each of the organizations that are involved with development 


#### /code/webapp/static/js
Javascript files used by the UI

| File | Description |
| --- | ----------- |
| site.js | Application specific Javascript code to handle client events within the UI |
| theme.js | Base Javascript file used by the underlying Bootstrap theme the UI is built off of |
		

#### /code/webapp/static/vendor
Folders for all third-party library files used in the application. This includes necessary CSS, Javascript and image files

| Folder | Description |
| --- | ----------- |
| `bootstrap` | Files needed by Bootstrap, which is the framework the UI is built on |
| `bootstrap-multiselect` | Files needed by the custom dropdown used to select variables for plots  |
| `datatables` | Third-party library that handles rending of the grids and allows for easy sorting and paging |
| `font-awesome` | Third-party library used to generate icons |
| `highcharts` | Third-party library used to render all plots |
| `jquery` | Common Javascript library that helps with client functionality |
| `jquery-ui` | Third-party library to handle custom UI controls, like date pickers and modal dialog windows |
| `jquery-validation` | Third-party library that handles validation of user input |
| `moment` | Third-party library to make handling date and time specific variables easier |
| `pnotify` | Used to display popup notifications to users based on their actions. For example, when saving data |


#### /code/webapp/tasks
Scripts to process the alert triggers in the application in order to generate alerts and alarms. This includes text/email notifications and updating the status of the moorings and their instruments 


#### /code/webapp/templates
HTML template files that are used to generate the pages used in the application. When a page request comes into the application, its handled by a view which will then render the page using one of the templates listed here

| Folder | Description |
| --- | ----------- |
| `account` | Templates for user account information |
| `alert` | Templates used for alerts and triggers |
| `asset` | Templates related to moorings and instruments |
| `config` | Templates used by the configuration parser tool to load, parse, and save platform configuration files for each mooring |
| `core/templatetags` | Templates for the common controls used on multiple pages, like the asset navigation area |
| `dashboard` | Templates for high level pages not tied to a particular mooring. E.g., the hot list or system overview |
| `plots` | Templates to render, administer and clone plots. Also included is the template for emails related to plot page sharing |
| `variables` | Templates for managing variables, currently just for managing L3 (custom) variables |

Additionally, the root of this folder contains a few more highlevel files

| Folder | Description |
| --- | ----------- |
| `base.html` | The master layout for the entire site. All other templates extend this template and it contains main UI areas such as the left sidebar |
| `public.html` | Similar to the base layout, except this is the view used for the few pages that do not require the user to be authenticated (such as the login screen) |
	

#### /code/webapp/templates/variable
HTML template files related to variables. Currently this is limited to pages used to manage L3 (custom) variables


#### /code/webapp/webapp
Django project settings

