# OMS++ Release and Deployment Instructions

### Introduction ###

These instructions are based on the current OMS++ BitBucket repository, and deployments to the current WHOI production instance of the application. Most of this information is generic enough where it will apply to all installations, but some of the operating system dependent information will need to be changed based on specific server configuration settings.
 
The process described here will generate a new release based on the current code changes within the development branch, and follow it through deployment to the production server and updating the master branch in the BitBucket repository.


### Create a release branch

First, make sure you have the latest code for both the develop and master branches locally

```
git checkout develop
git pull
git checkout master
git pull
```

Now create the release branch, and push it to BitBucket.

```
git checkout -b release-x.xx
git merge develop
git push -u origin release-x.xx
```

All changes to the master branch will require a pull requested in order to be merged. However, it's also helpful to create a pull request from the release branch to master now, since it will show you any code changes that you can review before deployment. This must be done within the BitBucket website

### Create backups

SSH into the server and switch to the user that controls the code

```
ssh [username]@ooivm1.whoi.net
sudo su - ooiuser
```

Switch to the code directory (which is specific to the WHOI production server) and backup the code

```
cd code
tar czvf backups/omsplusplus-YYYYMMDD.tgz omsplusplus_sw/
```

Now backup the database. The easiest way to do this is just to 'su' into the postgres user account

```
sudo su - postgres
pg_dump omsplusplus > backups/omsplusplus-YYYYMMDD.sql
exit
```

### Update the code on the server

The website needs to be stopped in order to pull down the code (and more importantly, restarted once the code has been refreshed)

```
systemctl stop omsplusplus
```

Stash any existing changes that are on the server, and pull down the latest release (that was just created) and switch to it
```
cd omsplusplus_sw/
git stash
git fetch origin release-x.xx
git checkout release-x.xx
```

### Run update processes

After updating the code, there are sometimes additional processes that need to be completed. This is going to be highly dependent on the changes that were made, but at a minimum, its a good idea to apply any database migrations and update any third-party libraries. To do so, you must first activate the same virtual environment used to run the website

```
cd code/webapp/
source ~/.virtualenvs/omsplusplus_sw/bin/activate
pip install -r requirements.txt 
./manage.py migrate
```

If you get a warning about "content types are stale and need to be deleted", you can just press enter to skip it

If there are any cron jobs that need to be added or modified, you can edit the cron job list (which are associated with the ooiuser account) with
```
crontab -e
```

### Start the site

Before starting the site, be sure to pop any of the stashed code changes. This may require you to fix any conflicts depending on what the changes to the development code are compared to what was on the server (but it should be minimal)

```
git stash pop
```

Now start the site

```
sudo systemctl start omsplusplus 
```

### Test

At this point, the latest code is now running on the server and testing should be completed prior to completing the remaining tasks.

If any problems are found that require hotfixes, update them locally, test, and push changes to the *release* branch that was created. Once tested, push those changes up to the server by running the following commands:

```
git stash
git pull
git stash pop
```


### Update the master branch

As of right now, the website is running off of the release branch rather than master. Now that testing has been finished, we can merge the latest release into the master branch and change the website to run off of that. On the BitBucket website, do the following:

* Merge the pull request that was created earlier to get the release changes into the master branch
* Create and merge a pull request from the release branch to the development branch. This is mainly to get any hotfixes back into the development branch, but it should be done regardless of if there are any actual hotfixes

This step is necessary as the BitBucket repository requires a pull request in order to merge changes into the master branch

### Switch production website to master branch

The final step is to update the master branch code on the production server and use it for the website instead of the release branch

```
sudo systemctl stop omsplusplus
git stash
git checkout master
git pull origin master
git stash pop
sudo systemctl start omsplusplus
```

### Final notes

The git process on the production server is based on a remote origin that includes the users name (we do not have a generic account for this). If this is your first time deploying changes, you will need to add a new remote to the list, and make sure to modify any git comments to specify the correct origin

There is a command in the Django application to rebuild the database from scratch (./manage.py rebuild_database). This should *NOT* be run on production as it will completely drop and recreate the database. This means all alerts, triggers, plots, user accounts will be wiped.

Generally, Apache does not need to be restarted as part of the process as it's just serving as a proxy to the Django application. But if you need to restart it for some reason, run the following:

```
sudo service apache2 restart
```
