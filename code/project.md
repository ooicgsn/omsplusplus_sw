Code/Data_ingest
	Scripts and text files that are used in entering data
Code/webapp
	Where the Django application is stored
Code/webapp/conf
	Json files for configuration values such as permissions and alerts
Code/webapp/core
	Where the core workings of the Django application are stored.
	Code/webapp/core/forms
		Python files for the Django forms that are used in the application
	Code/webapp/core/management/commands
		Python files for commands that manage the database and data
	Code/webapp/core/middleware
		Python files that add new middleware to the applications. Such as login middleware
	Code/webapp/core/migrations
		Python files that handle database/model migrations
	Code/webapp/core/models
		Python files that handle the Django models for the application
	Code/webapp/core/templatetags
		Folder for python files of tags used in html templates within the application
	Code/webapp/core/tests
		Unit tests for validations
	Code/webapp/core/views
		Folder containing views that run the backend of the application
Code/webapp/deploy
	Files that assist in database migrations
Code/webapp/external_scripts
	External scripts that are used by the application
	Code/webapp/external_scripts/ConfigLogs
		Config files for moorings, platforms
	Code/webapp/external_scripts/alerts_csv
		A comma separated values files for all of the alerts
	Code/webapp/external_scripts/tests
		Folder for adding variables to the database and fixing unit tests
		Code/webapp/external_scripts/tests/data
			CSV file with variable data
Code/webapp/media
	Folder for media used in the application
	Code/webapp/media/drawings
		Diagrams of each of the moorings and buoy examples.

Code/webapp/static
	Folder that hold external scripts and images that are used by the application
	Code/webapp/static/admin
		Folder that holds external scripts, fonts, and images used by the admin in the application
		Code/webapp/static/admin/css
			CSS files
		Code/webapp/static/admin/fonts
			Fonts used in the application
		Code/webapp/static/admin/img
			Images used in the application
			Code/webapp/static/admin/img/gis
				Images used in the application
		Code/webapp/static/admin/js
			Javascript used in the application
			Code/webapp/static/admin/js/admin
				Javascript used in the application
			Code/webapp/static/admin/js/vendor
				External scripts used in the application
				Code/webapp/static/admin/js/vendor/jquery
					Jquery scripts used in the application
				Code/webapp/static/admin/js/vendor/xregexp
					Regular Expression scripts used in the application
	Code/webapp/static/css
		CSS files that control most of the application
	Code/webapp/static/images
		Images used in the application. Related to graphing
		Code/webapp/static/images/logos
			Images of logos used in the application
	Code/webapp/static/js
		Javascript files that control most of the application
	Code/webapp/static/vendor
		External scripts and source code used by the application
		Code/webapp/static/vendor/bootstrap/3.3.7
			Folder for version 3.3.7 of Bootstrap
			Code/webapp/static/vendor/bootstrap/3.3.7/css
				CSS files for Bootstrap
			Code/webapp/static/vendor/bootstrap/3.3.7/fonts
				Files for fonts of Bootstrap
			Code/webapp/static/vendor/bootstrap/3.3.7/js
				Javascript files for Bootstrap
		

Code/webapp/static/vendor/bootstrap-multiselect
			Folder holding files for Bootstrap Multiselect
			Code/webapp/static/vendor/bootstrap-multiselect/css
				CSS files for Bootstrap Multiselect
			Code/webapp/static/vendor/bootstrap-multiselect/js
				Javascript files for Bootstrap Multiselect
		Code/webapp/static/vendor/chartjs/2.5.0
			Folder for version 2.5.0 of Chartjs. Holds Javascript files for Chartjs
		Code/webapp/static/vendor/datatables
			Folder for holding source code for Datatables
			Code/webapp/static/vendor/datatables/1.10.13
				Folder for version 1.10.13 of Datatables
				Code/webapp/static/vendor/datatables/1.10.13/css
					CSS files for version 1.10.13 of Datatables
				Code/webapp/static/vendor/datatables/1.10.13/images
					Images for version 1.10.13 of Datatables
				Code/webapp/static/vendor/datatables/1.10.13/js
					Javascript for version 1.10.13 of Datatables
			Code/webapp/static/vendor/datatables/plugins/1.10.15
				Javascript files for a plugin of version 1.10.15 of Datatables
		Code/webapp/static/vendor/font-awesome/4.7.0
			Folder for version 4.7.0 of Font Awesome
			Code/webapp/static/vendor/font-awesome/4.7.0/css
				CSS files used in version 4.7.0 of Font Awesome
			Code/webapp/static/vendor/font-awesome/4.7.0/fonts
				Fonts used in version 4.7.0 of Font Awesome
			Code/webapp/static/vendor/font-awesome/4.7.0/less
				LESS files used in version 4.7.0 of Font Awesome
			Code/webapp/static/vendor/font-awesome/4.7.0/scss
				SCSS files used in version 4.7.0 of Font Awesome
		Code/webapp/static/vendor/highcharts/5.0.14
			Folder for version 5.0.14 of Highcharts
			Code/webapp/static/vendor/highcharts/5.0.14/modules
				Javascript files used in version 5.0.14 of Highcharts
		Code/webapp/static/vendor/jquery/3.1.1/js
			Folder for version 3.1.1 of JQuery
		Code/webapp/static/vendor/jquery-ui/1.12.1
			Folder for version 1.12.1 of JQuery UI
			Code/webapp/static/vendor/jquery-ui/1.12.1/css
				CSS used in version 1.12.1 of JQuery UI
			Code/webapp/static/vendor/jquery-ui/1.12.1/images
				Images used in version 1.12.1 of JQuery UI
			
Code/webapp/static/vendor/jquery-io/1.12.1/js
				Javascript used in version 1.12.1 of JQuery UI
		Code/webapp/static/vendor/jquery-validation/1.16.0
			Folder for version 1.16.0 of JQuery Validation
		Code/webapp/static/vendor/moment/2.18.1
			Folder for version 2.18.1 of Moment
		Code/webapp/static/vendor/pnotify
			Folder for PNotify
			Code/webapp/static/vendor/pnotify/css
				CSS files used by PNotify
			Code/webapp/static/vendor/pnotify/js
				Javascript files used by PNotify
		Code/webapp/static/vendor/tabulator/2.11.0
			Folder for version 2.11.0 of Tabulator
			Code/webapp/static/vendor/tabulator/2.11.0/css
				CSS files used in version 2.11.0 of Tabulator
			Code/webapp/static/vendor/tabulator/2.11.0/js
				Javascript files used in version 2.11.0 of Tabulator
Code/webapp/tasks
	Python files that run test tasks
Code/webapp/templates
	HTML templates that are used in the application
	Code/webapp/templates/account
		HTML templates that relate to accounts
	Code/webapp/templates/alert
		HTML templates that relate to alerts
	Code/webapp/templates/asset
		HTML templates that relate to assets
		Code/webapp/templates/asset/templatetags
			HTML templates that are referenced using tags within asset templates
	Code/webapp/templates/core/templatetags
		HTML templates that are referenced using tags within the application
	Code/webapp/templates/dashboard
		HTML templates that relate to the dashboard
	Code/webapp/templates/plots
		HTML templates that relate to plots
		Code/webapp/templates/plots/emails
			HTML templates that relate to sharing a plot page
		Code/webapp/templates/plots/plot_page
			HTML templates that relate to creating and viewing a plot page
	Code/webapp/templates/variable
		HTML templates that relate to L3 variables

Code/webapp/webapp
	Python files that relate to the application, such as settings
