# OMS++ project, website and database setup #

The installation instructions are based on an Ubuntu based distribution, but the requirements should be achievable on most other modern distributions

### Prerequisites ###

## Ubuntu ##

```bash
sudo apt-get update
sudo apt-get install git
sudo apt-get install python3-pip
sudo apt-get install virtualenv
sudo apt-get install postgresql postgresql-contrib
sudo apt-get install libpq-dev
```

### Project Setup ###

Set up a virtual environment and activate it. This should be created outside of the cloned repository

```bash
virtualenv --python=python3 ~/env/omsplusplus
source ~/env/omsplusplus/bin/activate
```

Clone the repository

```bash
git clone [repository url]
cd omsplusplus_sw/code/webapp
```

All development work is done off the develop branch, so pull that one down to your environment

```bash
git fetch
git checkout develop
```

Install project requirements

```bash
pip3 install -r requirements.txt
```

Copy initial project settings, which can then be modified as needed to match the environment

```bash
cp webapp/local_settings.py.example webapp/local_settings.py
```

_Note: You may receive an error related to the appdirs package if you have version 1.4.2 installed. If so, simply run this command again and it should correct the issue. If not, submit an issue and we can troubleshoot the specific enviroment you're working on_

### Database Setup ###

#### RedHat 7 and derivatives (Fedora, CentOS)

Follow instructions listed here for enabling official PostgreSQL repositories.

Run
```bash
sudo yum install postgresql96-server postgresql96-devel
```
Enable PostgreSQL to start the service upon server restart:

```bash
sudo systemctl enable postgresql-9.6.service
```

Start the PostgreSQL server

```bash
sudo systemctl start postgresql-9.6.service
```

#### Debian/Ubuntu

The following instructions will add the updated PostgreSQL repository, update it, and install PostgreSQL

```bash
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-9.6 postgresql-contrib-9.6 libpq-dev
```

Note: Since these instructions were initially written, newer versions the required database libraries have been released. The application has been successfully tested against newer versions of PostgreSQL (10.8 and 10.10) and psycopg2 (2.7 and 2.8.2)

#### Configuring database access permissions

There may be additional requirements needed that are specific to the server PostgreSQL is being installed on. Specifically, additional settings may need to be changed to support the required authentication scheme. Details can be found in the PostgreSQL documentation located here: https://www.postgresql.org/docs/9.6/static/auth-pg-hba-conf.html
 
After making any needed changes, PostgreSQL must be restarted using the following command:

```bash
sudo -u postgres pg_ctl reload
```

### Loading database

Use the init_database.sql script to create the database and the `omsplusplus`
PostgreSQL user account.

```bash
sudo -u postgres psql -f deploy/init_database.sql
```

The `omsplusplus` account is initialized with a weak password for testing
development settings.  It can be changed while by running a query in
PostgreSQL:

```sql
ALTER ROLE omsplusplus WITH PASSWORD 'password';
```


### Site Configuration ###

For the site to have access to the raw data files, one of the following must be done.
 
On a production server, where the raw data files are available, create a symlink within the project folder structure that links to those files
```bash
ln -s conf/data /data/raw
``` 

On the development server, there's a Django command that can be run to "fake" the directory structure needed. This will copy the included platform config files to the same location they would be in if the folder was symlinks. For gliders, just an empty folder structure is created. The paths for each mooring are defined in /conf/rawdata.json

```bash
./manage.py create_raw_data
```

For both dev and production, be sure to also set the Data Directory Url on the system settings page to point to where the raw data files are being serverd from. E.g, http://cgsn-dashboard.whoi.edu/raw/ 

Next, run the rebuild database command, which will do the following:

* Run database migrations
* Update ERDDAP credentials, if specified in the command parameters
* Load initial asset data
* Load initial alert triggers
* Update permissions

````bash
./manage.py rebuild_database -eu [username] -ep [passsword] --nodrop
````

Now start the web server

```bash
./manage.py runserver 8000
```

You can log on with the default admin credentials (which should be changed to something more secure as soon as possible)

```
http://localhost:8000
Username: admin
Passsword: password11
```

# Project Notes #

### Development Environment ###

There is no requirement to use a specific IDE, so you can use whichever one you are most comfortable with. Below are some helpful hints if you're using PyCharm

* There is no project file, and instead you can just open the directory containing the files (/omsplusplus_sw/code/webapp)
* Under settings -> project -> project interpreter, make sure you change the interpreter to the python executable in the virtual environment you already created (~/env/omsplusplus/bin/python)
* Enable version control by going to "VCS -> Enable Version Control Integration", and choosing "Git" (may not be necessary if PyCharm automatically detects Git)

### Running Tests ###

To run the test suite, ensure that all Python dependencies from `test_requirements.txt` are installed and then run `./manage.py test`

### Running Alert Jobs ###

Currently, alerts are processed by calling the `tasks.run_alerts.run_tests()` function with an instance of Trigger object.  There is also a convenience method to run all alerts, `all_tests()`, which fetches all the Trigger objects and runs all tests associated with them.  This could also be done manually like so:

```python
for trigger in Trigger.objects.all():
    run_tests(trigger)
```

### L3 Variables ###

L3 variables refer to data that is calculated from one or more variables.
These variables can be either stored in ERDDAP or as other L3 variables.
L3 variables are calculated via an expression valid in numexpr.  Valid
operators include, but are not limited to `() + - * / ^`.
See https://numexpr.readthedocs.io/en/latest/user_guide.html#supported-operators
and https://numexpr.readthedocs.io/en/latest/user_guide.html#supported-functions
for more comprehensive information on the list of supported functions and
operators.   For example, a valid L3 expression comprised of the
`psu` and `depth` variables might be `2 * psu - depth`
L3 variables can be created in the "[Gear Icon]" > "L3 Variables" menu.
L3 variables can be plotted on their own, can be components of other L3
variables, or can appear as part of a trigger expression.  For calculating
L3 variables where individual component timestamps may not align and/or
are sampled at different intervals, setting a reasonable linear interpolation
interval is recommended.

### Style Guidelines ###

* Include your first initial, last name in any TODOs or other action items in the code that need further edits
    * e.g, # TODO(mchagnon): still work to be done

### Custom Migrations ###

Migrations are handled by Django, and all code is within a single "core" app. This migrations will be applied in order as they are added to the project. To create a custom migration to add in PostgreSQL functions or update date, use the following command

```bash
./manage.py makemigrations --empty --name some_name core
```

Once run, a new file will be created in the migrations folder of the core app, which can be modified with the appropriate SQL

### Security Settings ###

Now that the project is up and running, it's good time to update the password on the PostgreSQL user account, as well as the password on the default admin user in Django

### Deployment ###

The instructions are specifically tailored for getting a development environment up and running, but much of it still applies to a production release with the following additions:

* Use the master branch rather than develop
* The gunicorn_start script will run the Django web server. The script will need to be modified to account for the various differences (paths, etc) in the production environment
* It's generally best to run a proxy web server to handle website requests. deploy/apache.conf provides a good starting point, and it can be used in conjunction with the proxy used for the ERDDAP server
* The web server will need to be restarted when the production machine boots, and the deploy/systemd script is a starting point for that functionality. Just be sure to update the settings inside the script to match the production environment
* Various settings should be reviewed and adjusted in the webapp/settings.py file. This includes the DEBUG flag, along others that will be more clearly defined in the future

### Production Server Configuration

There are multiple ways to configure a Django application for production use, so the instructions described here are only one way to do so. Other options will also work so long as the application is being run under a process that will automatically start the application when the server boots or when critical might occur

To begin, the project should be set up on the production the same way it would be set up on a development machine. Be sure to use the "master" branch from BitBucket. The location of the project files and virtual environment can be put in different locations on the server than described in the project set up instructions.  

Once the project is set up, you can verify it works by running the runserver Django command to run the development server. Only do this to validate that the project is set up correctly, and do not rely on the development server to run the application going forward 

##### Application Configuration
The contents of `/code/webapp/webapp/local/local_settings.py` should be modified as needed based on the specifics of the server the application is being run on. In particular, the DEBUG settings should be set to "False", and the ALLOWED_HOSTS settings should be updated to incldue the domain the website will be hosted at. For example:
```python
ALLOWED_HOSTS = ['cgsn-dashboard.whoi.edu', 'localhost']
```

##### Systemd Configuration
To run gunicorn, which will handle keeping the Django process running at all times, a new systemd configtation will need to be created. An example of this file is in the project, `/code/webapp/deploy/systemd`. The file should go, depending on the OS on the server, in `/etc/systemd/system`. Once installed, the following commands can be used to stop, start and restart the application when needed (typically for pushing releases)
```bash
sudo systemctl start omsplusplus
sudo systemctl stop omsplusplus
sudo systemctl restart omsplusplus
```

##### Gunicorn Configuration
The configuration for gunicorn is located in the "deploy" folder in the project, but the settings may need to be updated based on any specific server settings. The file that needs to be configured is here: `/code/webapp/deploy/gunicorn_start`
 
##### Apache configuration (website proxy)
The gunicorn process is set up to run the application on a local port, and it will need to be exposed to the outside world via a proxy. This is done via Apache, and on the WHOI server this is done with the same Apache configuration file that runs ERDDAP. An example configuration file can be found within the project, but will most likely need to be updated to address server specific settings (file path, etc). The example file can be found in `/code/webapp/deploy/apache.conf`

##### Data Directory Configuration 
The application requires the raw data files to be stored on the same server, or at least in a location that the project can access it. There then needs to be a symlink created from the project files to that directory. The link should be in the `/code/webapp/conf` directory. To create this link, use the following command, updating the path to the raw data file as needed:
```bash
cd code/webapp/conf
ln /data/raw data
```

##### Starting The Site For The First Time
Once all of the above items are complete, the site can be started for the first time with the following command:
```bash
sudo systemctl start omsplusplus
```
