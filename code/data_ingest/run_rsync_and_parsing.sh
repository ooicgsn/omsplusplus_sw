#!/bin/bash


# This scripts rsync's new OOI Pioneer & Global Mooring data from the ooi-cgpss1.whoi.net ("PSS")
#
# S. Petillo, spetillo@whoi.edu 2017-04-26


HOME=/home/ooiuser
SHELL=/bin/bash
MAILTO=ooi-sw@whoi.edu
#CRON_TZ=UTC


# Set deployment numbers for the moorings
CP01CNSM="D0006"
CP02PMCO="D0007"
GI01SUMO="D0003"

# Set platforms to rsync, parse, & process
#PLATFORM=( "cp01cnsm" "cp02pmco" "gi01sumo" )
PLATFORM=( "cp01cnsm" )

# Set which N prior days to parse and process data from
#N_DAYS_PRIOR=( 0 1 2 3 4 5 6 7 8 9 )
N_DAYS_PRIOR_MIN=0
N_DAYS_PRIOR_MAX=0

# Set when these moorings were deployed (e.g., "Fall-2016", "Spring-2017")
# This should match the season & year on the script "process_[csm/cpm/gsm]_[season]-[year].sh
DEPLOY_SEASON="Fall-2016"

# copy data from the WHOI PSS for each mooring (TO DO: Is wget for CPM the same as for CSM?)

#WGET_CSM="/usr/bin/wget -rN -np -nv -nH --cut-dirs=4 -e robots=off -R index.html* --no-check-certificate"
#WGET_CPM="/usr/bin/wget -rN -np -nv -nH --cut-dirs=4 -e robots=off -R index.html* --no-check-certificate"
RSYNC_DATA="rsync -av --progress --timeout=120"
SERVER="ooi-cgpss1.whoi.net"

echo ""
echo "Rsync'ing data..."

for pltfm in "${PLATFORM[@]}"
do
    case "$pltfm" in
	"cp01cnsm" )
	    DEPLOY=$CP01CNSM ;;
        "cp02pmco" )
	    DEPLOY=$CP02PMCO ;;
        "gi01sumo" )
	    DEPLOY=$GI01SUMO ;;
	"ce09ossm" )
	    DEPLOY=$CE09OSSM ;;
	* )
	    echo "Unknown platform, please check the name again"
	    exit 0 ;;
    esac
    
    $RSYNC_DATA $pltfm@$SERVER:/home/$pltfm/$DEPLOY $HOME/data/raw/$pltfm/ >> $HOME/logs/rsync_data_$pltfm.log
    
    #$RSYNC_DATA cp01cnsm@$SERVER:/home/cp01cnsm/$CP01CNSM $HOME/data/raw/cp01cnsm/ > $HOME/logs/rsync_data_cnsm.log
    #$RSYNC_DATA cp02pmco@$SERVER:/home/cp02pmco/$CP02PMCO $HOME/data/raw/cp02pmco/ > $HOME/logs/rsync_data_pmco.log
    #$RSYNC_DATA gi01sumo@$SERVER:/home/gi01sumo/$GI01SUMO $HOME/data/raw/gi01sumo/ > $HOME/logs/rsync_data_gi.log
    
    # cd $HOME/data/raw/cp01cnsm/$CP01CNSM; $WGET_CSM $URL/CP01CNSM/$CP01CNSM/cg_data/ > /dev/null
    # cd $HOME/data/raw/cp02pmco/$CP02PMCO; $WGET_CPM $URL/CP02PMCO/$CP02PMCO/cg_data/ > /dev/null
    # cd $HOME/data/raw/gi01sumo/$GI01SUMO; $WGET_CSM $URL/GI01SUMO/$GI01SUMO/cg_data/ > /dev/null
    
done

echo "Rsync complete."
echo ""

# process the daily log files, today's file and the last part of yesterday (try twice) (TO DO: Update path to cgsn-parsers.)


echo ""
echo "Activating CGSN conda envt..."

source activate CGSN
#echo "CGSN python = "
#conda list python
#conda list munch

echo ""
echo "Running parsers & processors..."

for pltfm in "${PLATFORM[@]}"
do

    case "$pltfm" in
	"cp01cnsm"|"cp03issm"|"cp04ossm" )
	    PLAT_TYPE="CSM"
	    PLAT_TYPE_HARVEST="csm"
	    ARRAY="CP" ;;
	"cp02pmci"|"cp02pmco"|"cp02pmui"|"cp02pmuo"|"cp04ospm" )
	    PLAT_TYPE="PM"
	    PLAT_TYPE_HARVEST="cpm"
	    ARRAY="CP" ;;
	"ga01sumo"|"gi01sumo"|"gs01sumo" )
	    PLAT_TYPE="GSM"
	    PLAT_TYPE_HARVEST="gsm"
	    ARRAY="GLOBAL" ;;
	"ce09ossm" )
	    PLAT_TYPE="CSM"
	    PLAT_TYPE_HARVEST="csm"
	    ARRAY="CE" ;;
	* )
	    echo "Unknown platform, please check the name again"
	    exit 0 ;;
    esac

    case "$pltfm" in
	"cp01cnsm" )
	    DEPLOY=$CP01CNSM ;;
	"cp02pmco" )
	    DEPLOY=$CP02PMCO ;;
	"gi01sumo" )
	    DEPLOY=$GI01SUMO ;;
	"ce09ossm" )
	    DEPLOY=$CE09OSSM ;;
	* )
	    echo "Unknown platform, please check the name again"
	    exit 0 ;;
    esac
    echo "==== $pltfm ===="

    # for n in "${N_DAYS_PRIOR[@]}"
    for n in $(eval echo {${N_DAYS_PRIOR_MIN}..${N_DAYS_PRIOR_MAX}})
    do
	
	echo "$HOME/code/cgsn-parsers/utilities/harvesters/master_harvester_${PLAT_TYPE_HARVEST}.sh $pltfm $DEPLOY $n |& tee -a $HOME/logs/harvest_$pltfm.log"
	$HOME/code/cgsn-parsers/utilities/harvesters/master_harvester_${PLAT_TYPE_HARVEST}.sh $pltfm $DEPLOY $n | tee -a $HOME/logs/harvest_$pltfm.log

	echo ""
	
	echo "$HOME/code/cgsn-processing/utilities/processors/process_${ARRAY}_${PLAT_TYPE}_${DEPLOY_SEASON}.sh $pltfm $DEPLOY $n |& tee -a $HOME/logs/process_$pltfm.log"
	$HOME/code/cgsn-processing/utilities/processors/process_${ARRAY}_${PLAT_TYPE}_${DEPLOY_SEASON}.sh $pltfm $DEPLOY $n | tee -a $HOME/logs/process_$pltfm.log
	
	echo "$n days ago."
	echo "==="
	echo ""
	
    done
done

#$HOME/code/cgsn-parsers/utilities/harvesters/master_harvester_csm.sh cp01cnsm $CP01CNSM 0 > $HOME/logs/harvest_csm0.log
#$HOME/code/cgsn-parsers/utilities/harvesters/master_harvester_csm.sh cp01cnsm $CP01CNSM 1 > $HOME/logs/harvest_csm1.log

# $HOME/bin/cgsn-parsers/harvester/master_harvester_cpm.sh cp02pmco $CP02PMCO 0 > /dev/null
# $HOME/bin/cgsn-parsers/harvester/master_harvester_cpm.sh cp02pmco $CP02PMCO 1 > /dev/null

# $HOME/bin/cgsn-parsers/harvester/master_harvester_csm.sh gi01sumo $GI01SUMO 0 > /dev/null
# $HOME/bin/cgsn-parsers/harvester/master_harvester_csm.sh gi01sumo $GI01SUMO 1 > /dev/null

echo ""
echo "Parsing & processing complete."

#echo ""
#echo "Activating CGSN_py2 conda envt..."

#source activate CGSN_py2
#echo "CGSN_py2 python = "
#conda list python

#echo ""
#echo "Running processors..."

#$HOME/code/cgsn-processing/utilities/processors/process_csm_Fall-2016.sh cp01cnsm $CP01CNSM 0 > $HOME/logs/process_csm0.log
#$HOME/code/cgsn-processing/utilities/processors/process_csm_Fall-2016.sh cp01cnsm $CP01CNSM 1 > $HOME/logs/process_csm1.log

#echo ""
#echo "Processing complete."


echo ""
echo "Deactivating all conda envts."
source deactivate
