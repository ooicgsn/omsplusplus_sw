#!/bin/bash

######################################### Global Config #########################################

# paths

HOME=/home/ooiuser
LOGS=$HOME/logs

HARVESTERS=$HOME/code/cgsn-parsers/utilities/harvesters
PROCESSORS=$HOME/code/cgsn-processing/utilities/processors

# date range

N_DAYS_PRIOR_MIN=0
N_DAYS_PRIOR_MAX=0

############################# Configuration Utilities #############################


# Per-platform-deployment config

declare -A PLATFORM
declare -A DEPLOYMENT
declare -A SEASON

# args:
# platform (e.g., ga01sumo)
# deployment (e.g., D0003)
# season (or just year) (e.g., Spring-2017)
config_deploy() {
    platform=$1
    deployment=$2
    season=$3
    plat_dep="${platform}-${deployment}"
    PLATFORM[$plat_dep]=$platform
    DEPLOYMENT[$plat_dep]=$deployment
    SEASON[$plat_dep]=$season
}

# Per-platform-type config

declare -A PLAT_TYPE
declare -A PLAT_TYPE_HARVEST
declare -A PLAT_ARRAY

# args:
# platform
# platform type (e.g., CSM)
# platform type for harvester (e.g., cp_csm)
# platform array (e.g., CP)
config_plat_type() {
    platform=$1
    PLAT_TYPE[$platform]=$2
    PLAT_TYPE_HARVEST[$platform]=$3
    PLAT_ARRAY[$platform]=$4
}

######################################### Main Script #########################################


#########################################
# Configure platforms
#########################################

#########################################
# Configure season per platform/deployment
#########################################

# To prevent processing of a particular platform/deployment,
# comment out the corresponding line below.

# Note: Season year does not matter for Mobile Assets because
# we are not parsing/processing their data here,
# but their Season years are included for completeness.

# Coastal Pioneer surface mooring
config_deploy cp01cnsm D0001 Spring-2014
config_deploy cp01cnsm D0002 Fall-2014
config_deploy cp01cnsm D0003 Spring-2015
config_deploy cp01cnsm D0004 Fall-2015
config_deploy cp01cnsm D0005 Spring-2016
config_deploy cp01cnsm D0006 Fall-2016
config_deploy cp01cnsm D0007 Spring-2017
config_deploy cp01cnsm D0008 Fall-2017
config_deploy cp01cnsm D0009 Spring-2018
config_deploy cp01cnsm D0010 Fall-2018
config_deploy cp01cnsm D0011 Spring-2019

config_deploy cp03issm D0001 Fall-2014
config_deploy cp03issm D0002 Spring-2015
config_deploy cp03issm D0003 Fall-2015
config_deploy cp03issm D0004 Spring-2016
config_deploy cp03issm D0005 Fall-2016
config_deploy cp03issm D0006 Spring-2017
config_deploy cp03issm D0007 Fall-2017
config_deploy cp03issm D0008 Spring-2018
config_deploy cp03issm D0009 Fall-2018
config_deploy cp03issm D0010 Spring-2019

config_deploy cp04ossm D0001 Fall-2014
config_deploy cp04ossm D0002 Spring-2015
config_deploy cp04ossm D0003 Fall-2015
config_deploy cp04ossm D0004 Spring-2016
config_deploy cp04ossm D0005 Fall-2016
config_deploy cp04ossm D0006 Spring-2017
config_deploy cp04ossm D0007 Fall-2017
config_deploy cp04ossm D0008 Spring-2018
config_deploy cp04ossm D0009 Fall-2018
config_deploy cp04ossm D0010 Spring-2019

# Global surface mooring
config_deploy ga01sumo D0001 2014
config_deploy ga01sumo D0002 2015
config_deploy ga01sumo D0003 2016

config_deploy gi01sumo D0001 2014
config_deploy gi01sumo D0002 2015
config_deploy gi01sumo D0003 2016
config_deploy gi01sumo D0004 2017
config_deploy gi01sumo D0005 2018

config_deploy gs01sumo D0001 2014
config_deploy gs01sumo D0002 2015
config_deploy gs01sumo D0003 2016
config_deploy gs01sumo D0004 2018

# Global subsurface mooring
config_deploy ga02hypm 0001 201x
config_deploy ga02hypm 0002 201x
config_deploy ga02hypm 0003 201x

config_deploy ga03flma 0001 201x
config_deploy ga03flma 0002 201x
config_deploy ga03flma 0003 201x

config_deploy ga03flmb 0001 201x
config_deploy ga03flmb 0002 201x
config_deploy ga03flmb 0003 201x

config_deploy gi02hypm 0001 201x
config_deploy gi02hypm 0002 201x
config_deploy gi02hypm 0003 201x
config_deploy gi02hypm 0004 201x
config_deploy gi02hypm 0005 201x

config_deploy gi03flma 0001 201x
config_deploy gi03flma 0002 201x
config_deploy gi03flma 0003 201x
config_deploy gi03flma 0004 201x
config_deploy gi03flma 0005 201x

config_deploy gi03flmb 0001 201x
config_deploy gi03flmb 0002 201x
config_deploy gi03flmb 0003 201x
config_deploy gi03flmb 0004 201x
config_deploy gi03flmb 0005 201x

config_deploy gp02hypm 0001 201x
config_deploy gp02hypm 0002 201x
config_deploy gp02hypm 0003 201x
config_deploy gp02hypm 0004 201x
config_deploy gp02hypm 0005 201x
config_deploy gp02hypm 0006 201x

config_deploy gp03flma 0001 201x
config_deploy gp03flma 0002 201x
config_deploy gp03flma 0003 201x
config_deploy gp03flma 0004 201x
config_deploy gp03flma 0005 201x
config_deploy gp03flma 0006 201x

config_deploy gp03flmb 0001 201x
config_deploy gp03flmb 0002 201x
config_deploy gp03flmb 0003 201x
config_deploy gp03flmb 0004 201x
config_deploy gp03flmb 0005 201x
config_deploy gp03flmb 0006 201x

config_deploy gs02hypm 0001 201x
config_deploy gs02hypm 0002 201x
config_deploy gs02hypm 0003 201x

config_deploy gs03flma 0001 201x
config_deploy gs03flma 0002 201x
config_deploy gs03flma 0003 201x

config_deploy gs03flmb 0001 201x
config_deploy gs03flmb 0002 201x
config_deploy gs03flmb 0003 201x

# Coastal Pioneer profiling mooring
config_deploy cp01cnpm D0001 Fall-2017
config_deploy cp01cnpm D0002 Fall-2018

config_deploy cp02pmci D0001 Fall-2013
config_deploy cp02pmci D0002 Spring-2014
config_deploy cp02pmci D0003 Fall-2014
config_deploy cp02pmci D0004 Spring-2015
config_deploy cp02pmci D0005 Fall-2015
config_deploy cp02pmci D0006 Spring-2016
config_deploy cp02pmci D0007 Fall-2016
config_deploy cp02pmci D0008 Spring-2017
config_deploy cp02pmci D0009 Fall-2017
config_deploy cp02pmci D0010 Spring-2018
config_deploy cp02pmci D0011 Fall-2018
config_deploy cp02pmci D0012 Spring-2019

config_deploy cp02pmco D0001 Fall-2013
config_deploy cp02pmco D0002 Spring-2014
config_deploy cp02pmco D0003 Fall-2014
config_deploy cp02pmco D0004 Spring-2015
config_deploy cp02pmco D0005 Fall-2015
config_deploy cp02pmco D0006 Spring-2016
config_deploy cp02pmco D0007 Fall-2016
config_deploy cp02pmco D0008 Spring-2017
config_deploy cp02pmco D0009 Fall-2017
config_deploy cp02pmco D0010 Spring-2018
config_deploy cp02pmco D0011 Fall-2018
config_deploy cp02pmco D0012 Spring-2019

config_deploy cp02pmui D0001 Spring-2013
config_deploy cp02pmui D0002 Fall-2013
config_deploy cp02pmui D0003 Spring-2014
config_deploy cp02pmui D0004 Fall-2014
config_deploy cp02pmui D0005 Spring-2015
config_deploy cp02pmui D0006 Fall-2015
config_deploy cp02pmui D0007 Spring-2016
config_deploy cp02pmui D0008 Fall-2016
config_deploy cp02pmui D0009 Spring-2017
config_deploy cp02pmui D0010 Fall-2017
config_deploy cp02pmui D0011 Spring-2018
config_deploy cp02pmui D0012 Fall-2018
config_deploy cp02pmui D0013 Spring-2019

config_deploy cp02pmuo D0001 Spring-2013
config_deploy cp02pmuo D0002 Fall-2013
config_deploy cp02pmuo D0003 Spring-2014
config_deploy cp02pmuo D0004 Fall-2014
config_deploy cp02pmuo D0005 Spring-2015
config_deploy cp02pmuo D0006 Fall-2015
config_deploy cp02pmuo D0007 Spring-2016
config_deploy cp02pmuo D0008 Fall-2016
config_deploy cp02pmuo D0009 Spring-2017
config_deploy cp02pmuo D0010 Fall-2017
config_deploy cp02pmuo D0011 Spring-2018
config_deploy cp02pmuo D0012 Fall-2018
config_deploy cp02pmuo D0013 Spring-2019

config_deploy cp03ispm D0001 Fall-2017
config_deploy cp03ispm D0002 Fall-2018

config_deploy cp04ospm D0001 Spring-2014
config_deploy cp04ospm D0002 Fall-2014
config_deploy cp04ospm D0003 Spring-2015
config_deploy cp04ospm D0004 Fall-2015
config_deploy cp04ospm D0005 Spring-2016
config_deploy cp04ospm D0006 Fall-2016
config_deploy cp04ospm D0007 Spring-2017
config_deploy cp04ospm D0008 Fall-2017
config_deploy cp04ospm D0009 Spring-2018
config_deploy cp04ospm D0010 Fall-2018
config_deploy cp04ospm D0011 Spring-2019

# Coastal Pioneer mobile assets
config_deploy CP05MOAS-A6263 R00003 201?X
config_deploy CP05MOAS-A6264 R00003 201?X
config_deploy CP05MOAS-GL335 D00005 2017
config_deploy CP05MOAS-GL336 D00005 2017
config_deploy CP05MOAS-GL339 D00005 2017
config_deploy CP05MOAS-GL340 D00006 2017
config_deploy CP05MOAS-GL374 D00005 201?X
config_deploy CP05MOAS-GL375 D00001 2015
config_deploy CP05MOAS-GL376 D00008 2017X
config_deploy CP05MOAS-GL379 D00005 2017
config_deploy CP05MOAS-GL380 D00005 2017X
config_deploy CP05MOAS-GL387 D00006 2017X
config_deploy CP05MOAS-GL388 D00006 2017X
config_deploy CP05MOAS-GL389 D00004 2017
config_deploy CP05MOAS-PG564 D00002 2017
config_deploy CP05MOAS-PG583 D00001 2017
# Global Argentine mobile assets
config_deploy GA05MOAS-GL364 D00002 2016
config_deploy GA05MOAS-GL470 D00001 2016
config_deploy GA05MOAS-GL493 D00001 2015
config_deploy GA05MOAS-GL494 D00001 2015
config_deploy GA05MOAS-GL495 D00001 2015
config_deploy GA05MOAS-GL496 D00001 2015
config_deploy GA05MOAS-GL538 D00001 2015
config_deploy GA05MOAS-PG562 D00001 2015
config_deploy GA05MOAS-PG563 D00001 201?X
config_deploy GA05MOAS-PG578 D00001 201?X
config_deploy GA05MOAS-PG580 D00001 201?X
# Global Irminger mobile assets
config_deploy GI05MOAS-GL469 D00001 2014
config_deploy GI05MOAS-GL477 D00001 2014
config_deploy GI05MOAS-GL478 D00001 2014
config_deploy GI05MOAS-GL484 D00003 2016
config_deploy GI05MOAS-GL485 D00002 2015
config_deploy GI05MOAS-GL486 D00002 2017
config_deploy GI05MOAS-GL493 D00002 2017
config_deploy GI05MOAS-GL495 D00002 2015
config_deploy GI05MOAS-GL559 D00001 2016
config_deploy GI05MOAS-PG528 D00001 2015
config_deploy GI05MOAS-PG581 D00002 2017X
# Global Papa mobile assets
config_deploy GP05MOAS-GL276 D00001 2014
config_deploy GP05MOAS-GL362 D00001 2014
config_deploy GP05MOAS-GL364 D00001 2013
config_deploy GP05MOAS-GL453 D00002 201?X
config_deploy GP05MOAS-GL525 D00001 2015
config_deploy GP05MOAS-PG514 D00002 2015
config_deploy GP05MOAS-PG575 D00001 2016
config_deploy GP05MOAS-GL361 D00001 2014
config_deploy GP05MOAS-GL363 D00001 2013
config_deploy GP05MOAS-GL365 D00001 2013
config_deploy GP05MOAS-GL523 D00002 201?X
config_deploy GP05MOAS-GL537 D00002 201?X
config_deploy GP05MOAS-PG515 D00001 201?X
# Global Southern mobile assets
#config_deploy GS05MOAS-GL453 D0000X 201?X
#config_deploy GS05MOAS-GL469 D0000X 201?X
config_deploy GS05MOAS-GL484 D00001 2015
config_deploy GS05MOAS-GL485 D00001 2015
config_deploy GS05MOAS-GL486 D00001 2015
config_deploy GS05MOAS-GL524 D00001 2015
config_deploy GS05MOAS-GL560 D00001 2015
config_deploy GS05MOAS-GL561 D00001 2015
config_deploy GS05MOAS-PG565 D00001 2015
config_deploy GS05MOAS-PG566 D00001 2015
#config_deploy GS05MOAS-PG581 D0000X 201?X
#config_deploy GS05MOAS-PG582 D0000X 201?X


#########################################
# Configure platform type per platform
#########################################

# Coastal Pioneer surface mooring
for platform in cp01cnsm cp03issm cp04ossm; do
    config_plat_type $platform CSM cp_csm CP
done

# Global surface mooring
for platform in ga01sumo gi01sumo gs01sumo; do
    config_plat_type $platform GSM gsm GLOBAL
done

# Global subsurface mooring
for platform in ga02hypm ga03flma ga03flmb gi02hypm gi03flma gi03flmb gp02hypm gp03flma gp03flmb gs02hypm gs03flma gs03flmb; do
    config_plat_type $platform SSM ssm GLOBAL
done

# Coastal Pioneer profiling mooring
for platform in cp01cnpm cp02pmci cp02pmco cp02pmui cp02pmuo cp03ispm cp04ospm; do
    config_plat_type $platform PM pm CP
done

# Coastal Pioneer mobile assets
for platform in CP05MOAS-A6263  CP05MOAS-A6264; do
    config_plat_type $platform AUV NA CP
done
for platform in CP05MOAS-GL335  CP05MOAS-GL336  CP05MOAS-GL339  CP05MOAS-GL340  CP05MOAS-GL374  CP05MOAS-GL375  CP05MOAS-GL376  CP05MOAS-GL379  CP05MOAS-GL380  CP05MOAS-GL387  CP05MOAS-GL388  CP05MOAS-GL389; do
    config_plat_type $platform GL NA CP
done
for platform in CP05MOAS-PG564  CP05MOAS-PG583; do
    config_plat_type $platform PG NA CP
done

# Global Argentine mobile assets
for platform in GA05MOAS-GL364  GA05MOAS-GL470  GA05MOAS-GL493  GA05MOAS-GL494  GA05MOAS-GL495  GA05MOAS-GL496  GA05MOAS-GL538; do
    config_plat_type $platform GL NA GA
done
for platform in GA05MOAS-PG562  GA05MOAS-PG563  GA05MOAS-PG578  GA05MOAS-PG580; do
    config_plat_type $platform PG NA GA
done

# Global Irminger mobile assets
for platform in GI05MOAS-GL469  GI05MOAS-GL477  GI05MOAS-GL478  GI05MOAS-GL484  GI05MOAS-GL485  GI05MOAS-GL486  GI05MOAS-GL493  GI05MOAS-GL495  GI05MOAS-GL559; do
    config_plat_type $platform GL NA GI
done
for platform in GI05MOAS-PG528  GI05MOAS-PG581; do
    config_plat_type $platform PG NA GI
done

# Global Papa mobile assets
for platform in GP05MOAS-GL276  GP05MOAS-GL361  GP05MOAS-GL362  GP05MOAS-GL363  GP05MOAS-GL364  GP05MOAS-GL365  GP05MOAS-GL453  GP05MOAS-GL523  GP05MOAS-GL525  GP05MOAS-GL537; do
    config_plat_type $platform GL NA GP
done
for platform in GP05MOAS-PG514  GP05MOAS-PG515  GP05MOAS-PG575; do
    config_plat_type $platform PG NA GP
done

# Global Southern mobile assets
for platform in GS05MOAS-GL453  GS05MOAS-GL469  GS05MOAS-GL484  GS05MOAS-GL485  GS05MOAS-GL486  GS05MOAS-GL524  GS05MOAS-GL560  GS05MOAS-GL561; do
    config_plat_type $platform GL NA GS
done
for platform in GS05MOAS-PG565  GS05MOAS-PG566  GS05MOAS-PG581  GS05MOAS-PG582; do
    config_plat_type $platform PG NA GS
done

#########################################
# Start
#########################################

echo Started at $(date)


#########################################
# Rsync data
#########################################


RSYNC="rsync -av --progress --timeout=120"
RSYNC_SERVER="ooi-cgpss1.whoi.net"
DS_RSYNC_SERVER="ooi-omsds1.whoi.net"

echo rsyncing data ...
for plat_dep in "${!PLATFORM[@]}"
do
    platform=${PLATFORM[$plat_dep]}
    deploy=${DEPLOYMENT[$plat_dep]}
    plat_type=${PLAT_TYPE[$platform]}
    echo rsyncing $platform $deploy ...

    if [[ "${plat_type}" = "AUV" || "$plat_type" = "GL" || "$plat_type" = "PG" ]]; then
        # rsync from DS if it is a MObile ASset
        MOAS_RSYNC_PATH="/home/ooiuser/DS/$platform/$deploy"
        src=ooiuser@$DS_RSYNC_SERVER:$MOAS_RSYNC_PATH
    elif [[ "${plat_type}" = "SSM" ]]; then
        # rsync from DS if it is a SubSurface Mooring
        platform="$(echo $platform | tr '[:lower:]' '[:upper:]')" # folders are uppercase on DS
        SSM_RSYNC_PATH="/home/ooiuser/DS/$platform/D0$deploy" # There is an extra 0 in the deployment number when coming from the DS
        src=ooiuser@$DS_RSYNC_SERVER:$SSM_RSYNC_PATH
        deploy="D$deploy"
    else
        # rsync from PSS if it is anything else (a surface/profiler mooring)
        RSYNC_PATH="/home/$platform/$deploy"
        src=$platform@$RSYNC_SERVER:$RSYNC_PATH
    fi

    # use lowercase folders on OMS++ srever
    platform="$(echo $platform | tr '[:upper:]' '[:lower:]')"
    dest=$HOME/data/raw/$platform/
    
    $RSYNC $src $dest >> $LOGS/rsync_data_$platform.log
    echo "$RSYNC $src $dest >> $LOGS/rsync_data_$platform.log"
done

echo Rsync complete at $(date)
echo

exit # SKIP PARSING & PROCESSING FROM THIS SCRIPT (use automated p/p instead)

######################################################################################

#########################################
# Parsing and processing
#########################################

# Activate conda environment
source activate CGSN

for plat_dep in "${!PLATFORM[@]}"
do
    for (( n=N_DAYS_PRIOR_MIN; n <= N_DAYS_PRIOR_MAX; n++ ))
    do
	# configure
	platform=${PLATFORM[$plat_dep]}
	deployment=${DEPLOYMENT[$plat_dep]}
	season=${SEASON[$plat_dep]}
	plat_type=${PLAT_TYPE[$platform]}
	plat_type_harvest=${PLAT_TYPE_HARVEST[$platform]}
	plat_array=${PLAT_ARRAY[$platform]}
	if [[ "${plat_type}" = "AUV" || "$plat_type" = "GL" || "$plat_type" = "PG" ]]; then
            # Don't parse & process if it is a MObile ASset
            echo "Not parsing/processing Mobile Asset ($platform $deployment) from this script"
        else
            # Parse & process if it is anything else (a mooring)
	    echo "Parsing & processing $platform $deployment from $n days ago"
            
	    # parse
	    harvester=$HARVESTERS/master_harvester_${plat_type_harvest}.sh
	    echo "$harvester $platform $deployment $n"
	    $harvester $platform $deployment $n | tee -a $LOGS/harvest_$platform.log

	    # process
	    processor=$PROCESSORS/process_${plat_array}_${plat_type}_${season}.sh
	    echo "$processor $platform $deployment $n"
	    $processor $platform $deployment $n | tee -a $LOGS/process_$platform.log
        fi

	echo
    done
done

source deactivate

echo Parsing and processing complete

#########################################
# Finish
#########################################

echo Run completed at $(date)
