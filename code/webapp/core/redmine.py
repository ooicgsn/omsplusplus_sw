import sys

from django.shortcuts import get_object_or_404
from livesettings.functions import config_value

from redminelib import Redmine as RedmineLib
from redminelib.exceptions import ValidationError

from .models.redmine import CustomField, Project, Tracker

class Issue:
    STATUS_NEW = 1

    def __init__(self):
        self.project_id = 0
        self.tracker_id = 0
        self.subject = ''
        self.description = ''
        self.custom_fields = {}

    def __str__(self):
        return self.subject

    def get_custom_fields(self):
         # Create a dict of possible custom fields and their Redmine IDs
        fields = {cf.name: cf.external_id for cf in CustomField.objects.all()}

        # Return a list of dicts that will meet the requirements that the Redmine API has
        return [{'id':fields[k], 'value':v} for k,v in self.custom_fields.items()]


class Redmine:
    def __init__(self, redmine_key):
        self.server_url = config_value('REDMINE', 'SERVER_URL')
        self.redmine_key = redmine_key

    # This is specifically run on each API call per Redmine recommendations (vs once when the class is loaded)
    def _init_redmine(self):
        return RedmineLib(self.server_url, key=self.redmine_key, requests={'verify': False})

    def submit_issue(self, issue):
        # Make sure project and tracker are valid
        project = get_object_or_404(Project, external_id=issue.project_id)
        tracker = get_object_or_404(Tracker, external_id=issue.tracker_id)

        redmine_api = self._init_redmine()
        try:
            result = redmine_api.issue.create(
                project_id=issue.project_id,
                subject=issue.subject,
                tracker_id=issue.tracker_id,
                description=issue.description,
                priority_id=issue.priority_id,
                status_id=Issue.STATUS_NEW,
                custom_fields=issue.get_custom_fields()
            )
        except ValidationError:
            type, value, traceback = sys.exc_info()
            return (None, str(value).split(','))

        return (result.id, [])

    def get_projects(self):
        redmine_api = self._init_redmine()

        projects = {}
        for project in redmine_api.project.all():
            projects[project.id] = project.name.strip()

        return projects