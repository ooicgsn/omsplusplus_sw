import sys
import re
import requests
import pandas as pd
from io import BytesIO
from collections import OrderedDict
import numpy as np
import json
from collections import defaultdict
from time import sleep
from urllib.parse import urljoin
from django.http import JsonResponse

from livesettings.functions import config_value
from bs4 import BeautifulSoup

from django.conf import settings

import logging
from core.util import HttpStatusCode
from core.models.asset import Asset
from core.models.variable import Variable, VariableComponent

"""Methods for interfacing directly with L3 data"""

logger = logging.getLogger('django')

def data_rate_of_change(df, interval):
    """
    Returns a dataframe with first order rate of change
    `df` - The dataframe to process
    `interval` - The time or arbitrary interval which is used to calculate the
                 rate.  Should be a Timedelta if dealing with time-indexed data,
                 or a numeric type if the Index is non-temporal
    """
    idx_diff = df.index.to_series().diff()
    data_diff = df.diff()
    if isinstance(df.index, pd.DatetimeIndex):
        ddiff = data_diff.divide(idx_diff.apply(lambda dt: dt / interval),
                                 axis=0)
    else:
        ddiff = data_diff.divide(idx_diff / interval, axis=0)
    return ddiff.shift(-1)

class Erddap:
    def __init__(self):
        self.server_url = config_value('ERDDAP', 'SERVER_URL')
        self.username = config_value('ERDDAP', 'USERNAME')
        self.password = config_value('ERDDAP', 'PASSWORD')

    def transform_dataframe(self, variables, df, index_units='UTC'):
        """
        Given a set of variables and a Dataframe, transform the dataframe
        into an ERDDAP-like response which can be serialized to JSON.
        Optionally supply units for the index name.
        """
        # if the index is datetime-like, convert it to a Unix timestamp
        if isinstance(df.index, pd.DatetimeIndex):
            conv_df = df.set_index(df.index.astype(np.int64) / 10 ** 9)
        else:
            conv_df = df
        # convert to list and replace any NaNs
        # with Nones so the output can be properly serialized to json
        conv_data = [[None if np.isnan(elem) else elem for elem in tup]
                     for tup in conv_df.to_records().tolist()]
        col_names = [df.index.name] + [v.erddap_id for v in variables]
        col_units = [index_units] + [v.units for v in variables]
        structure = {'table': {'columnNames': col_names,
                               'columnUnits': col_units,
                               'rows': conv_data}}
        return structure


    def fetch_data(self, variables, expression, expr_name, time_span_days=-2,
                   return_multiple=False, deployment_field='deploy_id',
                   deployment_id=None, x_var=None, asset_rename=False,
                   relative_to='now', interp_interval=None,
                   change_interval=None,
                   start_date=None, end_date=None,):
        """
        Gets the data from an ERDDAP endpoint, applies L3 transformations if
        necessary.
        Params:
        `variables` A dict with variable_alias/variable pairs
        `expression` An expression to evaluate in numexpr
        `expr_name` The column name of the resultant data
        `time_span_days` a negative number indicating the number of days from
                        the current time to search for observations
        `return_multiple` - If True, returns any intermediate data fetched to
                            produce the result.  By default this is False, and only
                            returns the final result.
        `relative_to` - One of 'now' or 'max_time'.  If 'now', fetches data
                        relative to the current time.  If 'max_time', fetches
                        data relative to the last recorded time measurement in
                        ERDDAP
        `interp_interval` - The maximum time interval over which to interpolate
                            missing data points. Takes Pandas Timedelta or None (default).
                            If none
        `change_interval` - The time or arbitrary interval which is used to calculate the
                            rate.  Should be a Timedelta if dealing with time-indexed data,
                            or a numeric type if the Index is non-temporal
        """
        asset_vars = defaultdict(list)
        custom_vars = []
        for var in variables.values():
            if not var.is_custom:
                asset_vars[var.asset].append(var)
                continue

            # For customer (L3) variables, recursively call asset variables
            # TODO: could fetch in a single call with proper formatting
            components = VariableComponent.objects.filter(parent=var)

            # Custom vars should return time series
            custom_vars.append(self.fetch_data(
                {c.name: c.child for c in components},
                var.expression, var.erddap_id, time_span_days, return_multiple,
                asset_rename=asset_rename, relative_to=relative_to, interp_interval=var.interpolation_amount,
                change_interval=var.change_interval, deployment_id=deployment_id, 
                start_date=start_date, end_date=end_date,))

        # Make sure there's at least one variable; otherwise, there's no way to process the trigger
        #if len(asset_vars) == 0:
        #    raise Exception("- No variables for this trigger")

        # combine the variables from different assets/custom vars together into a
        # single dataframe
        dfs = []
        msg_arr = []
        for asset, erd_vars in asset_vars.items():
            deployment_code = deployment_id if deployment_id else asset.deployment.code

            df, msgs = Erddap().get_dataframe_data(asset, erd_vars,
                                                   time_span_days,
                                             deployment_field=deployment_field,
                                             deployment_id=deployment_code,
                                             x_var=x_var,
                                             asset_rename=asset_rename,
                                             relative_to=relative_to,
                                             start_date=start_date, end_date=end_date,)

            # Need to group by to get rid of possible duplicate time series data.
            # This might make things a little slower, but is needed so pandas
            # won't bomb out

            if msgs is not None:
                msg_arr.append(msgs)
            deduped = df[~df.index.duplicated()]

            # Retained in case debugging is needed, but choosing the first value should be safe
            # if deduped.size != df.size:
            #     logger.warning("Duplicate values found while processing for variable(s) {}. First value taken from dataframe.".format(erd_vars))

            dfs.append(deduped)

        dfs += custom_vars
        # if there are dataframes present with data, fetch data
        if dfs:
            combined_df = pd.concat(dfs, axis=1)
        else:
            combined_df = pd.DataFrame()

        # apply any requested interpolation for this variable
        if interp_interval is not None:
            def interp_fn(ser):
                return interpolate_continuous_series(ser, interp_interval)

            combined_df = combined_df.apply(interp_fn)

        if asset_rename:
            formula_remaps = {"{}_{}".format(v.asset.erddap_id,
                                                v.erddap_id): name
                                                for name, v in variables.items()}
        # potential of name collision - new code that uses variable objects
        # as column names should fix this
        else:
            formula_remaps = {v.erddap_id: name for name, v in
                              variables.items()}

        combined_df.rename(columns=formula_remaps, inplace=True)
        # get rate of change if requested
        if change_interval:
            combined_df = data_rate_of_change(combined_df, change_interval)

        if expression is not None:
            # the resulting DataFrame must not be empty
            if not combined_df.empty:
                res = combined_df.eval(expression, engine='numexpr')
                res.name = expr_name
            else:
                res = pd.Series(name=expr_name)
            res._empty_reasons = msg_arr
            if not return_multiple:
                return res
            else:
                return pd.concat([res, combined_df], axis=1)
        else:
            combined_df._empty_reasons = msg_arr
            return combined_df


    def check_erddap_connection(self):
        url = '{}/tabledap/allDatasets.json?datasetID'.format(self.server_url)

        try:
            resp = requests.get(url, auth=(self.username, self.password))
            if resp.status_code != HttpStatusCode.OK.value:
                return (False, resp.text)

            response_data = resp.json()
            if not 'table' in response_data:
                return (False, "Invalid JSON response from ERDDAP:\n{}".format(response_data))
        except:
            return (False, sys.exc_info()[0])

        return (True, "Ok")

    # TODO (badams): refactor to use instance variables or similar internal
    # instance state instead of passing around variables through functions
    def get_maxtime(self, asset, deployment_field="deploy_id",
                           variable=None, relative_to='now', time_ago=None, ignore_missing_time=False):
        """Returns the maximum time from the deployment"""
        base_url = "{}/tabledap/{}.json?time".format(self.server_url,
                                                     asset.erddap_id)
        if variable is not None:
            if asset != variable.asset:
                logger.warn('Asset {} is not the same as variable asset {}'.format(asset, variable.asset))
            # note that != NaN "works" in a similar way to SQL "IS NOT NULL"
            # in ERDDAP despite NaN != NaN returning false regardless of the
            # value in the vast majority of other application/language
            # implementations.  Here we want to make sure we are getting the
            # last value in time

            # FIXME (badams): fails for non-float variables -- need a generic
            #                 way to find nodata vars
            base_url += ',{0}&{0}!=NaN&'.format(variable.erddap_id)
        else:
            base_url += '&'

        if relative_to == 'now':
            base_url += "time>=now%sday&" % time_ago
        elif relative_to == 'max_time':
            base_url += "time>=max(time)%sday&" % time_ago
        # use whole time period if relative_to is None
        elif relative_to is None:
            pass
        else:
            raise ValueError("`relative_to` must be one of 'now' or 'max_time'"
                             " or None")
        base_url += '{}="{}"&orderByMax("time")'.format(deployment_field,
                                                        asset.deployment.code)
        logger.info("Sending max time request to: {}".format(base_url))
        resp = requests.get(base_url, auth=(self.username, self.password))
        # todo try catch for error cases
        if variable is None:
            output_label = 'all'
            output_erddap_id = 'all'
        else:
            output_label = variable.name
            output_erddap_id = variable.erddap_id

        try:
            resp.raise_for_status()
            response_data = resp.json()['table']
            return (output_label, output_erddap_id,
                    pd.Timestamp(response_data['rows'][0][0]),
                    None)
        # catch HTTP errors and JSON parsing errors
        except (requests.exceptions.HTTPError, ValueError):
            if resp.status_code != 500 or\
                not ignore_missing_time or\
                'Your query produced no matching results.'\
                not in str(resp.content):
                    logger.warning("Encountered error while finding maxtime for url "
                            "{}, asset {}, variable {}".format(base_url, asset, variable),
                            exc_info=True)
            # return with error string
            return (output_label, output_erddap_id, pd.NaT,
                    Erddap.parse_error_response(resp))

    def get_last_update_times(self, asset, fetch_vars=False,
                              relative_to=None, time_ago=-1, ignore_missing_time=False):
        try:
            if fetch_vars:
                time_recs = [self.get_maxtime(asset, variable=v,
                                              relative_to=relative_to,
                                              time_ago=time_ago, ignore_missing_time=ignore_missing_time)
                                    for v in Variable.objects.filter(asset=asset,
                                                                    is_custom=False)]
                # if the records are empty, then there are no datasets
                if not time_recs:
                    logger.warning("Asset id {} has no associated variables".format(asset.id))
                    return pd.DataFrame(data=[[None, None, pd.NaT,
                                            'Asset has no variables stored in database']],
                                        columns=['variable_name', 'variable_erddap_id',
                                                'time', 'err_msg'])
            else:
                time_recs = [self.get_maxtime(asset, relative_to=relative_to,
                                              time_ago=time_ago, ignore_missing_time=ignore_missing_time)]

            return pd.DataFrame.from_records(time_recs,
                                            columns=['variable_name',
                                                    'variable_erddap_id',
                                                    'time',
                                                    'err_msg'])
        except:
            logger.warning("Asset id {} data fetch had errors fetching from ERDDAP:".format(asset.id),
                           exc_info=True)
            return pd.DataFrame(data=[[None, None, pd.NaT,
                                      'Asset does not exist in ERDDAP']],
                                 columns=['variable_name', 'variable_erddap_id',
                                         'time', 'err_msg'])


    def get_data(self, asset, variables, time_window, fmt, time_window_format,
                 deployment_field='deploy_id', deployment_id=None,
                 return_none=True, x_var=None, relative_to='now', 
                 start_date=None, end_date=None):
        """
        Retrieves data in the chosen format, optionally filtering by a
        deployment field and deployment id.  If `deployment_id` is None, then
        the query returns data from all deployments.

        `relative_to`: Should be one of 'now' or 'max_time'. 'now' looks back
        from the current time.  'last_obs' looks back from the last defined
        time point.
        """
        erddap_var_ids = ','.join(v.erddap_id for v in variables)

        # get time relative to current date minus some interval
        url_str = "%s/tabledap/%s.%s?%s,%s"
        if start_date or end_date:
            if start_date:
                url_str += '&time>="{}"'.format(start_date)
            if end_date:
                url_str += '&time<="{}"'.format(end_date)
        else:
            if relative_to == 'now':
                url_str += "&time>=now{}{}".format(time_window, time_window_format)
            elif relative_to == 'max_time':
                url_str += "&time>=max(time){}{}".format(time_window, time_window_format)
            else:
                raise ValueError("`relative_to` must be one of 'now' or 'max_time'")
        # filter by a specific deployment if requested
        if deployment_id is not None:
            # allow integer as well as string deployment IDs
            if isinstance(deployment_id, str):
                # ERDDAP needs string variables queried against in double quotes
                url_str += '&{}="{}"'.format(deployment_field, deployment_id)
            else:
                url_str += '&{}={}'.format(deployment_field, deployment_id)
        # default to the time string if not specified
        if x_var is None:
            x_var_str = 'time'
        else:
            x_var_str = x_var.erddap_id

        url = url_str % (self.server_url, asset.erddap_id, fmt, x_var_str,
                         erddap_var_ids)

        if settings.LOG_ERDDAP_REQUESTS:
            logger.info("Sending ERDDAP data request to {}".format(url))

        # TODO(mchagnon): This is a very specific URL. Will probably need way more options and a cleaner, more structured way to run ERDDAP queries
        # TODO(mchagnon): cleanup, error handling, etc
        resp = requests.get(url, auth=(self.username, self.password))
        # (badams) return None only if we have set the flag
        # this may be removed later to return response object once better error
        # handling is in place, but leave for now so as not to break the UI
        if return_none and resp.status_code == 404:
            return None

        return resp

    def get_plot_data(self, asset, variable, time_window,
                      deployment_field='deploy_id', deployment_id=None,
                      x_var=None, relative_to='now'):

        # TODO(mchagnon): cleanup, error handling, etc
        var_list = variable if isinstance(variable, list) else [variable]
        resp = self.get_data(asset, var_list, time_window, 'json', 'days',
                             deployment_field, deployment_id, x_var=x_var,
                             relative_to=relative_to)
        try:
            if resp:
                return resp.json()
            # if the response code is 500, it can sometimes be due to there
            # being no data present given the ERDDAP query filters.
            # In this case, return empty data
            elif (resp.status_code == 500 and
                  'Your query produced no matching results.'
                  in str(resp.content)):
                  # janky hack for creating an empty plot
                  #return JsonResponse({'table': {'rows': []}})
                  return {'table': {'rows': []}}
            else:
                return None
        # TODO: (badams) add logging to error handlers
        except:
            return None

    def get_time_data(self, asset, variable, time_window=-1,
                      deployment_field='deploy_id', deployment_id=None,
                      relative_to='now'):

        # TODO(mchagnon): cleanup, error handling, etc
        if type(variable) == list:
            resp = self.get_data(asset, variable, time_window, 'json',
                                 'seconds', deployment_field=deployment_field,
                                 deployment_id=deployment_id,
                                 relative_to=relative_to)
        else:
            resp = self.get_data(asset, [variable], time_window, 'json',
                                 'seconds', deployment_field=deployment_field,
                                 deployment_id=deployment_id,
                                 relative_to=relative_to)

        try:
            if resp:
                return resp.json()
            else:
                return None
        # TODO: (badams) add logging to error handlers
        except:
            return None

    @classmethod
    def parse_error_response(cls, resp):
        """
        Attempts to extract return an error message string from a Tomcat error
        response. Returns a string with the status code followed by the
        extracted error message if the response was parseable/queryable, or the
        original response contents if it was not.
        """
        # Hack alert: may be unstable when ERDDAP response changes or Java
        # Servlet containers change.  There's not really a choice here currently
        # since ERDDAP doesn't allow other error response formats which might be
        # more consistent than HTML
        try:
            soup = BeautifulSoup(resp.text, 'html.parser')
            # BWA: depending on the Java Servlet Container running, the
            # "message" text might be capitalized differently, so
            # make case insensitive
            msg_bold = soup.find('b', text=lambda s: s.lower() == 'message')
            # skip over "message" tag text -- the contents can be enclosed in a
            # text node or may be in a <u> element
            msg_contents = msg_bold.parent.text[8:]
            extracted_msg = (msg_contents if msg_contents is not None
                             else resp.text)
        except:
            extracted_msg = resp.text
        return "HTTP Status {}: {}".format(resp.status_code, extracted_msg)

    def get_dataframe_data(self, asset, variable, time_window,
                           deployment_field='deploy_id', deployment_id=None,
                           asset_rename=False, x_var=None, relative_to='now',
                           start_date=None, end_date=None):
        if type(variable) == list:
            variable_list = variable
        else:
            variable_list = [variable]
        resp = self.get_data(asset, variable, time_window,
                             'csv', 'days', deployment_field,
                             deployment_id, False, x_var,
                             relative_to=relative_to, 
                             start_date=start_date,
                             end_date=end_date,)
        # if the response code is 500, it can sometimes be due to there
        # being no data present given the ERDDAP query filters.
        # In this case, return empty data and display the reason
        empty_df = pd.DataFrame(columns=[v.erddap_id for v
                                         in variable_list])
        var_str = ' for variables {} and asset {}'.format([v.name for v in variable],
                                                          asset.erddap_id)

        if resp.status_code == 500 and 'Your query produced no matching results' in resp.text:
            # monkey-patch attribute here. Using a 2-tuple with (dataframe,
            # reason) was also considered, but was decided against so that
            # the function return type wouldn't break existing routines
            # calling get_dataframe_data
            if '(nRows = 0)' in resp.text:
                empty_reason = 'No ERDDAP data in range'
            elif r"is outside of the variable\'s actual_range" in resp.text:
                empty_reason = 'ERDDAP data selection outside of actual_range'
            else:
                empty_reason = Erddap.parse_error_response(resp)
            var_str = ' for variables {} and asset {}'.format([v.name for v in variable],
                                                              asset.erddap_id)
            return (empty_df, empty_reason + var_str)

        else:
            # raise exception for error statuses that do not deal with empty
            # data for a given range
            try:
                resp.raise_for_status()
            except requests.exceptions.HTTPError as e:
                if settings.LOG_ERDDAP_DETAILS:
                    resp_msg = Erddap.parse_error_response(resp)
                    return (empty_df, resp_msg + var_str)
                else:
                    return (empty_df, var_str)

            df = pd.read_csv(BytesIO(resp.content), header=0, skiprows=[1],
                             index_col=0, parse_dates=True)

            # if the index is a DatetimeIndex, and no timezone is specified,
            # assume UTC. if a timezone is specified, convert to UTC.
            if isinstance(df.index, pd.DatetimeIndex):
                if df.index.tz is None:
                    df.index = df.index.tz_localize('UTC')
                else:
                    df.index = df.index.tz_convert('UTC')

            # rename columns by prepending the asset name prevent a name collision
            if asset_rename:
                df.rename(columns=lambda c: "{}_{}".format(asset.erddap_id, c),
                          inplace=True)
            return (df, None)


    # badams: this can probably be removed due to get_plot_data_multiple
    def get_pair_columns(self, asset, variable, time_window, x_var=None,
                         deployment_field='deploy_id', deployment_id=None):
        df = self.get_dataframe_data(asset, variable, time_window,
                                     deployment_field=deployment_field,
                                     deployment_id=deployment_id,
                                     x_var=x_var)
        # convert to unix timestamp
        if isinstance(df.index, pd.DatetimeIndex):
            # convert to unix timestamp in milliseconds (not seconds due to JS
            # implementation) since json can't serialize pandas timestamps,
            # nor can it return JS dates
            df.index = df.index.astype(np.int64) / 10 ** 6
        data_pairs = OrderedDict()
        #data_pairs = OrderedDict((name, list(zip(ser.index, ser.values))) for
        #                          name, ser in df.iteritems())
        for name, ser in df.iteritems():
            # exclude NaNs from the plot for the time being
            filt_ser = ser[ser.notnull()]
            data_pairs[name] = list(zip(filt_ser.index, filt_ser.values))
        return json.dumps(data_pairs)
    # # badams: this can probably be removed due to get_plot_data_multiple
    # def get_pair_columns(self, asset, variable, time_window, x_var=None,
    #                      deployment_field='deploy_id', deployment_id=None):
    #     df = self.get_dataframe_data(asset, variable, time_window,
    #                                  deployment_field=deployment_field,
    #                                  deployment_id=deployment_id,
    #                                  x_var=x_var)
    #     # convert to unix timestamp
    #     if isinstance(df.index, pd.DatetimeIndex):
    #         # convert to unix timestamp in milliseconds (not seconds due to JS
    #         # implementation) since json can't serialize pandas timestamps,
    #         # nor can it return JS dates
    #         df.index = df.index.astype(np.int64) / 10 ** 6
    #     data_pairs = OrderedDict()
    #     #data_pairs = OrderedDict((name, list(zip(ser.index, ser.values))) for
    #     #                          name, ser in df.iteritems())
    #     for name, ser in df.iteritems():
    #         # exclude NaNs from the plot for the time being
    #         filt_ser = ser[ser.notnull()]
    #         data_pairs[name] = list(zip(filt_ser.index, filt_ser.values))
    #     return json.dumps(data_pairs)

    # TODO: This was pulled from the importer. It should be moved to a common location (possibly here) and the importer
    #   should be updated to use this method
    @staticmethod
    def remove_blacklisted_variables( name):
        """Boolean check to determine if any invalid variable names are present"""
        blacklist = ['^NC_GLOBAL$', '^deploy_id$', '^http.*$', '.*feature.*']

        return not any(re.search(pattern, name) for pattern in blacklist)

    # TODO: This was pulled from the importer. It should be moved to a common location (possibly here) and the importer
    #   should be updated to use this method
    def parse_usable_variables(self, df):
        """
        Takes an ERDDAP info dataframe and outputs a pivoted dataframe with only
        valid variables and associated attributes
        """
        # exclude global attributes so that only variable related rows remain
        variable_df = df[df['Variable Name'].apply(Erddap.remove_blacklisted_variables)]

        # pivot on vars for index, attrs for columns
        pivot_vars = variable_df.pivot('Variable Name', 'Attribute Name', 'Value')

        # axis info should be present in most datasets
        if 'axis' not in pivot_vars:
            pivot_vars['axis'] = np.NaN

        # We can't reliably establish variable dimension from TableDAP datasets,
        # so platform is used as an arbitrary proxy to determine which variables
        # have dimensions.
        if 'platform' in pivot_vars:
            return pivot_vars[pivot_vars['platform'].notnull() |
                              pivot_vars['axis'].notnull()]

        # if it's not there, anything else goes until there's a reliable way of
        # determining dimension info.
        return pivot_vars

    def get_variables(self, erddap_id):
        # get units
        url_target = urljoin(self.server_url + '/', 'info/{}/index.csv'.format(erddap_id))

        try:
            req = requests.get(url_target, auth=(self.username, self.password))
        except:
            # For some reason, calls to ERDDAP will fail randomly with the following error:
            #   Name or service not known
            # Since there's nothing that can be done abut this, wait a few seconds and try again
            self._print_error("Call to ERDDAP failed, retrying after 30 seconds...")
            sleep(30)
            try:
                req = requests.get(url_target, auth=(self.username, self.password))
            except:
                self._print_error("Call to ERDDAP failed a second time; moving one")
                return

        try:
            df = pd.read_csv(BytesIO(req.content), header=0)
            erddap_variables = self.parse_usable_variables(df)
        except:
            return []

        variables = []
        for name, attributes in erddap_variables.iterrows():
            variable = Variable()
            variable.erddap_id = name
            variable.name = str(attributes.get('long_name'))
            variable.units = attributes.get('units')

            # ERDDAP may not always contain a long name. If not, use the ERDDAP ID, since that's the only
            #   name that's going to be similar
            if not variable.name or variable.name == "nan" or variable.name == "None":
                variable.name = name

            variables.append(variable)

        return variables


def interpolate_continuous_series(ser, max_dt):
    """
    Interpolates continuous portions of data in `ser`
    where the difference between the next non-null measurement
    is less than or equal to `max_dt`.  Returns a new
    linearly interpolated series of the same length as `ser`
    """

    # get the non-null measurements in this series, get the first
    # order difference of the times between non-null measurements
    # and compare them to a threshold
    ser_nn = ser[ser.notnull()]
    tflag = ser_nn.index.to_series().diff() > pd.Timedelta(max_dt)

    # group by the contiguous time bins, get back the min/max time
    # of each bin
    t_sm = tflag.cumsum()
    t_groups = t_sm.index.to_series().groupby(t_sm).apply(lambda s: ((min(s)),
                                                                      max(s)))

    #store the contiguous runs of time data which will be interpolated
    interp_dfs = []
    # (badams): couldn't find a way to do this with .groupby()
    for interval in t_groups:
        interp_dfs.append(ser.loc[interval[0]:interval[1]].interpolate())

    # merge back together with
    interp_ts = pd.concat(interp_dfs)

    # update a copy of the original series based on the index to input
    # the interpolated variables
    ser_new = ser.copy()
    ser_new.update(interp_ts)
    return ser_new
