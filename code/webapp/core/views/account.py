from livesettings.functions import config_value

from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.urls import reverse
from django.contrib.auth.hashers import make_password
from django.http import JsonResponse
from core.db import exec_void

from ..forms.account import UserForm, ProfileForm
from core.util import route_name, check_perm, DataTablesResponse, HttpStatusCode
from urllib import parse
from django.urls import resolve

from ..models.account import Profile, MobileCarrier
from ..models.static import Page, DEFAULT_PAGE
from ..models.asset import Asset, Group as AssetGroup


def preferences(request):
    return render(request, "account/preferences.html", {})


def my_account(request, user_id=None):
    was_saved = False
    is_new_user = False
    from_my_account = route_name(request.get_full_path()) == 'account.my_account'

    if from_my_account:
        user = get_object_or_404(User, pk=request.user.id)
    else:
        try:
            user = User.objects.get(pk=user_id)
        except:
            is_new_user = True
            user = User()
            user.profile = Profile()

    laptop_mode = config_value('LAPTOP', 'LAPTOP_MODE')

    if request.method == 'POST':
        user_form = UserForm(check_perm(request, 'manage_groups'), request.POST, instance=user)
        profile_form = ProfileForm(check_perm(request, 'manage_redmine'), request.POST, instance=user.profile)

        if user_form.is_valid() and profile_form.is_valid():
            if is_new_user:
                user = User.objects.create_user(user_form.save(commit=False))
                user.first_name = user_form.cleaned_data['first_name']
                user.last_name = user_form.cleaned_data['last_name']
                user.email = user_form.cleaned_data['email']
            else:
                user = user_form.save(commit=False)

            user.profile = profile_form.save(commit=False)
            if user.profile.sms_number:
                user.profile.sms_carrier = MobileCarrier.objects.get(pk=profile_form.cleaned_data['sms_carrier'])

            # Update the password it a new one was given
            # TODO(mchagnon): add minimum password strength requirements (can we use Django?)
            if user_form.cleaned_data['new_password'] != '':
                user.password = make_password(user_form.cleaned_data['new_password'])

            # commit to db
            user.save()

            # if new user, create hot list of active alerts
            if is_new_user:
               exec_void('add_alert_notifications_for_new_user', (user.id,))

            # Remove unselected groups
            if check_perm(request, 'manage_groups'):
                selected_ids = user_form.cleaned_data['groups'].values('id')
                for group in Group.objects.filter(user=user).exclude(id__in=selected_ids):
                    group.user_set.remove(user)

                # Add selected groups not already set for this user
                [group.user_set.add(user) for group in user_form.cleaned_data['groups']]

            if not from_my_account:
                return HttpResponseRedirect(reverse('account.users'))

            was_saved = True

    else:
        user_form = UserForm(check_perm(request, 'manage_groups'), instance=user)
        profile_form = ProfileForm(check_perm(request, 'manage_redmine'), instance=user.profile,
                                   initial={'sms_carrier': user.profile.sms_carrier_id})

    return render(request, "account/edit_user.html", {
        'is_new_user': is_new_user,
        'user_form': user_form,
        'profile_form': profile_form,
        'was_saved': was_saved,
        'from_my_account': from_my_account,
        'laptop_mode': laptop_mode
    })


def users(request):
    if not check_perm(request, 'manage_users'):
        return redirect(settings.DASHBOARD_URL)
    
    return render(request, 'account/users.html', {
        'users': User.objects.filter(is_active=True).select_related('profile'),
    })


def default_page(request):
    profile = Profile.objects.get(pk=request.user.id)

    if route_name(parse.urlparse(request.META['HTTP_REFERER']).path) == 'overview':
        # dashboard
        profile.default_page = Page(pk=DEFAULT_PAGE.DASHBOARD.value)
        profile.default_group = None
        profile.default_asset = None
        profile.save()
    elif route_name(parse.urlparse(request.META['HTTP_REFERER']).path) == 'asset.group_overview':
        # group overview
        profile.default_page = Page(pk=DEFAULT_PAGE.GROUP.value)
        profile.default_group = AssetGroup(pk=resolve(parse.urlparse(request.META['HTTP_REFERER']).path).kwargs['group_id'])
        profile.default_asset = None
        profile.save()
    elif route_name(parse.urlparse(request.META['HTTP_REFERER']).path) == 'asset.overview':
        # asset overview
        profile.default_page = Page(pk=DEFAULT_PAGE.ASSET.value)
        profile.default_asset = Asset(pk=resolve(parse.urlparse(request.META['HTTP_REFERER']).path).kwargs['asset_id'])
        profile.default_group = None
        profile.save()
    return JsonResponse({'success': True})


def delete_user(request):
    if not check_perm(request, 'delete_user'):
        return JsonResponse({}, status=HttpStatusCode.FORBIDDEN.value)

    user_id = request.POST.get('user_id')

    try:
        user = User.objects.get(pk=user_id)
        #Adding the user id to the username to free up this username for future users
        user.username = user.username + '_' + user_id
        user.is_active = False
        user.save()

        return JsonResponse({})
    except:
        return JsonResponse({}, status=HttpStatusCode.BAD_REQUEST.value)


def user_list(request):
    ret_users = User.objects\
                .filter(is_active=True)\
                .select_related('profile')\
                .values('id', 'username', 'first_name', 'last_name', 'email',
                        'profile__phone', 'profile__position', 'profile__affiliation')

    return DataTablesResponse(list(ret_users))

