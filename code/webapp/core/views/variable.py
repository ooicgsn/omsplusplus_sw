import json
from django import forms
import numpy as np
import collections
from django.db.models import Q
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from ..models.variable import Variable, VariableComponent
from ..forms.l3_variable import L3VariableForm
from ..models.asset import Asset, AssetDisposition, AssetDispositions, AssetLink
from ..util import DataTablesResponse, check_perm
from ..variables import get_linked_variable
from ..db import exec_dict
from core.helpers import validate_expression
from core.erddap import Erddap
from core.plotting import Plotting
from ..models.static import L3Duration, IntervalTimes
from ..models.account import User
from webapp.settings import DASHBOARD_URL
from ..variables import get_related_triggers, get_related_plots


class PreviewVariable:
    """Simple class to mock a variable for use with previewing"""
    def __init__(self, erddap_id, units):
        self.erddap_id = erddap_id
        self.units = units


def manage_l3_variables(request):
    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value,]
    return render(request, 'variable/manage_l3_variables.html', {
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions
    })


def delete_l3(request):
    variable_id = request.POST.get("variable_id")
    variable = get_object_or_404(Variable, pk=variable_id)

    if variable.is_global and not check_perm(request, "delete_shared_items"):
        return JsonResponse({'success': False})

    if not variable.is_global and variable.user != request.user and not check_perm(request, "delete_other_items"):
        return JsonResponse({'success': False})

    # Prevent deleting an L3 that's still tied to other triggers/plots
    triggers = get_related_triggers(variable_id)
    plots = get_related_plots(variable_id)
    if len(triggers) > 0 or len(plots) > 0:
        return JsonResponse({'success': False})

    variable.delete()

    return JsonResponse({'success': True})


def clone_l3_variable(request, variable_id=''):
    variable = Variable.objects.get(pk=variable_id)
    can_clone_shared_items = check_perm(request, "clone_shared_items")
    can_clone_other_items = check_perm(request, "clone_other_items")

    if variable.is_global and not can_clone_shared_items:
        return redirect(DASHBOARD_URL)

    if not variable.is_global and variable.user != request.user and not can_clone_other_items:
        return redirect(DASHBOARD_URL)

    try:
        asset = Asset.objects.get(pk=variable.asset_id)
        variables = Variable.objects.filter(pk__in=VariableComponent.objects.filter(
            parent_id=variable_id).values_list('child_id', flat=True), is_custom=False)
        target_assets = Asset().get_clone_variable_assets(variable_id)
        dispositions = AssetDisposition.objects.all()
        shown_dispositions = [AssetDispositions.DEPLOYED.value, ]

    except ObjectDoesNotExist:
        pass

    return render(request, 'variable/clone_l3_variable.html', {
        'variable': variable,
        'variables': variables,
        'asset': asset,
        'target_assets': target_assets,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
    })


def execute_clone_variable(request):
    clone_without_owning = check_perm(request, "clone_without_owning")
    success, successful_assets, failed_assets = \
        clone_variable(request.POST['variable_id'],
                   Asset.objects.filter(pk__in=request.POST['target_asset_ids'].split(',')),
                   request.user.id, clone_without_owning)

    return JsonResponse({'success': success, 'successful_assets': successful_assets, 'failed_assets': failed_assets})

def clone_variable(variable_id, target_assets, user_id, clone_without_owning):
    success = True
    successful_assets = []
    failed_assets = []
    try:
        # trigger = get_object_or_404(Trigger, pk=trigger_id)
        # trigger_variables = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=False)
        # trigger_l3_vars = TriggerVariable.objects.filter(trigger_id=trigger.id, variable__is_custom=True)

        l3_var = Variable.objects.filter(pk=variable_id).first()

        # Loop through each target asset and clone the trigger for it
        for target_asset in target_assets:
            returned_successful_assets, returned_failed_assets = clone_variable_for_asset(
                l3_var, target_asset, user_id, clone_without_owning
            )
            successful_assets += returned_successful_assets
            failed_assets += returned_failed_assets
    except:
        success = False

    if len(successful_assets) is 0:
        success = False

    return success, successful_assets, failed_assets


def clone_variable_for_asset(l3_var, target_asset, user_id, clone_without_owning):
    successful_assets = []
    failed_assets = []
    try:
        mismatched_vars = False
        mismatched_l3_vars = False

        # Get list of child assets to traverse through to find variable assets
        linked_assets = Asset.objects.filter(pk__in=AssetLink.objects.filter(
            is_child=True, asset_id=target_asset.id).values_list('linked_asset_id', flat=True))

        # Loop through and find variables to clone
        new_l3_vars = []
        new_l3_var_components = []
        new_l3_var_variables = []
        try:
            current_l3_var_component = []
            current_l3_var_variables = []
            l3_var_variables = Variable.objects.filter(childVariable__parent_id=l3_var.id)
            for l3_var_variable in l3_var_variables:
                new_l3_variable = get_linked_variable(l3_var_variable, linked_assets,
                                                        target_asset.deployment_id, True)
                if new_l3_variable is not None:
                    current_l3_var_component.append(
                        VariableComponent.objects.filter(parent_id=l3_var.id,
                                                            child_id=l3_var_variable.id)[0])
                    current_l3_var_variables.append(new_l3_variable)

            if len(current_l3_var_variables) is len(l3_var_variables):
                new_l3_var_components.append(current_l3_var_component)
                new_l3_vars.append(l3_var)
                new_l3_var_variables.append(current_l3_var_variables)
            else:
                mismatched_l3_vars = True
        except:
            mismatched_l3_vars = True

        # Save new L3 variables for the cloned asset
        var_index = 0
        for l3_var in new_l3_vars:
            # Save parent variable
            new_l3_var = Variable(name=l3_var.name,
                                    is_custom=True,
                                    erddap_id=l3_var.erddap_id,
                                    units=l3_var.units,
                                    is_plottable=l3_var.is_plottable,
                                    expression=l3_var.expression,
                                    asset=target_asset,
                                    deployment=target_asset.deployment,
                                    platform=target_asset.platform,
                                    is_global=l3_var.is_global,
                                    user_id=user_id if not l3_var.is_global else None)
            new_l3_var.save()

            # Save child variables
            current_l3_var_components = new_l3_var_components[var_index]
            current_l3_var_variables = new_l3_var_variables[var_index]
            var_l3_index = 0
            for current_l3_var_variable in current_l3_var_variables:
                new_var_com = VariableComponent(name=current_l3_var_components[var_l3_index].name,
                                                child=current_l3_var_variable,
                                                parent=new_l3_var)
                new_var_com.save()
                var_l3_index += 1

            var_index += 1

            successful_assets.append({'AssetID': target_asset.id,
                                    'L3VarID': new_l3_var.id,
                                    'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                                    'L3Name': l3_var.name,
                                    'DeploymentCode': target_asset.deployment.code})

        if mismatched_l3_vars:
            failed_assets.append({'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                                    'L3Name': l3_var.name,
                                    'Message': '{} ({}) > {}: L3 Variables do not match up.'.format(
                                        target_asset.platform.name, target_asset.deployment.code, target_asset.name)})

    except:
        failed_assets.append({'AssetName': '{} ({}) > {}'.format(target_asset.platform.name, target_asset.deployment.code, target_asset.name),
                              'L3Name': l3_var.name,
                              'Message': '{} ({}) > {}: Unexpected error.'.format(
                                  target_asset.platform.name, target_asset.deployment.code, target_asset.name)})

    return successful_assets, failed_assets

def edit_l3_variable(request, variable_id=''):
    is_readonly = False
    can_edit_shared_items = check_perm(request, "edit_shared_items")
    can_edit_other_items = check_perm(request, "edit_other_items")

    try:
        variable = Variable.objects.get(pk=variable_id)

        # Non custom (L3) variables cannot be edited at all
        if not variable.is_custom:
            return redirect('variable.manage_l3_variables')

        # User's cannot modify non-owner, non-global variables
        if not variable.is_global and variable.user_id != request.user.id and not can_edit_other_items:
            is_readonly = True

        # See if the user can edit shared (global) variables
        if variable.is_global and not can_edit_shared_items:
            is_readonly = True
    except:
        variable = Variable()

    assets = Asset.objects.all()
    duration = ''

    if request.method == 'POST':
        initial_user_id = request.user if not can_edit_shared_items else None

        form = L3VariableForm(
            can_edit_shared_items,
            request.POST,
            instance=variable,
            initial={'user': initial_user_id}
        )

        if 'deletionComponentIds' in request.POST:
            deletion_ids_obj = request.POST.get('deletionComponentIds')
            if deletion_ids_obj:
                deletion_id_list = json.loads(deletion_ids_obj)
                VariableComponent.objects.filter(id__in=deletion_id_list[0].values()).delete()

        interval = variable.interpolation_amount
        if interval:
            seconds = interval.total_seconds()
            if seconds < IntervalTimes.SECONDS.value:
                duration_code = L3Duration.SECONDS.value
                duration = seconds
            if IntervalTimes.SECONDS.value <= seconds < IntervalTimes.MINUTES.value:
                duration_code = L3Duration.MINUTES.value
                duration = seconds / IntervalTimes.SECONDS.value
            if seconds >= IntervalTimes.MINUTES.value:
                duration_code = L3Duration.HOURS.value
                duration = seconds / IntervalTimes.MINUTES.value

        # Required to pass form validation
        if not can_edit_shared_items:
            form.is_global = False
            form.user = request.user
        elif variable.is_global:
            form.user = None

        if form.is_valid():
            variable = form.save(commit=False)
            variable.is_custom = True
            asset = Asset.objects.get(pk=request.POST.get('selectAsset_assetpicker'))
            platform_asset = Asset.objects.get(pk=asset.platform.id)
            variable.asset = asset
            variable.platform = platform_asset

            if not can_edit_shared_items:
                variable.is_global = False
                variable.user = request.user
            else:
                variable.user = form.cleaned_data['user'] if not variable.is_global else None

            if request.POST['duration'] and request.POST['duration_code']:
                duration_units = ''
                duration_value = request.POST.get('duration_code')
                if duration_value == str(L3Duration.SECONDS.value):
                    duration_units = 'Seconds'
                if duration_value == str(L3Duration.MINUTES.value):
                    duration_units = 'Minutes'
                if duration_value == str(L3Duration.HOURS.value):
                    duration_units = 'Hours'
                duration_string = str(request.POST.get('duration')) + ' ' + str(duration_units)
                variable.interpolation_amount = duration_string
            else:
                variable.interpolation_amount = None

            variable.save()

            components_obj = request.POST.get('saveComponents')
            components_obj_list = json.loads(components_obj)
            for component_obj in components_obj_list:
                component_variable = Variable.objects.get(pk=component_obj['childId'])

                if component_obj['componentId'] > 0:
                    new_component = VariableComponent.objects.get(pk=component_obj['componentId'])
                else:
                    new_component = VariableComponent()

                new_component.child = component_variable
                new_component.parent = variable
                new_component.name = component_obj['name']
                new_component.save()

            return redirect('variable.manage_l3_variables')
        else:
            comp_obj = str(json.loads(request.POST.get('saveComponents'))).replace("'", '"')

            # Prevents null asset error on re-post since asset picker is cleared on post-back
            variable.asset_id = request.POST.get('selectAsset_assetpicker')
            if request.POST['duration'] and duration == '':
                duration = request.POST['duration']

            return render(request, 'variable/edit_l3_variable.html', {
                'form': form,
                'variable': variable,
                'assets': assets,
                'duration': duration,
                'saved_components': comp_obj,
                "is_readonly": is_readonly,
            })

    else:
        duration_code = ''
        interval = variable.interpolation_amount
        if interval:
            seconds = interval.total_seconds()
            if seconds < IntervalTimes.SECONDS.value:
                duration_code = L3Duration.SECONDS.value
                duration = seconds
            if IntervalTimes.SECONDS.value <= seconds < IntervalTimes.MINUTES.value:
                duration_code = L3Duration.MINUTES.value
                duration = seconds / IntervalTimes.SECONDS.value
            if seconds >= IntervalTimes.MINUTES.value:
                duration_code = L3Duration.HOURS.value
                duration = seconds / IntervalTimes.MINUTES.value

        # Set some defaults if this is someone who cannot edit shared variables
        if not can_edit_shared_items and not variable.pk:
            variable.is_global = False
            variable.user = request.user

        form = L3VariableForm(
            can_edit_shared_items,
            instance=variable,
            initial={'duration': duration, 'duration_code': duration_code, 'user': variable.user}
        )

    return render(request, 'variable/edit_l3_variable.html', {
        'form': form,
        'variable': variable,
        'assets': assets,
        'components': variable.components.all(),
        'duration': duration,
        #'can_edit_global': can_edit_global
        "is_readonly": is_readonly,
    })


def saved_component_list(request):
    components_obj = str((request.POST.get('components'))).replace('&quot;', '"')
    vars = json.loads(components_obj)
    list = []

    for var in vars:
        variable = Variable.objects.get(pk=var['childId'])

        # The names here must match the output from the component_list method
        list.append({
            'id': var['componentId'],
            'child__id': var['childId'],
            'name': var['name'],
            'child__name': variable.name,
            'child__asset__id': variable.asset.id,
            'child__asset__name': variable.asset.name,
            'child__platform__id': variable.platform.id,
            'child__platform__name': variable.platform.name,
            'child__deployment__id': variable.deployment.id,
            'child__deployment__name': variable.deployment.code
        })

    return DataTablesResponse(list)


def component_list(request, variable_id):
    components = VariableComponent.objects\
        .filter(parent_id=variable_id)\
        .values('id', 'child__id', 'name', 'child__name', 'child__asset__id', 'child__asset__name',
                'child__platform__id', 'child__platform__name', 'child__deployment__id', 'child__deployment__name')

    return DataTablesResponse(list(components))


def l3_variable_list(request):
    variable_disposition = request.POST['variable_disposition']

    if variable_disposition != '':
        variables = exec_dict('get_custom_variables', (variable_disposition,))
    else:
        variables = exec_dict('get_custom_variables', ('',))

    return DataTablesResponse(list(variables))


def get_related_triggers_plots(request):
    variable_id = request.POST.get("variable_id")
    triggers = get_related_triggers(variable_id)
    plots = get_related_plots(variable_id)

    return JsonResponse({
        "triggers": [t.name for t in triggers],
        "plots": [p.name for p in plots]
    })


def variable_list_by_asset(request, asset_id):
    variables = Variable.objects.filter(asset_id=asset_id) \
        .values_list(
            'id', 'name', 'asset__id', 'asset__name', 'platform__id', 'platform__name',
            'deployment__id', 'deployment__name', 'units', 'erddap_id',
        ).order_by('name')

    return JsonResponse({'variables': list(variables)})


def validate(request):
    variable_alias = request.POST.getlist('variable_alias[]')
    expression = request.POST['expression']
    valid = validate_expression(expression, variable_alias, None)

    return JsonResponse({'success': valid})


def l3_plot_data(request):
    present_asset_id = request.POST['asset']
    expression = request.POST['expression']
    variable_ids = request.POST.getlist('variable_ids[]')
    variable_alias = request.POST.getlist('variable_alias[]')
    title = request.POST['title']
    y_var = request.POST['y_var']
    units = request.POST['units']

    asset = get_object_or_404(Asset, pk=present_asset_id)

    if len(variable_ids) != len(variable_alias):
        raise IndexError("variable_ids must be the same length as variable_alias")
    var_objs = []
    for v_id in variable_ids:
        # TODO: add exception handling
        var_objs.append(Variable.objects.get(id=v_id))
    variable_mapping = collections.OrderedDict((alias, var) for alias, var in
                                                zip(variable_alias, var_objs))
    asset_ids = []
    deployment_ids = []
    plot_data_dict = collections.defaultdict(dict)
    for v in range(0, len(variable_ids)):
        var = Variable.objects.get(pk=variable_ids[v])
        asset_ids.append(var.asset_id)
        deployment_ids.append(var.deployment_id)
    l3_data = Erddap()
    if l3_data.check_erddap_connection():
        l3_data_frame = l3_data.fetch_data(variable_mapping, expression,
                                           'l3_preview',
                                           interp_interval=var.interpolation_amount).to_frame()
        # TODO (badams): pass in L3 variable name and units here
        # a mock L3 variable that exposes some of the same fields as
        # the variable model
        l3_variable_mock = PreviewVariable('Test Expression', 'Some units')
        # simulated response from ERDDAP
        table_struct = l3_data.transform_dataframe([l3_variable_mock],
                                                   l3_data_frame)

    # TODO (badams) add exception case for when ERDDAP is unreachable
    if table_struct:
        plot_data = Plotting().build_l3_plot(table_struct, plot_type='time_series', title=title, y_var=y_var,
                                             units=units, deployment_code=asset.deployment.code, custom_variable=True,
                                             parent_asset=asset.platform.name, present_asset=asset.name)
        plot_data_dict['0000'] = plot_data
    else:
        msg = 'Could not load ERDDAP plot data for custom L3 Variable'
        plot_data_dict['0000'] = {'error': msg}

    return JsonResponse({
        'asset_ids': asset_ids,
        'deployment_ids': deployment_ids,
        'l3_data': plot_data_dict
    })
