from django.shortcuts import get_object_or_404, render, redirect
from django.http import JsonResponse, HttpResponseRedirect, QueryDict
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist
from ..models.asset import Asset, AssetLink, AssetDisposition, AssetDispositions
from ..models.plot import Plot, PlotVariable, PlotPage, PlotPagePlot, PlotTimeRange, PlatformDefaultPlot
from ..models.static import AssetPages
from ..forms.plot import PlotForm, PlotClass
from ..models.variable import Variable, VariableComponent
from ..models.account import User
from ..variables import get_linked_variable
from ..db import exec_raw, exec_dict
from ..util import check_perm
from webapp.settings import DASHBOARD_URL

def construct_var_name(variable_id):
    var_instance = get_object_or_404(Variable, pk=variable_id)
    var_instance_name = var_instance.name
    var_instance_asset = var_instance.asset.name
    var_instance_parent_asset = var_instance.asset.platform.name
    try:
        var_instance_deployment = var_instance.deployment.name
    except:
        var_instance_deployment = '---'

    if var_instance_asset == var_instance_parent_asset:
        detailed_var_name = var_instance_parent_asset + \
                        ' (' + var_instance_deployment + '): ' + var_instance_name
    else:
        detailed_var_name = var_instance_asset + ' on ' + var_instance_parent_asset + \
                        ' (' + var_instance_deployment + '): ' + var_instance_name

    #remove deployment placeholder for now (L3 variables dont have a deployment)
    if var_instance.is_custom:
        detailed_var_name = detailed_var_name.replace(' (---)','')

    return detailed_var_name


def edit_plot(request, asset_id, deployment_code, plot_id=None):
    is_readonly = False
    can_edit_shared_items = check_perm(request, "edit_shared_items")
    can_edit_other_items = check_perm(request, "edit_other_items")

    edit_type = request.GET.get('type', '')
    add_another = request.GET.get('add_another', '')
    variables_raw_x = request.GET.get('x_var', '')
    variables_raw_y = request.GET.get('y_var', '')
    from_where = request.GET.get('from', 'asset')
    plot_type = request.GET.get('plot_type', 1)
    plot_class = request.GET.get('plot_class', 'Timeseries')
    y_direction = request.GET.get('y_axis_reversed', False)
    time_range = request.GET.get('time_range_id', 1)
    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    user = request.user

    try:
        plot = get_object_or_404(Plot, pk=plot_id)

        # User's cannot modify non-owner, non-global variables
        if not plot.is_global and plot.user_id != request.user.id and not can_edit_other_items:
            is_readonly = True

        # See if the user can edit shared (global) variables
        if plot.is_global and not can_edit_shared_items:
            is_readonly = True
    except:
        plot = Plot()

    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    time_choices = PlotTimeRange.objects.all()
    time_choices_dict = {}
    for i in time_choices:
        time_choices_dict.setdefault(i.id, {"name": i.name, "days": i.days})

    xvarArr = []
    yvarArr = []
    plot=''

    if type(y_direction) == str:
        if y_direction == 'false':
            y_direction = False
        else:
            y_direction = True

    if asset_id == "asset=none" and request.method == "GET":

        form = PlotForm(initial={'variables_x': 'time', 'variables_y': '', 'is_global': False, 'owner': user.id,
                                 'plot_type': plot_type,'plot_class': plot_class_dict[plot_class],
                                 'yaxis_orientation': y_direction,'timeframe': 1, 
                                 'start_date':start_date, 'end_date':end_date})

        return render(request, 'asset/edit_plot.html', {
            'form': form,
            'asset': '',
            'xvarArr': xvarArr,
            'yvarArr': yvarArr,
            'plot_class_dict': plot_class_dict,
            'time_choices_dict': time_choices_dict,
            'from_where': from_where,
            'edit_type': edit_type,
            "is_readonly": is_readonly,
        })

    asset = get_object_or_404(Asset, pk=asset_id)
    platform = asset.platform or asset
    deployment = asset.deployment
    variables = Variable().get_asset_variables(asset_id, request.user.id)

    plot_pages = PlotPage.objects.filter(user_id=user.id, asset_id=platform.id, is_asset_page=False)

    # build querydict from scratch
    qdict = QueryDict('', mutable=True)

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = PlotForm(request.POST)
        if form.is_valid():
            plotname_output = form.cleaned_data['name']
            variable_output_x = form.cleaned_data['variables_x']
            variable_output_y = form.cleaned_data['variables_y']
            timeframe_output = form.cleaned_data['timeframe']
            start_date_output = form.cleaned_data['start_date'] or None
            end_date_output = form.cleaned_data['end_date'] or None
            globalplot_output = form.cleaned_data['is_global']

            default_on_platforms = form.cleaned_data['default_on_platforms']

            plot_type_output = form.cleaned_data['plot_type']
            plot_class_output = form.cleaned_data['plot_class']
            yaxis_direction_output = form.cleaned_data['yaxis_orientation']

            if form.cleaned_data['owner'] is None or form.cleaned_data['owner'] == '':
                user_output = user.id
            else:
                user_output = form.cleaned_data['owner']

            if plot_class_output.name == 'Comparison':
                # x-axis (there is only 1 x variable in a comparison plot)
                variable_output_x_array = [int(variable_output_x)]

                # y-axis (there is only 1 x variable in a comparison plot)
                variable_output_y_array = [int(variable_output_y)]


            if plot_class_output.name == 'Timeseries':
                # y-axis (up to 4 variables at this time)
                variable_output_y_array = [int(v) for v in variable_output_y.split(',')]

                # x-axis (each y variable could have a different corresponding time.. let's keep track of them all)
                variable_output_x_array = []
                for y_var in variable_output_y_array:
                    variable = get_object_or_404(Variable, pk=y_var)
                    time_var = Variable.objects.filter(asset=variable.asset, erddap_id='time')
                    variable_output_x_array.append(time_var.first().id)

            if edit_type == 'create':
                plot = Plot()
                plot.name = plotname_output
                plot.asset = asset

                if not can_edit_shared_items:
                    plot.user_id = user_output
                    plot.is_global = globalplot_output
                else:
                    plot.user = request.user
                    plot.is_global = False

                plot.plot_type = plot_type_output
                plot.plot_class = plot_class_output
                plot.plot_time_range = timeframe_output
                plot.start_date = start_date_output
                plot.end_date = end_date_output
                plot.save()

                asset_overview_page = PlotPage.objects.filter(asset_id=asset.id, user_id=request.user.id, is_asset_page=True).first()
                if asset_overview_page:
                    plot_page_plot = PlotPagePlot()
                    plot_page_plot.page = asset_overview_page
                    plot_page_plot.plot = plot
                    plot_page_plot.order = PlotPagePlot.objects.filter(page_id=asset_overview_page.id).count()+1
                    plot_page_plot.save()
            else:
                plot = get_object_or_404(Plot, pk=plot_id)
                #apply update logic

                Plot.objects.filter(pk=plot_id).update(name=plotname_output, user=user_output,
                                                       is_global=globalplot_output, plot_type=plot_type_output,
                                                       plot_class=plot_class_output, plot_time_range=timeframe_output, 
                                                       start_date = start_date_output, end_date = end_date_output)

                # remove vars that aren't in the requested array (x axis)
                PlotVariable.objects.filter(plot=plot, axis='x').exclude(variable__in=variable_output_x_array).delete()

                # remove vars that aren't in the requested array (y axis)
                PlotVariable.objects.filter(plot=plot, axis='y').exclude(variable__in=variable_output_y_array).delete()

                # might need to update the remaining variables yaxis orientation (the ones that werent deleted)

            # loop through requested variables and save to plot_variable table if they don't already exist
            PlatformDefaultPlot.objects.filter(plot=plot).delete()
            for platform_d in default_on_platforms:
                defaultplot = PlatformDefaultPlot()
                defaultplot.plot = plot
                defaultplot.platform_name = platform_d
                defaultplot.save()
                

            # loop through requested variables and save to plot_variable table if they don't already exist
            for var in variable_output_x_array:

                if PlotVariable.objects.filter(variable=var, plot=plot, axis='x').count() == 1:
                    variable = PlotVariable.objects.filter(variable=var, plot=plot, axis='x').update(reverse_axis=False)
                else:
                    variable = get_object_or_404(Variable, pk=var)
                    plotvariable = PlotVariable()
                    plotvariable.plot = plot
                    plotvariable.axis = 'x'
                    plotvariable.variable = variable
                    plotvariable.reverse_axis = False
                    plotvariable.save()

            # loop through requested variables and save to plot_variable table if they don't already exist
            for var in variable_output_y_array:

                if PlotVariable.objects.filter(variable=var, plot=plot, axis='y').count() == 1:
                    variable = PlotVariable.objects.filter(variable=var, plot=plot, axis='y').update(reverse_axis=yaxis_direction_output)
                else:
                    variable = get_object_or_404(Variable, pk=var)
                    plotvariable = PlotVariable()
                    plotvariable.plot = plot
                    plotvariable.axis = 'y'
                    plotvariable.variable = variable
                    plotvariable.reverse_axis = yaxis_direction_output
                    plotvariable.save()

            # check if we are coming from a plot page
            if 'plot_page' in from_where:
                plot_page_number = from_where.replace('plot_page','')
                from_where = 'plot_page'

            if from_where == 'asset':
                return HttpResponseRedirect(reverse('asset.overview', args=(asset_id, deployment_code)))

            elif from_where == 'admin_manager':
                qdict.update({'platform_id': platform.id, 'saved': True})
                if add_another:
                    qdict.update({'type': 'create', 'from': 'admin_manager'})
                    return HttpResponseRedirect(reverse('plots.edit_plot', args=(asset_id, deployment_code)) + '?' + qdict.urlencode())
                else:
                    return HttpResponseRedirect(reverse('plots.manage_plots') + '?' + qdict.urlencode())
            elif from_where == 'plot_page':
                return HttpResponseRedirect(reverse('plot.plot_page', args=(plot_page_number)))
            else:
                return HttpResponseRedirect(reverse('asset.overview', args=(asset_id, deployment_code)))

    # if a GET (or any other method)
    else:
        # pre-existing plot
        if plot_id != None:
            plotvar_x = PlotVariable.objects.filter(plot=plot_id, axis='x')

            for x in plotvar_x:
                xvarObj={}
                variable_id = x.variable.id
                if x.variable.erddap_id == 'time':
                    variable_id = 'Time'
                    detailed_var_name = 'Time'
                else:
                    detailed_var_name = construct_var_name(variable_id)

                xvarObj.setdefault('name', detailed_var_name)
                xvarObj.setdefault('id', variable_id)
                xvarArr.append(xvarObj)

            if len(plotvar_x) == 0 or 'Time' in [elem['id'] for elem in xvarArr]:
                xvarArr =[{'name': 'Time', 'id': 'Time'}]

            plotvar_y = PlotVariable.objects.filter(plot=plot_id, axis='y')
            for y in plotvar_y:
                yvarObj = {}
                variable_id = y.variable.id
                detailed_var_name = construct_var_name(variable_id)

                yvarObj.setdefault('name', detailed_var_name)
                yvarObj.setdefault('id', variable_id)
                yvarArr.append(yvarObj)

        # no pre-existing plot
        if plot_id == None:
            x_var_ids = variables_raw_x.split(',')
            if ((len(variables_raw_x) > 0) & ('Time' not in variables_raw_x)):
                x_var_ids_int=[int(v) for v in x_var_ids]
                for var_id in x_var_ids_int:
                    xvarObj={}
                    detailed_var_name = construct_var_name(var_id)
                    xvarObj.setdefault('name', detailed_var_name)
                    xvarObj.setdefault('id', var_id)
                    xvarArr.append(xvarObj)
            else:
                xvarArr = [{'name': 'Time', 'id': 'Time'}]
            if variables_raw_y:
                y_var_ids = variables_raw_y.split(',')
                y_var_ids_int = [int(v) for v in y_var_ids]
                for var_id in y_var_ids_int:
                    yvarObj = {}
                    detailed_var_name = construct_var_name(var_id)
                    yvarObj.setdefault('name', detailed_var_name)
                    yvarObj.setdefault('id', var_id)
                    yvarArr.append(yvarObj)

        # the specifics of the initial form will depend on if type is create or update
        if edit_type == 'create':
            plot=''
            form = PlotForm(initial={'variables_x': variables_raw_x, 'variables_y': variables_raw_y, 'is_global': False,
                                     'plot_type': plot_type, 'owner': user.id,
                                     'plot_class': plot_class_dict[plot_class],'yaxis_orientation': y_direction,
                                     'timeframe': time_range, 'start_date':start_date, 'end_date':end_date})
        else:
            plot = get_object_or_404(Plot, pk=plot_id)

            # get y-axis orientation from the DB and default to the query param if necessary (at this time all y-vars
            # will have the same axis orientation
            y_direction = PlotVariable.objects.filter(plot_id=plot.id, axis='y').first().reverse_axis

            default_on_platforms = list(PlatformDefaultPlot.objects.filter(plot=plot).values_list("platform_name", flat=True))

            form = PlotForm(instance=plot, initial={'variables_x': variables_raw_x, 'variables_y': variables_raw_y,
                                                    'default_on_platforms': default_on_platforms,
                                                    'owner': plot.user_id, 'plot_type': plot.plot_type, 
                                                    'start_date': plot.start_date, 'end_date': plot.end_date,
                                                    'plot_class': plot.plot_class, 'yaxis_orientation': y_direction,
                                                    'timeframe': plot.plot_time_range})

    return render(request, 'asset/edit_plot.html', {
        'form': form,
        'asset': asset,
        'deployment': deployment,
        'platform': platform,
        'plot_pages': plot_pages,
        'current_asset_page': AssetPages.ASSET_OVERVIEW.value,
        'variables': variables,
        'xvarArr': xvarArr,
        'yvarArr': yvarArr,
        'time_choices_dict': time_choices_dict,
        'plot_class_dict': plot_class_dict,
        'from_where': from_where,
        'edit_type': edit_type,
        'plot': plot,
        "is_readonly": is_readonly,
    })


def delete_custom_plot(request):
    plot_id = request.POST.get("plot_id")
    plot = Plot.objects.get(pk=plot_id)

    if plot.is_global and not check_perm(request, "delete_shared_items"):
        return JsonResponse({'success': False})

    if not plot.is_global and plot.user != request.user and not check_perm(request, "delete_other_items"):
        return JsonResponse({'success': False})

    Plot.delete(plot_id)

    return JsonResponse({
        'success': True,
        'plotid': plot_id
    })


def manage_plots(request):
    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value,]

    platforms = Asset.objects.filter(parent_id=None).select_related('platform').select_related('deployment')
    platforms = sorted(platforms, key=lambda a: a.name + str(a.deployment.sortable_code))

    return render(request, 'asset/manage_plots.html', {
        'platforms': platforms,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions
    })


def plot_list(request):
    asset_id = request.POST['asset_id']
    asset_disposition = request.POST['asset_disposition']

    if asset_id != '' and asset_disposition != '':
        asset = get_object_or_404(Asset, pk=asset_id)
        platform_id = asset.platform_id or asset.id
        plotData = exec_dict('get_plots', (platform_id, asset_disposition))

    elif asset_id == '' and asset_disposition == '':
        plotData = exec_dict('get_plots', (None, ''))

    elif asset_id != '' and asset_disposition == '':
        asset = get_object_or_404(Asset, pk=asset_id)
        platform_id = asset.platform_id or asset.id
        plotData = exec_dict('get_plots', (platform_id, ''))

    else:
        plotData = exec_dict('get_plots', (None, asset_disposition))

    return JsonResponse({ 'data': plotData })


def get_plot_info(request):
    plot_id = request.POST['plot_id']
    plot = Plot.objects.get(pk=plot_id)

    plot_name = plot.name
    plot_global = plot.is_global
    plot_class = plot.plot_class.id
    plot_type = plot.plot_type.name.lower()
    plot_asset = plot.asset.id
    plot_deployment = plot.asset.deployment.id
    plot_time_window = -1 # hardcode for now since not defined in DB

    x_var_ids = []
    y_var_ids = []

    plotvar_x = PlotVariable.objects.filter(plot=plot_id, axis='x')
    for x in plotvar_x:
        if x.variable.erddap_id == 'time':
            x_var_ids.append('Time')
        else:
            variable_id = x.variable.id
            x_var_ids.append(variable_id)

    if 'Time' in x_var_ids:
        variables_raw_x = 'Time'
    else:
        variables_raw_x = ','.join([str(i) for i in sorted(x_var_ids)])


    plotvar_y = PlotVariable.objects.filter(plot=plot_id, axis='y')
    for y in plotvar_y:
        if y.variable.erddap_id == 'depth':
            y_var_ids.append('Depth')
        else:
            variable_id = y.variable.id
            y_var_ids.append(variable_id)

    if 'Depth' in y_var_ids:
        variables_raw_y = 'Depth'
    else:
        variables_raw_y = ','.join([str(i) for i in sorted(y_var_ids)])

    y_direction = plotvar_y.first().reverse_axis

    plot_data_obj = {
        'plot_name': plot_name,
        'plot_global': plot_global,
        'plot_time_window': plot_time_window,
        'plot_class': plot_class,
        'plot_type': plot_type,
        'plot_asset': plot_asset,
        'plot_deployment': plot_deployment,
        'plot_x_vars': variables_raw_x,
        'plot_y_vars': variables_raw_y,
        'plot_y_dir_reversed': y_direction
    }

    return JsonResponse(plot_data_obj)


def get_asset_variables(request):

    asset_id = request.POST['asset_id']
    variables = Variable().get_asset_variables(asset_id, request.user.id)
    # TODO: add logic to weed out time and depth variables?

    return JsonResponse({'success': True, 'variables': variables})


def clone_plot_page(request, asset_id, deployment_code, plot_id):
    can_clone_shared_items = check_perm(request, "clone_shared_items")
    can_clone_other_items = check_perm(request, "clone_other_items")
    plot = get_object_or_404(Plot, pk=plot_id)

    if plot.is_global and not can_clone_shared_items:
        return redirect(DASHBOARD_URL)

    if not plot.is_global and plot.user != request.user and not can_clone_other_items:
        return redirect(DASHBOARD_URL)

    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    try:
        asset = Asset.objects.get(pk=plot.asset_id)
        variables = Variable.objects.filter(pk__in=PlotVariable.objects.filter(
            plot_id=plot_id).values_list('variable_id', flat=True), is_custom=False)
        l3_vars = Variable.objects.filter(pk__in=PlotVariable.objects.filter(
            plot_id=plot_id).values_list('variable_id', flat=True), is_custom=True)
        target_assets = Asset().get_clone_plot_assets(plot_id)
        dispositions = AssetDisposition.objects.all()
        shown_dispositions = [AssetDispositions.DEPLOYED.value, ]
        variables_x = request.GET.get('variables_x', '')
        variables_y = request.GET.get('variables_y', '')
        plot_class = request.GET.get('plot_class', '')
        y_direction = request.GET.get('y_axis_reversed', False)

        if type(y_direction) == str:
            if y_direction == 'false':
                y_direction = False
            else:
                y_direction = True

    except ObjectDoesNotExist:
        pass

    return render(request, 'plots/plot_page/clone_plot.html', {
        'plot': plot,
        'asset': asset,
        'variables': variables,
        'l3_vars': l3_vars,
        'target_assets': target_assets,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
        'deployment_code': deployment_code,
        'variables_x': variables_x,
        'variables_y': variables_y,
        'plot_class': plot_class,
        'plot_class_dict': plot_class_dict,
        'y_direction': y_direction
    })


def execute_clone_plot(request):
    clone_without_owning = check_perm(request, "clone_without_owning")
    success, successful_assets, failed_assets = \
        clone_plot(request.POST['plot_id'],
                   Asset.objects.filter(pk__in=request.POST['target_asset_ids'].split(',')),
                   request.user.id, clone_without_owning)

    return JsonResponse({'success': success, 'successful_assets': successful_assets, 'failed_assets': failed_assets})


def clone_plot(plot_id, target_assets, user_id, clone_without_owning):
    success = True
    successful_assets = []
    failed_assets = []
    try:
        plot = get_object_or_404(Plot, pk=plot_id)
        plot_variables = PlotVariable.objects.filter(plot_id=plot.id, variable__is_custom=False)
        plot_l3_vars = PlotVariable.objects.filter(plot_id=plot.id, variable__is_custom=True)

        # Loop through each target asset and clone the plot for it
        for target_asset in target_assets:
            returned_successful_assets, returned_failed_assets = clone_plot_for_asset(
                plot, plot_variables, plot_l3_vars, target_asset, user_id, clone_without_owning
            )
            successful_assets += returned_successful_assets
            failed_assets += returned_failed_assets
    except:
        success = False

    if len(successful_assets) is 0:
        success = False

    return success, successful_assets, failed_assets


def clone_plot_for_asset(plot, plot_variables, plot_l3_vars, target_asset, user_id, clone_without_owning):
    successful_assets = []
    failed_assets = []
    platform_name = target_asset.platform.name if target_asset.platform else target_asset.name
    try:
        mismatched_vars = False
        mismatched_l3_vars = False

        # Get list of child assets to traverse through to find variable assets
        linked_assets = Asset.objects.filter(pk__in=AssetLink.objects.filter(
            is_child=True, asset_id=target_asset.id).values_list('linked_asset_id', flat=True))

        # Loop through plot variables and find matching assets/plots on the target asset
        new_plot_variables = []
        for plot_variable in plot_variables:
            try:
                new_plot_variable = get_linked_variable(plot_variable.variable, linked_assets,
                                                        target_asset.deployment_id)

                if new_plot_variable is not None:
                    new_plot_variables.append(new_plot_variable)
                else:
                    mismatched_vars = True
            except:
                mismatched_vars = True

        # Loop through and find plot L3 variables to clone
        new_l3_vars = []
        new_l3_var_components = []
        new_l3_var_variables = []
        for plot_l3_var in plot_l3_vars:
            try:
                current_l3_var_component = []
                current_l3_var_variables = []
                plot_l3_var_variables = Variable.objects.filter(childVariable__parent_id=plot_l3_var.variable_id)
                for plot_l3_var_variable in plot_l3_var_variables:
                    new_l3_variable = get_linked_variable(plot_l3_var_variable, linked_assets,
                                                          target_asset.deployment_id)

                    if new_l3_variable is not None:
                        current_l3_var_component.append(
                            VariableComponent.objects.filter(parent_id=plot_l3_var.variable_id,
                                                             child_id=plot_l3_var_variable.id)[0])
                        current_l3_var_variables.append(new_l3_variable)

                if len(current_l3_var_variables) is len(plot_l3_var_variables):
                    new_l3_var_components.append(current_l3_var_component)
                    new_l3_vars.append(plot_l3_var)
                    new_l3_var_variables.append(current_l3_var_variables)
                else:
                    mismatched_l3_vars = True
            except:
                mismatched_l3_vars = True

        # Only clone the plot if all the variables match
        if len(new_plot_variables) is len(plot_variables) and len(new_l3_vars) is len(plot_l3_vars):
            # Create the new plot
            new_plot = Plot(name=plot.name,
                            is_global=plot.is_global,
                            asset=target_asset,
                            user=plot.user,
                            plot_type=plot.plot_type,
                            plot_class=plot.plot_class,
                            plot_time_range=plot.plot_time_range, 
                            start_date=plot.start_date,
                            end_date=plot.end_date)

            # If the user cannot clone the plot as is, make sure that the clone is owned by this user and not shared
            if not clone_without_owning and not plot.is_global:
                new_plot.user_id = user_id
                new_plot.is_global = False

            new_plot.save()

            # Save new plot variables for the cloned asset
            var_index = 0
            x_variable_ids = []
            y_variable_ids = []
            for variable in new_plot_variables:
                new_plot_variable = PlotVariable(plot=new_plot,
                                                 variable=variable,
                                                 axis=plot_variables[var_index].axis,
                                                 reverse_axis=plot_variables[var_index].reverse_axis)
                new_plot_variable.save()
                if new_plot_variable.axis is 'x':
                    x_variable_ids.append(new_plot_variable.variable.id)
                else:
                    y_variable_ids.append(new_plot_variable.variable.id)
                var_index += 1

            # Save new L3 variables for the cloned asset
            var_index = 0
            for l3_var in new_l3_vars:
                # Save parent variable
                new_l3_var = Variable(name=l3_var.variable.name,
                                      is_custom=True,
                                      erddap_id=l3_var.variable.erddap_id,
                                      units=l3_var.variable.units,
                                      is_plottable=l3_var.variable.is_plottable,
                                      expression=l3_var.variable.expression,
                                      asset=target_asset,
                                      deployment=target_asset.deployment,
                                      platform=target_asset.platform,
                                      is_global=l3_var.variable.is_global,
                                      user_id=user_id if not l3_var.variable.is_global else None)
                new_l3_var.save()

                # Save child variables
                current_l3_var_components = new_l3_var_components[var_index]
                current_l3_var_variables = new_l3_var_variables[var_index]
                var_l3_index = 0
                for current_l3_var_variable in current_l3_var_variables:
                    new_var_com = VariableComponent(name=current_l3_var_components[var_l3_index].name,
                                                    child=current_l3_var_variable,
                                                    parent=new_l3_var)
                    new_var_com.save()
                    var_l3_index += 1

                # Save the actual plot variable for the L3
                new_plot_variable = PlotVariable(plot=new_plot,
                                                 variable=new_l3_var,
                                                 axis=l3_var.axis,
                                                 reverse_axis=l3_var.reverse_axis)
                new_plot_variable.save()
                if new_plot_variable.axis is 'x':
                    x_variable_ids.append(new_plot_variable.variable.id)
                else:
                    y_variable_ids.append(new_plot_variable.variable.id)
                var_index += 1

            successful_assets.append({'AssetID': target_asset.id,
                                      'PlotID': new_plot.id,
                                      'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'PlotName': plot.name,
                                      'PlotClass': new_plot.plot_class.id,
                                      'DeploymentCode': target_asset.deployment.code,
                                      'X_VariableIDs': ','.join(str(i) for i in x_variable_ids),
                                      'Y_VariableIDs': ','.join(str(i) for i in y_variable_ids)})
        else:
            if mismatched_vars and mismatched_l3_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'PlotName': plot.name,
                                      'Message': '{} ({}) > {}: Plot and L3 Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            elif mismatched_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'PlotName': plot.name,
                                      'Message': '{} ({}) > {}: Plot Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            elif mismatched_l3_vars:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'PlotName': plot.name,
                                      'Message': '{} ({}) > {}: L3 Variables do not match up.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
            else:
                failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                                      'PlotName': plot.name,
                                      'Message': '{} ({}) > {}: Unexpected error.'.format(
                                          platform_name, target_asset.deployment.code, target_asset.name)})
    except:
        failed_assets.append({'AssetName': '{} ({}) > {}'.format(platform_name, target_asset.deployment.code, target_asset.name),
                              'PlotName': plot.name,
                              'Message': '{} ({}) > {}: Unexpected error.'.format(
                                  platform_name, target_asset.deployment.code, target_asset.name)})

    return successful_assets, failed_assets




