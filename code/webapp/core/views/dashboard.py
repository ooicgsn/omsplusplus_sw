import os 
from statistics import mean
from django.shortcuts import render, HttpResponseRedirect, reverse, get_object_or_404

from ..models.alert import Alert
from ..models.asset import Asset, AssetGroups, Group, AssetDisposition, AssetDispositions
# TODO: Clean up and remove unneeded links
from ..models.static import DEFAULT_PAGE
from ..models.static import AssetPages, TimeChoices, YAxisChoices, PlotClasses, PlotTypes, Statuses
from ..models.variable import Variable

from ..models.account import Profile
from ..models.plot import Plot, PlotPage, PlotPagePlot, PlotVariable, PlotClass
from django.http import JsonResponse
from ..util import parse_querystring_list  

from django.core.management import call_command

from django.conf import settings

def default_check(request):
    profile = request.user.profile
    if profile.default_page_id == DEFAULT_PAGE.DASHBOARD.value:
        alerts = Alert().get_hotlist_alerts(request.user.id)
        return render(request, 'dashboard/overview.html', {
            'alerts': alerts,
        })
    elif profile.default_page_id == DEFAULT_PAGE.GROUP.value:
        return HttpResponseRedirect(reverse('asset.group_overview', args=[profile.default_group.pk]))
    elif profile.default_page_id == DEFAULT_PAGE.ASSET.value:
        return HttpResponseRedirect(reverse('asset.overview',
                args=[profile.default_asset.pk, profile.default_asset.deployment.name]))
    else:
        alerts = Alert().get_hotlist_alerts(request.user.id)
        return render(request, 'dashboard/overview.html', {
            'alerts': alerts,
            'bookmark': True
        })


def overview(request):
    alerts = Alert().get_hotlist_alerts(request.user.id)

    return render(request, 'dashboard/overview.html', {
        'alerts': alerts,
        'bookmark': True
    })


def execute_backup_database(request):
    try:
        call_command('dbbackup', compress=True)
        return JsonResponse({'success': True})
    except Exception as e:
        return JsonResponse({'success': False, 'error_message': str(e) })


def backup_database(request):
    path = settings.DBBACKUP_STORAGE_OPTIONS.get("location") #'./media/db'
    file_path = path[1:] if path[0] is '.' else path
    try:
        file_list = os.listdir(path)
    except IOError:
        os.mkdir(path)
        file_list = os.listdir(path)

    file_list = sorted(file_list)
    return render(request, 'dashboard/backup_database.html', {
        'file_list': file_list,
        'file_path': file_path,
    })


def system_overview(request, group_id):
    show_disposition_filter = True
    url = request.build_absolute_uri()
    public = 'base.html'
    if 'public' in url:
        public = 'public_base.html'
        show_disposition_filter = False

    dispositions = AssetDisposition.objects.all()
    shown_dispositions = [AssetDispositions.DEPLOYED.value,]

    groups = [group_id]
    page_title = Group.objects.get(pk=group_id).name + ' Array'

    assets = Asset.objects.filter(parent=None, group_id__in=groups)
    if assets.count() > 0:
        array_status = mean([asset.average_platform_status for asset in assets])
    else:
        array_status = 0
    alerts_dict = {}

    for plat_asset in assets:
        one = 0
        two = 0
        three = 0
        four = 0

        alerts = Alert.objects.filter(asset__platform=plat_asset, status=Statuses.OPEN.value)
        for alert in alerts:
            if alert.severity.id == 1:
                one += 1
            elif alert.severity.id == 2:
                two += 1
            elif alert.severity.id == 3:
                three += 1
            else:
                four += 1

        alerts_dict[plat_asset.id] = {1: one, 2: two, 3: three, 4: four}

    user = Profile.objects.get(user=request.user)


    return render(request, 'dashboard/system_overview.html', {
        'assets': assets,
        'array_status': array_status,
        'page_title': page_title,
        'public': public,
        'alerts': alerts_dict,
        'group_id': group_id,
        'dispositions': dispositions,
        'shown_dispositions': shown_dispositions,
        'show_disposition_filter': show_disposition_filter,
        'alert_count': user.overview_functional_counts,
        'functional_percentage': user.overview_functional_percentage
    })

def update_system_overview_preference(request):
    pref_id = request.POST['preference_id']
    current_user = Profile.objects.get(user=request.user)

    if pref_id == '1':
        current_user.overview_functional_counts = False
        current_user.overview_functional_percentage = True
    elif pref_id == '2':
        current_user.overview_functional_counts = True
        current_user.overview_functional_percentage = False
    else:
        current_user.overview_functional_counts = True
        current_user.overview_functional_percentage = True

    current_user.save()

    return JsonResponse({'success': True})

def update_alert_by_disposition(request):
    group_id = request.POST['group_id']
    alert_dispositions = request.POST['alert_disposition']
    one = 0
    two = 0
    three = 0
    four = 0

    if alert_dispositions != '':
        disp_arr = alert_dispositions.split(',')
        assets = Asset.objects.filter(parent=None, group_id__in=group_id, disposition_id__in=disp_arr)
        for plat_asset in assets:
            alerts = Alert.objects.filter(asset__platform=plat_asset, status=Statuses.OPEN.value)
            for alert in alerts:
                if alert.severity.id == 1:
                    one += 1
                elif alert.severity.id == 2:
                    two += 1
                elif alert.severity.id == 3:
                    three += 1
                else:
                    four += 1

    return JsonResponse({'alerts_arr': [four,three,two,one]})

def power_details(request, platform_id):
    platform = get_object_or_404(Asset, pk=platform_id)
    plots = []

    # Solar Panel Currents
    solar_panel_currents = Variable.objects.filter(asset=platform, erddap_id__iregex=r'solar_panel\d_current')
    if solar_panel_currents.count() > 0:
        plots.append(create_plot("Solar Panel Current", platform, solar_panel_currents))

    # Wind Turbine Currents
    wind_turbine_currents = Variable.objects.filter(asset=platform, erddap_id__iregex=r'wind_turbine\d_current')
    if wind_turbine_currents.count() > 0:
        plots.append(create_plot("Wind Turbine Current", platform, wind_turbine_currents))

    # Solar Panel Voltages
    solar_panel_voltages = Variable.objects.filter(asset=platform, erddap_id__iregex=r'solar_panel\d_voltage')
    if solar_panel_voltages.count() > 0:
        plots.append(create_plot("Solar Panel Voltage", platform, solar_panel_voltages))

    # Wind Turbine Voltages
    wind_turbine_voltages = Variable.objects.filter(asset=platform, erddap_id__iregex=r'wind_turbine\d_voltage')
    if wind_turbine_voltages.count() > 0:
        plots.append(create_plot("Wind Turbine Voltage", platform, wind_turbine_voltages))

    # Main Current
    main_current = Variable.objects.filter(asset=platform, erddap_id='main_current')
    if main_current.count() > 0:
        plots.append(create_plot("Main Current", platform, main_current))

    # Main Voltage
    main_voltage = Variable.objects.filter(asset=platform, erddap_id='main_voltage')
    if main_voltage.count() > 0:
        plots.append(create_plot("Main Voltage", platform, main_voltage))

    # Percent Charge
    percent_charge = Variable.objects.filter(asset=platform, erddap_id='percent_charge')
    if percent_charge.count() > 0:
        plots.append(create_plot("Percent Charge", platform, percent_charge))

    # Wind Velocities
    metbk = Asset.objects.filter(platform=platform, code__icontains='metbk').order_by('code').first()
    wind_velocities = Variable.objects.filter(asset=metbk, erddap_id__icontains='wind_velocity')
    if metbk and wind_velocities.count() > 0:
        plots.append(create_plot("Wind Velocity", metbk, wind_velocities))




    # The UI requires an ID on each plot, so add one
    for idx, plot in enumerate(plots):
        plot.id = idx + 1

    time_choices = [(e.value, e.name.title().replace('_', ' ')) for e in TimeChoices]
    plot_type_choices = [(e.value, e.name) for e in PlotTypes]
    yaxis_choices = [(e.value, e.name.title()) for e in YAxisChoices]

    plotclass = PlotClass.objects.all()
    plot_class_dict = {}
    for i in plotclass:
        plot_class_dict.setdefault(i.name, i.id)

    return render(request, 'dashboard/power_details.html', {
        'platform': platform,
        'plots': plots,
        'time_choices': time_choices,
        'plot_type_choices': plot_type_choices,
        'plot_class_dict': plot_class_dict,
        'yaxis_choices': yaxis_choices,
        'deployment': platform.deployment,
        'default_time_window': TimeChoices.LAST_7_DAYS.value,
    })

def create_plot(name, platform, variables):
    plot = Plot()
    plot.asset = platform
    plot.name = name
    plot.plot_class_id = PlotClasses.Timeseries.value
    plot.plot_type_id = PlotTypes.Line.value
    plot.user = None
    plot.is_global = False
    plot.yaxis_dir = False
    plot.y_var = list(variables.values_list('id', flat=True))

    return plot

