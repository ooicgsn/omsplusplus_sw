import dateutil.parser


class Plotting:
    def build_plot_data(self, asset, deployment_code, variable, x_var, y_var,
                        plot_type, raw_data):

        isList = isinstance(variable, list)
        if isList:
            title = ','.join([v.name or 'n/a' for v in variable])
            units = ','.join([v.units or 'n/a' for v in variable])
            variable_id = [v.id for v in variable]
            custom_variable = [v.is_custom for v in variable]
        else:
            title = variable.name
            units = variable.units
            variable_id = variable.id
            custom_variable = variable.is_custom

        if deployment_code is None:
            deployment_code = '---'

        data = raw_data['table']['rows']
        for row in data:
            row[0] = int(row[0]) * 1000

        return {
            'title': title,
            'subtitle': asset.platform.code + ': ' + asset.code,
            'parent_asset': asset.platform.code or asset.code,
            'present_asset': asset.code,
            'deployment_code': deployment_code,
            'units': units,
            'variable_id': variable_id,
            'custom_variable': custom_variable,
            'plot_type': plot_type,
            'x_var': x_var,
            'y_var': y_var,
            'data': data,
            'errors': raw_data.get('errors', []),
            'interp_values': raw_data['interp_values']
        }

    def build_l3_plot(self, raw_data, **kwargs):
        """Method for building L3 variable preview responses"""
        plot_props = {}
        for key in kwargs:
            plot_props[key] = kwargs[key]

        data = raw_data['table']['rows']
        for row in data:
            row[0] = int(row[0]) * 1000

        return {
            'title': 'L3 Variable Preview',
            'subtitle': 'L3 Preview',
            'parent_asset': plot_props['parent_asset'] or '',
            'present_asset': plot_props['present_asset'] or '',
            'deployment_code': plot_props['deployment_code'] or '',
            'units': plot_props['units'] or '',
            'plot_type': plot_props['plot_type'] or 'Timeseries',
            'custom_variable': plot_props['custom_variable'] or True,
            'x_var': 'time',
            'y_var': plot_props['y_var'] or 'L3 Plot Preview',
            'data': data,
            'errors': []
        }
