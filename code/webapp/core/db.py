from django.db import connection


def exec_raw(function_name, parameters):
    """ Executes a database function and returns data exactly how we get it from the driver used """
    with connection.cursor() as cursor:
        cursor.callproc(function_name, parameters)

        return cursor.fetchall()

def exec_dict(function_name, parameters):
    """ 
    Executes a database function and returns data s a dictionary
     See: https://docs.djangoproject.com/en/1.10/topics/db/sql/
     Note: This call will be memory intensive for large queries, since all data has to be pushed into
       memory. It will also be bloated as the key names are stored in the dict, adding to the overall size
    """
    with connection.cursor() as cursor:
        cursor.callproc(function_name, parameters)

        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row)) for row in cursor.fetchall()
        ]

def exec_void(function_name, parameters):
    """ 
    Executes a database function without expecting any results
     See: https://docs.djangoproject.com/en/1.10/topics/db/sql/
    """
    with connection.cursor() as cursor:
        cursor.callproc(function_name, parameters)

def exec_scalar(function_name, parameters):
    """ Returns a single value from a database function, or None if nothing was returned """
    with connection.cursor() as cursor:
        cursor.callproc(function_name, parameters)

        row = cursor.fetchone()
        return row[0]
