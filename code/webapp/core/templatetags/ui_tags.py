from collections import deque
from munch import Munch as Bunch

from django import template
from django.conf import settings
from django.forms.models import model_to_dict
from livesettings.functions import config_value

from core.models.static import AssetPages
from ..models.asset import Asset, Group
from ..util import check_perm

register = template.Library()


@register.inclusion_tag(name='sidebar_assets', filename='core/templatetags/_sidebar_assets.html')
def get_sidebar_groups():
    """
    Transforms the single data table coming from the database call into a hierarchical
    view needed for the template
    """
    data = Asset().get_sidebar_assets()
    datagliders = Group.objects.filter(has_gliders=True)

    assets = []
    group = None

    for row in data:
        # This assumes the list coming from the database is sorted by group name
        if not group or row['group_name'] != group.name:
            group = Bunch({
                'id': row['group_id'],
                'name': row['group_name'],
                'is_glider': row['group_is_glider'],
                'columns': {
                    "left": [],
                    "right": []
                }
            })
            assets.append(group)

        if row['asset_id']:
            asset = Asset(
                id=row['asset_id'],
                status_id=row['status_id'])

            item = Bunch({
                'name': row['name'],
                'id': row['asset_id'],
                'code': row['code'],
                'deployment': row['deployment'],
                'css_class': asset.sidebar_css_class,
            })

            if int(row["sidebar_column"]) == 2:
                group.columns["right"].append(item)
            else:
                group.columns["left"].append(item)
            #group.columns.append()

    return {'assets': assets, 'datagliders': datagliders}


@register.inclusion_tag(name='asset_nav', filename='core/templatetags/_asset_nav.html', takes_context=True)
def get_asset_nav(context, asset_id, deployment_code, plot_pages, current_asset_page):
    breadcrumbs = deque()

    current_asset = Asset.objects.get(pk=asset_id)
    asset = current_asset
    platform = asset.platform or asset
    disposition = platform.disposition.name

    while asset:
        breadcrumbs.appendleft(asset)
        asset = asset.parent

    all_deployments = Asset.objects\
        .filter(name=platform.name)\
        .select_related('deployment')

    related_assets = Asset.objects\
        .filter(name=current_asset.name, platform__name=platform.name)\
        .select_related('deployment')

    if current_asset.parent:
        related_assets = related_assets.filter(parent__name=current_asset.parent.name)

    related_assets = [related_assets.get(deployment__code=a.deployment.code) if related_assets.filter(deployment__code=a.deployment.code).exists() else a for a in all_deployments] 
        
    related_assets = sorted(related_assets, key=lambda a: a.deployment.sortable_code)

    depth = round(current_asset.depth) if current_asset.depth else None
    is_purgable = current_asset.type_id != 1 and not Asset.objects.filter(parent_id=asset_id).exists()

    return {
        'asset_id': asset_id,
        'type_id': current_asset.type_id,
        'platform': platform,
        'group': platform.group,
        'breadcrumbs': breadcrumbs,
        'related_assets': related_assets,
        'deployment_code': deployment_code,
        'data_dir': current_asset.raw_data_dir,
        'photo_url': platform.photo_url,
        'plot_pages': plot_pages,
        'current_asset_page': current_asset_page,
        'asset_overview': AssetPages.ASSET_OVERVIEW.value,
        'plot_page': AssetPages.PLOT_PAGE.value,
        'create_plot_page': AssetPages.CREATE_PLOT_PAGE.value,
        'create_manual_alert': AssetPages.CREATE_MANUAL_ALERT.value,
        'erddap_id': current_asset.erddap_id,
        'deployment_date': current_asset.deployment_date,
        'recovery_date': current_asset.recovery_date,
        'lat': current_asset.lat,
        'lon': current_asset.lon,
        'depth': depth,
        'disposition': disposition,
        'erddap_perm': check_perm(context['request'], 'manage_erddap_id'),
        'is_superuser': context['request'].user.is_superuser,
        'is_purgable': is_purgable,
    }


@register.inclusion_tag(name='asset_picker', filename='core/templatetags/_asset_picker.html')
def asset_picker(id):
    return {'id': id}


@register.inclusion_tag(name='asset_picker_scripts', filename='core/templatetags/_asset_picker_scripts.html')
def asset_picker_scripts(id, related_id=None):
    return {'id': id, 'related_id':related_id}


@register.simple_tag
def erddap_url():
    return config_value('ERDDAP', 'SERVER_URL')


@register.filter
def asset_fullname(asset):
    # Builds name of asset with parents in the following format:
    # platform > parent1 > parent2 > ... > asset
    if 'asset_id' in asset:
        asset_obj = Asset.objects.get(pk=asset['asset_id'])
    else:
        asset_obj = Asset.objects.get(pk=asset['id'])

    fullname = asset["name"]

    if asset["parent_id"] is not None:
        # If asset has parent elements then recursively traverse up the tree to build full name
        # (vpeou): If this ends up being too inefficient we'll have to go with a SQL sproc
        fullname = '</span>' + fullname
        asset = model_to_dict(asset_obj
                              )
        while asset["parent"] is not None:
            asset_obj = Asset.objects.get(pk=asset["parent"])
            asset = model_to_dict(asset_obj)

            if asset_obj.parent:
                fullname = '{} > {}'.format(asset["name"], fullname)
            else:
                fullname = '{} ({}) > {}'.format(asset['name'], asset_obj.deployment.code, fullname)
        fullname = '<span style="font-weight:normal">' + fullname
    else:
        # If asset doesn't have any parents then just display it's name
        if not asset_obj.parent:
            fullname += ' ({})'.format(asset_obj.deployment.code)

    return fullname


@register.simple_tag
def app_version():
    return settings.VERSION
