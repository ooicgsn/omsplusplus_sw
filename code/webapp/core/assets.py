from .models.asset import Asset

# Strips asset name to retrieve only the alpha character prefix
def strip_asset_name(asset_name):
    # stripped_asset_name = ''.join(i for i in asset_name if not i.isdigit())
    index_of_first_number = 0
    for i in asset_name:
        if i.isdigit():
            break
        else:
            index_of_first_number += 1
    return asset_name[:index_of_first_number]
