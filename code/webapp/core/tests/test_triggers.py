from django.test import TestCase
from core.models.alert_trigger import Trigger

class TestTrigger(TestCase):

    def test_split_logic(self):
        """
        Tests the Trigger._split_trigger_logic method to ensure it breaks up
        logical operators correctly
        """

        # helper function to aid in comparison
        def groups_helper(expr):
            return Trigger._split_trigger_logic(expr).groups()

        self.assertEqual(groups_helper('a + 222 > 555'), ('a + 222 ', '> 555'))
        self.assertEqual(groups_helper('a + 222 < 555'), ('a + 222 ', '< 555'))
        # test equality variants
        self.assertEqual(groups_helper('a + 222 = 555'), ('a + 222 ', '= 555'))
        self.assertEqual(groups_helper('a + 222 == 555'),
                                       ('a + 222 ', '== 555'))
        self.assertEqual(groups_helper('a + 222 != 555'),
                                       ('a + 222 ', '!= 555'))
        # invalid
        self.assertRaises(AttributeError, groups_helper, 'a + 222 === 555')
        # bit shift operators should not match
        self.assertRaises(AttributeError, groups_helper, 'a + 222 >> 22')
        self.assertRaises(AttributeError, groups_helper, 'a + 222 << 22')
        # multiple logical operands should only grab the last logical operator
        self.assertEqual(groups_helper('b > c != bool_var'),
                                       ('b > c ', '!= bool_var'))
        # Mike Chagnon's example with bit shifting in predicate
        self.assertEqual(groups_helper('H > x >> 1'), ('H ', '> x >> 1'))
