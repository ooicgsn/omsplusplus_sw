from .models.static import Severities

def get_alert_css_class(severity_id):
    if severity_id == Severities.LOW.value:
        return 'alert-summary-low'
    elif severity_id == Severities.MEDIUM.value:
        return 'alert-summary-medium'
    elif severity_id == Severities.HIGH.value:
        return 'alert-summary-high'
    elif severity_id == Severities.ALARM.value:
        return 'alert-summary-alert'

def get_alert_bar_css_class(severity_id):
    if severity_id == Severities.LOW.value:
        return 'alert-bar-low'
    elif severity_id == Severities.MEDIUM.value:
        return 'alert-bar-medium'
    elif severity_id == Severities.HIGH.value:
        return 'alert-bar-high'
    elif severity_id == Severities.ALARM.value:
        return 'alert-bar-alert'
