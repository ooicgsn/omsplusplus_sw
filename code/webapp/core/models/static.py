from enum import Enum

from django.db import models


class BoolChoices(Enum):
    YES = True
    NO = False

class YAxisChoices(Enum):
    ASCENDING = False
    DESCENDING = True

class PlotTypes(Enum):
    Line = 1
    Scatter = 2

class PlotClasses(Enum):
    Timeseries = 1
    Profile = 2
    Comparison = 3

class TimeChoices(Enum):
    LAST_24_HOURS = -1
    LAST_2_DAYS = -2
    LAST_3_DAYS = -3
    LAST_7_DAYS = -7
    LAST_30_DAYS = -30
    LAST_6_MONTHS = -182
    LAST_YEAR = -365

class Statuses(Enum):
    OPEN = 1
    RESOLVED = 4


class DeliveryMethods(Enum):
    EMAIL = 1
    TEXT = 2
    SUMMARY = 3


class DurationCodes(Enum):
    MINUTES = 1
    HOURS = 2
    CURRENT_DEPLOYMENT = 3
    SECONDS = 4

class L3Duration(Enum):
    SECONDS = 1
    MINUTES = 2
    HOURS = 3

class Severities(Enum):
    LOW = 1
    MEDIUM = 2
    HIGH = 3
    ALARM = 4

class DEFAULT_PAGE(Enum):
    ASSET = 2
    GROUP = 3
    DASHBOARD = 1

class AssetPages(Enum):
    ASSET_OVERVIEW = 1
    PLOT_PAGE = 2
    CREATE_PLOT_PAGE = 3
    CREATE_MANUAL_ALERT = 4

class IntervalTimes(Enum):
    SECONDS = 60
    MINUTES = 3600


class Status(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'alert_status'


class DeliveryMethod(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'alert_delivery_method'


class DurationCode(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'alert_duration_code'


class Severity(models.Model):
    name = models.CharField(max_length=100)
    css_class = models.CharField(max_length=100)

    # TODO(mchagnon): This field can be removed; was only used for status calculations, and we can use multiplier for that
    level = models.PositiveSmallIntegerField()
    multiplier = models.DecimalField(max_digits=6, decimal_places=3, default=0)

    class Meta:
        db_table = 'alert_severity'

class Page(models.Model):
    name = models.CharField(max_length=100)
    route = models.CharField(max_length=100)

    class Meta:
        db_table = 'page'

class TriggerCategory(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'alert_trigger_category'