from enum import Enum

from django.db import models
from django.contrib.auth.models import User
from .alert_trigger import Trigger
from .static import Statuses, Status, Severities, Severity, DeliveryMethod, DeliveryMethods
from ..models.asset import Asset, Deployment
from ..ui import get_alert_bar_css_class, get_alert_css_class
from ..db import exec_dict, exec_scalar, exec_void
from ..util import enum_choices


class Alert(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    trigger = models.ForeignKey(Trigger, null=True, blank=True)
    status = models.ForeignKey(Status, limit_choices_to=enum_choices(Statuses))
    operational_pct = models.DecimalField(decimal_places=3, max_digits=6, default=0)
    severity = models.ForeignKey(Severity, limit_choices_to=enum_choices(Severities), null=True, blank=True)
    asset = models.ForeignKey(Asset)
    deployment = models.ForeignKey(Deployment)
    details = models.TextField(null=True, blank=True)
    is_manual = models.BooleanField(null=False, blank=False, default=False)
    name = models.CharField(max_length=100, blank=True, null=True)
    last_occurrence = models.DateTimeField(null=True, blank=True)
    roll_up = models.BooleanField(default=False)
    roll_down = models.BooleanField(default=False)
    redmine_id = models.IntegerField(null=True, blank=True)

    def get_hotlist_alerts(self, user_id):
        data = exec_dict('get_hotlist_alerts', (user_id,))

        return [dict(row, css_class=get_alert_bar_css_class(row['severity_id'])) for row in data]

    def get_highest_alert_severity_for_asset(self, asset_id):
        return exec_scalar('get_highest_alert_severity_for_asset', (asset_id,))

    def add_alert_notifications(self, alert_id):
        exec_void('add_alert_notifications', (alert_id,))

    # TODO(mchagnon): We could use a better name than "value"
    def get_status_from_value(self, value):
        return exec_scalar('get_status_from_value', (value,))

    @property
    def alert_css_class(self):
        return get_alert_css_class(self.severity.id)

    @property
    def bar_css_class(self):
        return get_alert_bar_css_class(self.severity.id)

    class Meta:
        db_table = 'alert'

    def get_pending_notifications_for_digest(self, selected_date=None):
        if selected_date is None:
            return exec_dict('get_pending_notifications_for_digest', ())
        else:
            return exec_dict('get_pending_notifications_for_digest', (selected_date,))

    @staticmethod
    def delete(alert_id):
        exec_void("delete_alert", (alert_id, ))


class Note(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    alert = models.ForeignKey(Alert)
    user = models.ForeignKey(User)
    details = models.TextField()

    class Meta:
        db_table = 'alert_note'


class Occurrence(models.Model):
    created = models.DateTimeField()
    alert = models.ForeignKey(Alert)

    # TODO(mchagnon): Deprecated; needs to be removed once any code using it is removed
    triggered_value = models.TextField()

    class Meta:
        db_table = 'alert_occurrence'

class OccurrenceItem(models.Model):
    occurrence = models.ForeignKey(Occurrence)
    occurred_at = models.DateTimeField()

    # TODO: We might need to come back to this and update the precision/length
    value = models.DecimalField(decimal_places=8, max_digits=18)

    class Meta:
        db_table = 'alert_occurrence_item'


class History(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    alert = models.ForeignKey(Alert)
    severity = models.ForeignKey(Severity, limit_choices_to=enum_choices(Severities), null=True, blank=True)
    user = models.ForeignKey(User)

    # TODO(mchagnon): flush this field out
    triggered_value = models.TextField()

    class Meta:
        db_table = 'alert_history'


class Notification(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    alert = models.ForeignKey(Alert)
    user = models.ForeignKey(User)
    has_acknowledged = models.BooleanField(default=False)
    delivery_method = models.ForeignKey(DeliveryMethod, limit_choices_to=enum_choices(DeliveryMethods), null=True, blank=True)

    class Meta:
        db_table = 'alert_notification'


class AlertStatuses(Enum):
    OPEN = 1
    RESOLVED = 4

