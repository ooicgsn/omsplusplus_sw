from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    external_id = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_project'


class Tracker(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    external_id = models.IntegerField(null=False, blank=False)
    project = models.ForeignKey(Project, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_tracker'

class Priority(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    external_id = models.IntegerField(null=False, blank=False)
    is_default = models.BooleanField(default=False, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_priority'

class Organization(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_organization'


class Location(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_location'


class Condition(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_condition'


class Platform(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_platform'

class TrackerField(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    tracker = models.ForeignKey(Tracker, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_tracker_field'

class CustomField(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    external_id = models.IntegerField(null=False, blank=False)
    
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'redmine_custom_field'