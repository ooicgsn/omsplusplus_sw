from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from enum import Enum
from .static import Page
from .asset import Asset, Group

class MobileCarrier(models.Model):
    name = models.CharField(max_length=100)
    email_template = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = 'mobile_carrier'

    def get_email(self, phone):
        return self.email_template.format(phone)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    position = models.CharField(max_length=100, null=True, blank=True)
    affiliation = models.CharField(max_length=100, null=True, blank=True)
    default_page = models.ForeignKey(Page, blank=True, null=True)
    default_asset = models.ForeignKey(Asset, blank=True, null=True)
    default_group = models.ForeignKey(Group, blank=True, null=True)
    redmine_api_key = models.CharField("Redmine API Key", max_length=50, blank=True, null=True)
    sms_number = models.CharField(max_length=10, null=True, blank=True)
    sms_carrier = models.ForeignKey(MobileCarrier, null=True, blank=True)
    overview_functional_counts = models.BooleanField(default=True, null=False, blank=False)
    overview_functional_percentage = models.BooleanField(default=True, null=False, blank=False)

    class Meta:
        db_table = 'user_profile'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
#