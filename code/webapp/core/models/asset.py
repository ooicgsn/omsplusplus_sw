import re, os
from enum import Enum
import datetime
import pytz

from django.db import models
from django.db.models import Avg
from django.contrib.auth.models import User
from django.conf import settings
from livesettings.functions import config_value
from collections import deque

from ..db import exec_dict, exec_void, exec_raw
from ..util import enum_choices, DataTablesResponse, parse_deployment_number, parse_deployment_code_leading_zeros
from ..ui import get_alert_bar_css_class

import pandas as pd

# TODO(mchagnon): break this file into multiple model classes
# TODO(mchagnon): Class names here use "asset" prefix, but not in the alert models...need to be consistent


class AssetGroups(Enum):
    GLOBAL = 1
    PIONEER = 2
    ENDURANCE = 3
    AUV = 4
    GLIDERS = 5


# TODO(mchagnon): these need to be flushed out mode
class AssetStatuses(Enum):
    OK = 1
    WARNING = 2
    ERROR = 3


class AssetTypes(Enum):
    PLATFORM = 1
    CPM = 2
    DCL = 3
    INSTRUMENT = 4
    BUOY = 7
    NSIF = 8
    MFN = 9
    PORT = 10
    STC = 11

class AssetDispositions(Enum):
    BURN_IN = 1
    DEPLOYED = 2
    RECOVERED = 3
    INACTIVE = 4


class Group(models.Model):
    name = models.CharField(max_length=50)
    sort_order = models.SmallIntegerField()

    # Glider is both a type of mooring and a top level group. This flag is the group that all gliders are attached to
    is_glider = models.BooleanField(default=False)
    has_gliders = models.BooleanField(default=False)

    # Determine whether there are any gliders on this group. For example, Pioneer has gliders and would get this flag,
    #   even though the gliders themselves are attached to the "gliders" asset group
    has_gliders = models.BooleanField(default=False)

    class Meta:
        db_table = 'asset_group'

    def __str__(self):
        return self.name


class AssetStatus(models.Model):
    name = models.CharField(max_length=50)
    min_threshold = models.DecimalField(decimal_places=0, max_digits=3, default=0)
    max_threshold = models.DecimalField(decimal_places=0, max_digits=3, default=0)

    # TODO(mchagnon): Does this class needed a css_class field?

    class Meta:
        db_table = 'asset_status'


class AssetType(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'asset_type'


class AssetDisposition(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'asset_disposition'


class AssetCategory(models.Model):
    name = models.CharField(max_length=100)
    sidebar_column = models.IntegerField(default=1, null=False, blank=False)

    class Meta:
        db_table = 'asset_category'

class AssetSubcategory(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(AssetCategory)

    class Meta:
        db_table = 'asset_subcategory'


class Deployment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)
    deployment_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    withdraw_date = models.DateTimeField(blank=True, null=True)
    withdraw_reason = models.TextField(blank=True, null=True)

    # Returns the deployment number as an integer so that it can be used for sorting
    @property
    def sortable_code(self):
        return parse_deployment_code_leading_zeros(self.code)

    class Meta:
        db_table = 'deployment'


class Asset(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    last_update = models.DateTimeField(null=True, blank=True)
    group = models.ForeignKey(Group)
    platform = models.ForeignKey('Asset', null=True, blank=True, related_name='+')
    parent = models.ForeignKey('Asset', null=True, blank=True, related_name='+')
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)
    type = models.ForeignKey(AssetType, limit_choices_to=enum_choices(AssetTypes))
    status = models.ForeignKey(AssetStatus, limit_choices_to=enum_choices(AssetStatuses))
    status_reason = models.TextField(blank=True, null=True)
    status_reported_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    status_is_manually_set = models.BooleanField(default=False)
    erddap_id = models.CharField(max_length=50, null=True, blank=True)
    deployment = models.ForeignKey(Deployment, null=True, blank=True)

    deployment_date = models.DateTimeField(blank=True, null=True)
    recovery_date = models.DateTimeField(blank=True, null=True)

    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)

    depth = models.FloatField(null=True)



    # Decimal based percentage from 0 to 1
    status_percent = models.DecimalField(decimal_places=2, max_digits=3, default=1.00, null=True, blank=True)

    # Port number, which is needed for the instrument level of an asset (under a DCL)
    port = models.PositiveSmallIntegerField(null=True, blank=True)

    photo = models.CharField(max_length=255, null=True, blank=True)

    # Technically this is a status on the deployment, but it makes more sense to keep it on the asset since it
    #   it will make database calls easier
    disposition = models.ForeignKey(AssetDisposition, null=False, blank=False, default=AssetDispositions.DEPLOYED.value)

    category = models.ForeignKey(AssetCategory, null=True, blank=True)
    subcategory = models.ForeignKey(AssetSubcategory, null=True, blank=True)

    # Flag set when an instrument is removed from mooring during its deployment
    is_deleted = models.BooleanField(default=False, null=False, blank=False)

    # TODO(mchagnon): This is a fixed amount for now. Does this need to be a cfg setting? We're relying on a constant
    # TODO(mchagnon): number for all DCL's in the UI currently
    NUM_PORTS_PER_DCL = 8

    # Store a specific ID of the GPs instrument on the asset (mainly for the top level platform)
    gps = models.ForeignKey('Asset', null=True, blank=True, related_name='+')

    # Even though gliders are all assigned to a top level group of "glider", we still need to determine which array
    #   this asset is part of
    glider_group = models.ForeignKey(Group, null=True, blank=True, related_name='+')

    class Meta:
        db_table = 'asset'

    def __str__(self):
        return self.name


    def get_sidebar_assets(self):
        return exec_dict('get_sidebar_assets', None)

    def get_asset_status_grid(self, asset_id):
        return exec_dict('get_asset_status_grid', (asset_id,))

    def get_asset_status_history(self, asset_id):
        return exec_dict('get_asset_status_history', (asset_id,))

    def get_asset_name(self):
        last_id = self.id
        name_list = deque([self.code])
        # platforms are their own platform ids
        cur_asset = self.platform

        while last_id != cur_asset.id or cur_asset is None:
            name_list.appendleft(cur_asset.code)
            last_id = cur_asset.id
            cur_asset = cur_asset.platform

        return '-'.join(name_list)

    def rebuild_asset_links(self, asset_id):
        exec_void('rebuild_asset_links', (asset_id,))

    def get_alerts(self, asset_id, include_all):
        alerts = []

        for alert in exec_dict('get_asset_alerts', (asset_id, include_all)):
            alert["row_css_class"] = get_alert_bar_css_class(alert["severity_id"])
            alerts.append(alert)

        return DataTablesResponse(alerts)

    def get_asset_alerts(self, asset_id, include_all):
        # TODO(mchagnon): This needs to be cleaned up; remove hard coded 6 for the position of severity ID
        data = exec_raw('get_asset_alerts', (asset_id, include_all))
        data = [row + (get_alert_bar_css_class(row[6]),) for row in data]
        return DataTablesResponse(data)

    def get_assets_for_status_grid(self, platform_id):
        return exec_dict('get_assets_for_status_grid', (platform_id,))

    def get_group_platforms(self, group_id):
        return exec_dict('get_group_platforms', (group_id,))

    def get_assets_for_pulling_last_update(self):
        return exec_dict('get_assets_for_pulling_last_update', None)

    def get_clone_trigger_assets(self, trigger_id):
        return exec_dict('get_clone_trigger_assets', (trigger_id,))

    def get_clone_variable_assets(self, variable_id):
        return exec_dict('get_clone_variable_assets', (variable_id,))

    def get_clone_plot_assets(self, plot_id):
        return exec_dict('get_clone_plot_assets', (plot_id,))

    def get_asset_severity_multipliers(self, asset_id):
        return [float(row[0]) for row in exec_raw('get_asset_severity_multipliers', (asset_id,))]



    @property
    def status_grid_css_class(self):
        return Asset.get_status_grid_class(self.status_id, self.type_id)

    @staticmethod
    def get_status_grid_class(status_id, type_id):
        css_class = 'status'

        if status_id == AssetStatuses.OK.value:
            css_class += ' status status-ok'
        elif status_id == AssetStatuses.WARNING.value:
            css_class += ' status-warning'
        elif status_id == AssetStatuses.ERROR.value:
            css_class += ' status-error'

        if type_id in (AssetTypes.CPM.value, AssetTypes.BUOY.value, AssetTypes.NSIF.value, AssetTypes.MFN.value):
            css_class += ' status-grid-cpm'
        elif type_id in (AssetTypes.DCL.value, AssetTypes.STC.value):
            css_class += ' status-grid-dcl'

        return css_class

    @property
    def is_data_recent(self):
        # use 24 hour threshold
        present_utc_time = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
        asset_updated_time = self.last_update or datetime.datetime.min.replace(tzinfo=pytz.UTC)
        time_dif = (present_utc_time - asset_updated_time).total_seconds() < 86400
        return time_dif

    @property
    def sidebar_css_class(self):
        if self.status_id == AssetStatuses.OK.value:
            return 'circle-status-ok'
        elif self.status_id == AssetStatuses.WARNING.value:
            return 'circle-status-warning'
        elif self.status_id == AssetStatuses.ERROR.value:
            return 'circle-status-error'

        return 'status'

    # TODO(mchagnon): somewhat temporary until we find out how to link the photo to a mooring (using the db field)
    @property
    def photo_url(self):
        platform = self.platform or self

        return settings.DRAWINGS_URL + platform.code + '.png'

    @property
    def erddap_data_url(self):
        erddap_url = config_value('ERDDAP', 'SERVER_URL')

        return "{}/tabledap/{}.html".format(erddap_url, self.erddap_id)

    @property
    def erddap_graph_url(self):
        erddap_url = config_value('ERDDAP', 'SERVER_URL')

        return "{}/tabledap/{}.graph".format(erddap_url, self.erddap_id)

    # TODO(mchagnon): This could be cleaned up to prevent lost of database calls, since this is called within a loop
    @property
    def average_platform_status(self):
        assets = Asset.objects.filter(platform=self.platform)
        average_status = assets.aggregate(Avg('status_percent'))['status_percent__avg']

        if assets.count() == 0:
            return 0

        total = float(0.0)
        for asset in assets:
            total += float(100.0) if asset.status_percent is None else float(asset.status_percent) * 100.0

        return total / assets.count()

    @property
    def raw_data_dir(self):
        return Asset.get_data_url(self.platform.code, self.deployment.code)

    @staticmethod
    def get_data_url(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        return os.path.join(config_value('DATA_DIR', 'URL'), location) if location else ""

    @staticmethod
    def get_data_path(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        return os.path.join(settings.LOCAL_DATA_DIR, location) if location else ""

    @staticmethod
    def get_config_url(platform_code, deployment_code):
        location = Asset._get_config_location(platform_code, deployment_code)
        return os.path.join(config_value('DATA_DIR', 'URL'), location) if location else ""

    @staticmethod
    def get_config_path(platform_code, deployment_code):
        location = Asset._get_config_location(platform_code, deployment_code)
        return os.path.join(settings.LOCAL_DATA_DIR, location) if location else ""

    @staticmethod
    def _get_data_location(platform_code, deployment_code):
        local_path = os.path.join(settings.LOCAL_DATA_DIR, platform_code.lower(), deployment_code.upper())
        if not os.path.exists(local_path):
            return ""

        return os.path.join(platform_code.lower(), deployment_code.upper())

    @staticmethod
    def _get_config_location(platform_code, deployment_code):
        location = Asset._get_data_location(platform_code, deployment_code)
        if not location:
            return ""

        # There are two potential locations for a config file:
        #   cfg_files/platform.cfg
        #   cg_data/cfg_files/platform.cfg
        config_location = os.path.join(location, "cfg_files", "platform.cfg")
        if os.path.isfile(os.path.join(settings.LOCAL_DATA_DIR, config_location)):
            return config_location

        config_location = os.path.join(location, "cg_data", "cfg_files", "platform.cfg")
        if os.path.isfile(os.path.join(settings.LOCAL_DATA_DIR, config_location)):
            return config_location

        return ""



class AssetLink(models.Model):
    asset = models.ForeignKey(Asset, related_name='links')
    linked_asset = models.ForeignKey(Asset, related_name='linked_asset')

    # Due to the way this model is used, assets will be linked to themselves as both the
    #   parent and child. This is to allow calls to get parents for status changes to include
    #   the asset, as well as allowing calls to get children for variables to plot to also
    #   include them
    is_parent = models.BooleanField(null=False, blank=False, default=True)
    is_child = models.BooleanField(null=False, blank=False, default=False)

    class Meta:
        db_table = 'asset_link'


class AssetProperty(models.Model):
    asset = models.ForeignKey(Asset)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'asset_property'


class Cruise(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    notes = models.TextField(blank=True, null=True)
    log_notes = models.TextField(blank=True, null=True)
    ship_name = models.CharField(max_length=100, blank=True, null=True)

    # TODO(mchagnon): Use 'date' prefix in variable name, or use "started" and "ended"
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cruise'


class CruiseDeployment(models.Model):
    cruise = models.ForeignKey(Cruise, on_delete=models.CASCADE)
    deployment = models.ForeignKey(Deployment, on_delete=models.CASCADE)

    class Meta:
        db_table = 'cruise_deployment'


class CruiseDocument(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    cruise = models.ForeignKey(Cruise, on_delete=models.CASCADE)
    user = models.ForeignKey(User)
    notes = models.TextField()

    # TODO(mchagnon): one more field is needed for either the document contents or the file name
    # content = models.BinaryField()
    # filename = models.CharField(max_length=255)

    class Meta:
        db_table = 'cruise_document'


class UserAsset(models.Model):
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_asset'


class StatusHistory(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    asset = models.ForeignKey(Asset)
    status_id = models.IntegerField(choices=enum_choices(AssetStatuses))
    prev_status_id = models.IntegerField(choices=enum_choices(AssetStatuses), blank=True, null=True)
    reason = models.TextField()

    class Meta:
        db_table = 'asset_history'

# TODO(mchagnon): According to the initial database diagram, we have a table for coefficient data. Further discussion
# TODO(mchagnon): needed to see if we do need this table and/or which Django app it goes in

class AssetLocation(models.Model):
    asset = models.ForeignKey(Asset, blank=False, null=False)
    location_date = models.DateTimeField()
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    class Meta:
        db_table = 'asset_location'
