from django.db import models

from ..models.asset import Asset, Deployment
from ..models.account import User
from ..db import exec_dict


class Variable(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    is_custom = models.BooleanField(default=False)
    asset = models.ForeignKey(Asset, related_name='variable')
    deployment = models.ForeignKey(Deployment, null=True, blank=True)
    erddap_id = models.CharField(max_length=100)
    units = models.CharField(max_length=50, null=True, blank=True)
    platform = models.ForeignKey(Asset, null=True, blank=True, related_name='+')
    is_plottable = models.BooleanField(null=False, blank=False, default=True)
    interpolation_amount = models.DurationField(null=True, blank=True)
    change_interval = models.DurationField(null=True, blank=True)
    is_global = models.BooleanField(null=False, blank=False, default=True)
    user = models.ForeignKey(User, null=True, blank=True)

    # TODO(mchagnon): Expression needs to be flushed out further
    expression = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'variable'

    def __str__(self):
        return self.name

    def get_asset_variables(self, asset_id, user_id):
        return exec_dict('get_asset_variables', (asset_id, user_id,))

    def get_usage(self):
        return [item["name"] for item in exec_dict("get_variable_usage", (self.id, ))]


class VariableComponent(models.Model):
    # TODO(mchagnon): The related_name fields may not be correct
    parent = models.ForeignKey(Variable, related_name='components')
    child = models.ForeignKey(Variable, related_name='childVariable')
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'variable_component'
