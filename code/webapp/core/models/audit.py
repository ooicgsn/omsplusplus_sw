from enum import Enum

from django.db import models
from django.contrib.auth.models import User

from asset.models import Asset
from ..util import enum_choices

# TODO(mchagnon): Standardize naming convention for enums and backing tables

class AuditCodes(Enum):
    LOGIN = 1
    LOGOUT = 2


class Code(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'audit_code'


class Audit(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    code = models.ForeignKey(Code, limit_choices_to=enum_choices(AuditCodes))

    class Meta:
        db_table = 'audit'


class OperatorLog(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    log_date = models.DateTimeField()
    user = models.ForeignKey(User)
    asset =  models.ForeignKey(Asset)
    title = models.CharField(max_length=100)
    notes = models.TextField()

    class Meta:
        db_table = 'operator_log'
