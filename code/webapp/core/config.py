from livesettings.functions import config_register
from livesettings.values import ConfigurationGroup, StringValue, PasswordValue, BooleanValue

CORE_GROUP = ConfigurationGroup('ERDDAP', 'ERDDAP Settings', ordering=0)

config_register(StringValue(
    CORE_GROUP,
    'SERVER_URL',
    description='Server Url',
    help_text='e.g. https://<site>.edu/erddap',
    default='https://cgoms.coas.oregonstate.edu/erddap',
    ordering=0
))

config_register(StringValue(
    CORE_GROUP,
    'USERNAME',
    description='Username',
    ordering=1
))

config_register(PasswordValue(
    CORE_GROUP,
    'PASSWORD',
    description='Password',
    ordering=2
))

LAPTOP_GROUP = ConfigurationGroup('LAPTOP', 'Laptop Mode Settings', ordering=0)

config_register(BooleanValue(
    LAPTOP_GROUP,
    'LAPTOP_MODE',
    description='Laptop Mode',
    default=False,
    ordering=1
))


DATA_DIRECTORY_GROUP = ConfigurationGroup('DATA_DIR', 'Data Directory Settings', ordering=1)

config_register(StringValue(
    DATA_DIRECTORY_GROUP,
    'URL',
    description="Data Directory Url",
    help_text='e.g. https://[domain]/raw/',
    ordering=0
))

REDMINE_GROUP = ConfigurationGroup('REDMINE', 'Redmine Integration Settings', ordering=2)

config_register(StringValue(
    REDMINE_GROUP,
    'SERVER_URL',
    description='Redmine Url',
    help_text='Url used to connect to the Redmine instance',
    default='https://redmine.oceanobservatories.org',
    ordering=0
))

config_register(BooleanValue(
    REDMINE_GROUP,
    'USE_LIVE_PROJECTS',
    description='Use Live Redmine Projects',
    default=False,
    ordering=1,
    help_text='If disabled, the test projects will be used. After changing this settings, you must run the "update_redmine" Django command'
))