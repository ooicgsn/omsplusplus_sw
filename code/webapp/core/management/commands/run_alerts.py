from django.core.management.base import BaseCommand

# Prevent's future warnings from Pandas, which are cluttering up the output/logging
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

from tasks.run_alerts import process_triggers


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        process_triggers()
