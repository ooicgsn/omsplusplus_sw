import sys
from django.core.management.base import BaseCommand

from core.models.asset import Asset
from core.erddap import Erddap

import pandas as pd

class Command(BaseCommand):
    help = 'Updates all assets with the latest modified date from ERDDAP'

    def add_arguments(self, parser):
        parser.add_argument('--relative-to', choices=['now', 'max_time',
                                                      'none'],
                            default='max_time',
                            help="One of now, maxtime, or None. If maxtime "
                                 "(default) search from the maximum time in the"
                                 " dataset.  If now, search from the current "
                                 " time backwards.  If None, search all time "
                                 " values, which ensures an accurate result, "
                                 " but is much slower")

        parser.add_argument('--days', type=int, default=1,
                            help='If --relative-to is not "none", the number of'
                                 ' days to search backwards for the last update'
                                 ' time.  Defaults to 1, is ignored if '
                                 ' --relative-to is "none"')


    def handle(self, *args, **options):
        # TODO(mchagnon): The stored proc for this has a hard coded clause to exclude variables with an erddap_id
        # TODO(mchagnon):   of 'feature_type_instance'; this should really be filtered out of the load scripts
        return_stat = 0
        erd = Erddap()
        # TODO: (badams) make async to improve throughput
        relative_to = (None if options['relative_to'] == 'none'
                            else options['relative_to'])
        for row in Asset().get_assets_for_pulling_last_update():
            asset = Asset.objects.get(pk=row['asset_id'])
            data = erd.get_last_update_times(asset, relative_to=relative_to,
                                             time_ago=-options['days'], ignore_missing_time=True)
            try:
                unstacked_data = data.unstack()
                last_update = unstacked_data.time.iloc[0]
                if last_update is pd.NaT:
                    err_msg = unstacked_data.err_msg.iloc[0]
                    if 'Your query produced no matching results.' not in str(err_msg):
                        raise ValueError("Last time for asset {}, deployment {}"
                                        " was not parsed: error was "
                                        "{}".format(asset.erddap_id,
                                                    asset.deployment.code,
                                                    err_msg))
                else:
                    asset.last_update = last_update
                    asset.save()
            except Exception as e:
                self.stderr.write(self.style.ERROR(
                    "Failed to parse ERDDAP last update data: "
                    "{}".format(e)))
                return_stat = 1
                continue

            self.stdout.write("{} last updated at {}".format(asset.erddap_id,
                                                             asset.last_update))
        sys.exit(return_stat)
