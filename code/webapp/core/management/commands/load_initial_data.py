import os

from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings
from external_scripts.Load_Plots import load_plots
from external_scripts.Load_Alerts import load_alerts
from external_scripts.load_assets import load_assets
from core.models.asset import Asset
from core.erddap import Erddap

class Command(BaseCommand):
    help = 'Loads initial asset, variable, plot and alert data'

    CONFIG_LOGS = os.path.join(settings.BASE_DIR, 'external_scripts/ConfigLogs')
    EXTERNAL_SCRIPTS  = os.path.join(settings.BASE_DIR, 'external_scripts')

    def handle(self, *args, **options):
        success, msg = Erddap().check_erddap_connection()
        if not success:
            self.stdout.write(self.style.ERROR(
                "Failed to connect to ERDDAP instance: \n" + str(msg)
            ))

            return

        self.stdout.write("Loading assets and variables...")
        # TODO: (badams) load module directly instead of forking processes
        for filename in os.listdir(self.CONFIG_LOGS):
            self.stdout.write("Loading " + filename)
            #     load_assets(os.path.join(self.CONFIG_LOGS, fs))
            call_command('update_assets', filename)

        self.stdout.write("Loading glider assets...")
        call_command('load_gliders')

        self.stdout.write("Loading plots...")
        load_plots()

        self.stdout.write("Loading alerts...")
        call_command("load_alerts")

        self.stdout.write("Building asset links...")
        asset_ids = Asset.objects.all().values_list('id', flat=True)
        [Asset().rebuild_asset_links(id) for id in asset_ids]
