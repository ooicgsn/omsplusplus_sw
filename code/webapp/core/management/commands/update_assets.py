import os
from django.conf import settings
from django.core.management.base import BaseCommand

from core.importer import AssetImporter

class Command(BaseCommand):
    help = 'Updates all assets'
    CONFIG_LOGS = os.path.join(settings.BASE_DIR, 'external_scripts/ConfigLogs')

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        filename = os.path.join(self.CONFIG_LOGS, options['filename'])

        status, msg = AssetImporter().import_asset(filename)
        if not status:
            print(msg)
            return

        print("Import successful")
        return
