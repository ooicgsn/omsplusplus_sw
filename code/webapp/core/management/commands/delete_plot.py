from django.core.management.base import BaseCommand
from core.models.alert_trigger import Trigger
from core.models.alert import Alert
from core.models.plot import Plot
from core.util import print_error, print_success, print_info


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument("name", help="Regex to match against plot names", type=str)
        parser.add_argument("--confirm", action="store_true", help="Confirm deleting of found plots")

    def handle(self, *args, **options):
        name = options.get("name")

        plots = Plot.objects.filter(name__iregex=name)

        print_info(self, "Matching Plots: {}".format(len(plots)))

        if options["confirm"]:
            print_error(self, "=============================")
            print_error(self, "== DELETING MATCHING PLOTS ==")
            print_error(self, "=============================")

        for plot in plots:
            name = plot.name
            print_info(self, name)

            if options["confirm"]:
                try:
                    Plot.delete(plot.id)
                except Exception as e:
                    print_error(self, "Failed to delete plot '{}': {}".format(name, e))
                    continue

                print_success(self, "Plot '{}' deleted successfully".format(name))









