from django.core.management.base import BaseCommand
from core.models.alert_trigger import Trigger
from core.models.alert import Alert
from core.util import print_error, print_success, print_info


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument("name", help="Regex to match against trigger names", type=str)
        parser.add_argument("--confirm", action="store_true", help="Confirm deleting of found triggers")

    def handle(self, *args, **options):
        name = options.get("name")

        triggers = Trigger.objects.filter(name__iregex=name)

        print_info(self, "Matching Triggers: {}".format(len(triggers)))

        if options["confirm"]:
            print_error(self, "===========================================")
            print_error(self, "== DELETING MATCHING TRIGGERS AND ALERTS ==")
            print_error(self, "===========================================")

        for trigger in triggers:
            name = trigger.name
            print_info(self, name)

            alerts = Alert.objects.filter(trigger=trigger)
            print_info(self, "- Alerts: {}".format(len(alerts)))

            if options["confirm"]:
                try:
                    for alert in alerts:
                        Alert.delete(alert.id)

                    Trigger.delete(trigger.id)
                except Exception as e:
                    print_error(self, "Failed to delete trigger '{}': {}".format(name, e))
                    continue

                print_success(self, "Trigger '{}' and all related alerts deleted successfully".format(name))









