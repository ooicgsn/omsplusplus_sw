from django.core.management.base import BaseCommand

from ...models.asset import Asset


class Command(BaseCommand):
    help = 'Rebuilds asset links based on the asset hierarchy'

    def add_arguments(self, parser):
        parser.add_argument(
            'assets',
            nargs='*',
            type=int,
            help='List of asset ID''s to process, separated by spaces'
        )
        parser.add_argument(
            '--all',
            action='store_true',
            dest='all_assets',
            default=False,
            help='Rebuild links for all assets',
        )

    def handle(self, *args, **options):
        if options['all_assets']:
            assets = Asset.objects.all().values_list('id', flat=True)
        else:
            assets = options['assets']

        num_failed = 0
        for asset_id in assets:
            asset = Asset.objects.filter(id=asset_id).first()
            if not asset:
                self.stdout.write(self.style.ERROR('Asset with ID {id} does not exist'.format(id=asset_id)))
                num_failed += 1
                continue

            Asset().rebuild_asset_links(asset_id)

        self.stdout.write(self.style.SUCCESS('Rebuilt asset links for {num} assets'.format(num=len(assets))))
        if num_failed > 0:
            self.stdout.write(self.style.ERROR('Failed to Rebuilt rebuild asset links for {num} assets'.format(num=num_failed)))