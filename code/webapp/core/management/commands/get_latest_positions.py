import datetime
import numpy as np

from django.core.management.base import BaseCommand
from django.db import DatabaseError, transaction

from core.models.asset import Asset, AssetLocation, AssetTypes, AssetDispositions
from core.models.variable import Variable
from core.erddap import Erddap
from core.util import print_error, print_success


class Command(BaseCommand):
    help = 'Grabs the latest positions for all moorings from ERDDAP'

    def handle(self, *args, **options):
        erddap = Erddap()
        time_window = -1  # last 1 day
        six_hour_interval = datetime.timedelta(seconds=(60 * 60 * 6))
        platforms = Asset.objects\
            .filter(disposition_id=AssetDispositions.DEPLOYED.value, type_id=AssetTypes.PLATFORM.value)\
            .exclude(group__is_glider=True)

        for platform in platforms:
            deployment_code = platform.deployment.code
            gps = Asset.objects.filter(platform=platform).filter(name='GPS').first()

            # Make sure the GPS instrument was found
            if not gps:
                print_error(self, "{} ({}): Could not locate GPS instrument"
                            .format(platform.code, deployment_code))
                continue

            # Make sure latitude/longitude variables were found on the GPS instrument
            lat_var = Variable.objects.filter(erddap_id='lat', asset=gps).first()
            lon_var = Variable.objects.filter(erddap_id='lon', asset=gps).first()

            if not lat_var or not lon_var:
                print_error(self, "{} ({}): Could not locate latitude/longitude variables on GPS instrument"
                            .format(platform.code, deployment_code))
                continue

            # Always attempt to get fresh data and only look relative to datetime now
            try:
                erddap_data = erddap.get_data(
                    gps, [lat_var, lon_var], time_window, 'json', 'days',
                    'deploy_id', deployment_code, False, None, 'max_time')

                mooring_data = erddap_data.json()['table']['rows']
            except:
                print_error(self, "{} ({}): Could not pull mooring position from ERDDAP"
                            .format(platform.code, deployment_code))
                continue

            if not mooring_data or len(mooring_data) == 0:
                print_error(self, "{} ({}): No latitude/longitude data available in ERDDAP"
                            .format(platform.code, deployment_code))
                continue

            tp_array = []
            time_array = np.array([datetime.datetime.strptime(elem[0], '%Y-%m-%dT%H:%M:%SZ').
                                  replace(tzinfo=datetime.timezone.utc) for elem in mooring_data])

            max_time = np.max(time_array)
            max_time_index = np.where(time_array == max_time)[0][0]
            tp_array.append([
                mooring_data[max_time_index][0],
                mooring_data[max_time_index][1],
                mooring_data[max_time_index][2]
            ])

            previous_time = max_time
            while previous_time > (max_time-datetime.timedelta(days=int(np.absolute(time_window)))):
                previous_time -= six_hour_interval
                data_exists = np.where(time_array <= previous_time)[0].size > 0
                if data_exists:
                    previous_time_index = np.where(time_array <= previous_time)[0][-1]

                    latitude = float(format(mooring_data[previous_time_index][1], '.6f'))
                    longitude = float(format(mooring_data[previous_time_index][2], '.6f'))
                    tp_array.append([mooring_data[previous_time_index][0], latitude, longitude])

            try:
                with transaction.atomic():
                    # delete previous positions for this platform if there are new ones available
                    if len(tp_array):
                        AssetLocation.objects.filter(asset=platform).delete()

                    # save new positions in bulk
                    coordinates = []
                    for row in tp_array:
                        # Make sure the latitude/longitude is valid
                        if row[1] >= -90 and row[1] <= 90 and row[2] >= -180 and row[2] <= 180:
                            coordinates.append(
                                AssetLocation(
                                    asset=platform,
                                    location_date=row[0],
                                    latitude=row[1],
                                    longitude= row[2]
                                )
                            )
                        else:
                            print_error(self, "{} ({}): Bad coordinates ({}, {}) ignored"
                                        .format(platform.code, deployment_code, row[1], row[2]))

                    if len(coordinates) > 0:
                        AssetLocation.objects.bulk_create(coordinates)

                        print_success(self, "{} ({}): Mooring position updated successfully"
                                      .format(platform.code, deployment_code))

            except DatabaseError as e:
                print_error(self, "{} ({}): Database error during save/delete transaction"
                            .format(platform.code, deployment_code))
