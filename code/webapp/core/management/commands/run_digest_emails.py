from django.core.management.base import BaseCommand
from tasks.run_digest_emails import run_daily_digest_emails
from datetime import datetime


class Command(BaseCommand):
    help = 'Emails to assigned users a summary of alerts that have occurred.'

    def add_arguments(self, parser):
        parser.add_argument('date', nargs='?', type=str, default='')

    def handle(self, *args, **options):
        print("Running digest emails")

        if options['date']:
            selected_date = datetime.strptime(options['date'], '%m/%d/%Y')
            run_daily_digest_emails(selected_date)
        else:
            run_daily_digest_emails()

