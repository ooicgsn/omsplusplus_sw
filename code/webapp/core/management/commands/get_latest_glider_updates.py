import sys
import dateutil.parser
import requests
from urllib.parse import urljoin

from livesettings.functions import config_value
from django.core.management.base import BaseCommand
from core.models.asset import Asset


class Command(BaseCommand):
    help = 'Updates all gliders with the latest modified date from ERDDAP'

    def handle(self, *args, **options):

        """Get ERDDAP user info from database"""
        user, password = (config_value('ERDDAP', 'USERNAME'),
                          config_value('ERDDAP', 'PASSWORD'))

        # authenticate creds for ERDDAP
        auth = requests.auth.HTTPBasicAuth(user, password)
        #
        # get ERDDAP url from Database
        base_url = config_value('ERDDAP', 'SERVER_URL')

        # example url for glider data grab (assume all gliders have a 'time' variable
        # http://ooivm1.whoi.net/erddap/tabledap/CP05MOAS-GL388-D00006.json?time&time=max(time)

        for glider in Asset.objects.filter(group__is_glider=True):

            glider_url = urljoin(base_url + '/tabledap/', glider.erddap_id + '.json?time&time=max(time)')
            try:
                resp = requests.get(glider_url, auth=auth)
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error pulling data for {} from ERDDAP: {}'.format(glider.erddap_id, e)))
            else:
                try:
                    data = resp.json()
                except ValueError:
                    self.stdout.write(
                        self.style.ERROR('ERDDAP returned no data for {}'.format(glider.erddap_id)))
                else:
                    glider.last_update = dateutil.parser.parse(data['table']['rows'][-1][0])
                    glider.save()

                    self.stdout.write(
                        self.style.SUCCESS(
                            'Successfully pulled data for {} from ERDDAP'.format(glider.erddap_id)))
            finally:
                print("{} last updated at {}".format(glider.erddap_id, glider.last_update))

        return
