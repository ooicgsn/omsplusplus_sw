from django.core.management.base import BaseCommand

from core.models.asset import Asset
from core.alerts import _get_status_from_open_alerts

# TODO(mchagnon): This is really just used for testing severity->status calculations and can be removed once testing is complete
class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('asset_id', help='AssetID', type=int)

    def handle(self, *args, **options):
        try:
            asset = Asset.objects.get(pk=options.get('asset_id'))
        except:
            print('Asset not found')
            return

        status_id, status_percent = _get_status_from_open_alerts(asset.id)

        print("Status ID: {}, Percent: {}".format(status_id, status_percent))


