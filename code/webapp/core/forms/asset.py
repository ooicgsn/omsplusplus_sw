from django import forms

from ..models.asset import AssetDisposition, AssetDispositions, AssetCategory, AssetSubcategory

class PlatformForm(forms.Form):
    disposition = forms.ChoiceField()
    category = forms.ChoiceField()
    subcategory = forms.ChoiceField(required=False)

    deployment_date = forms.CharField(required=False, label='Deployment Date', widget=forms.DateTimeInput(attrs={'class': 'datetimepicker deployment_date'}, format="%m/%d/%y %I:%M%p"))
    recovery_date = forms.CharField(required=False, label='Recovery Date', widget=forms.DateTimeInput(attrs={'class': 'datetimepicker recovery_date'}, format="%m/%d/%y %I:%M%p"))

    lat = forms.FloatField(required=False, widget=forms.TextInput())
    lon = forms.FloatField(required=False, widget=forms.TextInput())

    depth = forms.FloatField(required=False, widget=forms.TextInput())

    def __init__(self, *args, **kwargs):
        super(PlatformForm, self).__init__(*args, **kwargs)

        self.fields['disposition'] = forms.ChoiceField(
            choices=tuple(AssetDisposition.objects.all().values_list('id', 'name')),
            widget=forms.Select(attrs={'class': 'form-control'})
        )

        categories = AssetCategory.objects.all().values_list("id", "name")
        categories = [(0, "Select a Category")] + list(categories)

        self.fields['category'] = forms.ChoiceField(
            choices=categories,
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )

        subcategories = AssetSubcategory.objects.all().values_list("id", "name")
        subcategories = [(0, "Select a Subcategory")] + list(subcategories)

        self.fields['subcategory'] = forms.ChoiceField(
            choices=subcategories,
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )
    
    def clean(self):
        
        self.cleaned_data = super(PlatformForm, self).clean()
        if (self.cleaned_data.get('lat') and not self.cleaned_data.get('lon') or
            self.cleaned_data.get('lon') and not self.cleaned_data.get('lat')):
            raise forms.ValidationError("Please enter both Latitude and Longitude")

        return self.cleaned_data


class ImportPlatformForm(forms.Form):
    platform_code = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    deployment_code = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    disposition = forms.ModelChoiceField(
        queryset=AssetDisposition.objects.all(),
        required=False,
        empty_label=None,
        widget=forms.Select(attrs={'class': 'form-control'}))
    category = forms.ChoiceField(required=False)
    subcategory = forms.ChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        super(ImportPlatformForm, self).__init__(*args, **kwargs)

        cats = AssetCategory.objects.all()
        cat = []
        cat.append([0, 'Select a Category'])
        for c in cats:
            cat.append([c.id, c.name])

        self.fields['category'] = forms.ChoiceField(
            choices=tuple(cat),
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )
        subcat = []
        subcat.append([0, 'Select a Subcategory'])
        subcats = AssetSubcategory.objects.all()
        for sub in subcats:
            subcat.append([sub.id, sub.name])
        self.fields['subcategory'] = forms.ChoiceField(
            choices=tuple(subcat),
            widget=forms.Select(attrs={'class': 'form-control'}),
            required=False
        )