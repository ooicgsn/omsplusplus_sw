import json
from django import forms
from django.utils import timezone
from ..models.alert import Alert, Occurrence, Severities, Trigger
from ..models.alert_trigger import TriggerCategory
from ..models.static import Statuses, DurationCodes, L3Duration
from ..models.account import User
from ..models.asset import Asset, AssetTypes
from ..alerts import recalculate_asset_status
from ..util import enum_choices


class ManualAlertForm(forms.ModelForm):
    users_list = []
    severity = forms.ChoiceField(
        choices=enum_choices(Severities),
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    trigger = forms.ModelChoiceField(
        required=False,
        queryset=None,
        empty_label="Custom",
        widget=forms.Select(attrs={'class': 'form-control select-trigger'})
    )
    owner = forms.ChoiceField(
        required=False,
        choices=tuple(users_list),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = Alert
        fields = ('asset', 'deployment', 'details', 'name', 'roll_up', 'roll_down')
        widgets = {
            'asset': forms.HiddenInput(),
            'deployment': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'details': forms.Textarea(attrs={'class': 'form-control', 'rows': 8}),
            'roll_up': forms.CheckboxInput(),
            'roll_down': forms.CheckboxInput(),
        }

    def __init__(self, *args, **kwargs):
        asset = kwargs.pop('asset', None)
        deployment = kwargs.pop('deployment', None)

        super(ManualAlertForm, self).__init__(*args, **kwargs)

        self.fields['asset'].initial = asset
        self.fields['deployment'].initial = deployment
        self.fields['trigger'].queryset = Trigger.objects.filter(asset=asset)
        self.fields['details'].required = True
        self.fields['roll_up'].initial = False
        self.fields['roll_down'].initial = False
        users = User.objects.filter(is_active=True)
        users_list = []
        #users_list.append(['', 'Please select an owner'])
        for user in users:
            if not user.first_name and not user.last_name:
                entry = [user.id, user.username]
                users_list.append(entry)
            else:
                entry = [user.id, user.first_name + ' ' + user.last_name]
                users_list.append(entry)
        self.fields['owner'].choices = tuple(users_list)

    def clean(self):
        if not self.cleaned_data['trigger'] and not self.cleaned_data['name']:
            self.add_error('name', 'Required when trigger is set to custom')

        return self.cleaned_data

    def save(self, commit=True):

        alert = super(ManualAlertForm, self).save(commit=False)
        alert.severity_id = self.cleaned_data['severity']
        alert.trigger = self.cleaned_data['trigger']
        alert.status_id = Statuses.OPEN.value
        alert.is_manual = True
        alert.last_occurrence = timezone.now()
        alert.roll_up = self.cleaned_data['roll_up']
        alert.roll_down = self.cleaned_data['roll_down']
        alert.save()
        #alert.add_alert_notifications(alert.id)

        occurrence = Occurrence()
        occurrence.created = timezone.now()
        if 'triggered_value' in self.cleaned_data:
            occurrence.triggered_value = self.cleaned_data['triggered_value']
        occurrence.alert = alert
        occurrence.save()

        recalculate_asset_status(alert.asset.id, alert.details, None, True, alert.id)

        return alert


class EditTriggerForm(forms.ModelForm):
    choices_list = list(enum_choices(Severities))
    choices_list.insert(0, ('', ''))
    choices_tuple = tuple(choices_list)

    dc_list = list(enum_choices(DurationCodes))
    dc_list.insert(0, ('', ''))
    dc_tuple = tuple(dc_list)

    interp_duration = list(enum_choices(L3Duration))
    interp_duration.insert(0, ('', ''))

    # TODO(mchagnon): This is causing issues with the rebuild script
    # users = User.objects.filter(is_active=True)
    users_list = []
    # users_list.append(['', 'Please select an owner'])
    # for user in users:
    #     if not user.first_name and not user.last_name:
    #         entry = [user.id, user.username]
    #         users_list.append(entry)
    #     else:
    #         entry = [user.id, user.first_name + ' ' + user.last_name]
    #         users_list.append(entry)


    severity = forms.ChoiceField(
        choices=choices_tuple,
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=True
    )
    duration_code = forms.ChoiceField(
        choices=dc_tuple,
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=True
    )
    owner = forms.ChoiceField(
        choices=tuple(users_list),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    category = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=False
    )

    interpolation_code = forms.ChoiceField(
        choices=tuple(interp_duration),
        widget=forms.Select(attrs={'class': 'form-control'}),
        required=False
    )

    _platformList = list(Asset.objects.filter(type_id=AssetTypes.PLATFORM.value).values_list("name", flat=True).order_by("name").distinct())

    default_on_platforms = forms.MultipleChoiceField(choices=zip(_platformList, _platformList), required=False, label='Default on Platform', 
        widget=forms.SelectMultiple(attrs={'class': 'select2_multiple'}))

    def __init__(self, is_admin, *args, **kwargs):
        super(EditTriggerForm, self).__init__(*args, **kwargs)

        self.fields['expression'].required = True
        if not is_admin:
            self.fields['owner'].required = False
            self.fields['is_global'].required = False

        users = User.objects.filter()
        users_list = []
        users_list.append(['', 'Please select an owner'])
        for user in users:
            try:
                if user.id == self.initial['owner'] or user.is_active:
                    if not user.first_name and not user.last_name:
                        entry = [user.id, user.username]
                        users_list.append(entry)
                    else:
                        entry = [user.id, user.first_name + ' ' + user.last_name]
                        users_list.append(entry)
            except:
                if user.is_active:
                    if not user.first_name and not user.last_name:
                        entry = [user.id, user.username]
                        users_list.append(entry)
                    else:
                        entry = [user.id, user.first_name + ' ' + user.last_name]
                        users_list.append(entry)

        self.fields['owner'].choices = tuple(users_list)

        categories = []
        cat = TriggerCategory.objects.all()
        categories.append(['', 'Select a Category'])
        for c in cat:
            categories.append([c.id, c.name])
        self.fields['category'].choices = tuple(categories)

    def clean_duration(self):
        if self.cleaned_data["duration"] <= 0:
            raise forms.ValidationError("Duration must be greater than zero")

        return self.cleaned_data["duration"]

    def clean(self):
        cleaned_data = super(EditTriggerForm, self).clean()
        error_msg_var = "Please add a variable."
        error_msg_asset = "Please select an alert asset"
        error_msg_duration = "Please select an interpolation code"
        variables_obj = self.data['saveVariables']
        trigger_asset = self.data['selectAsset_assetpicker']
        variables_obj_list = json.loads(variables_obj)

        if not variables_obj_list:
            raise forms.ValidationError(error_msg_var)
        if not trigger_asset:
            raise forms.ValidationError(error_msg_asset)
        if self.data['interp_duration'] and not self.data['interpolation_code']:
            raise forms.ValidationError(error_msg_duration)

        return cleaned_data

    class Meta:
        model = Trigger
        fields = ('name', 'duration', 'is_global', 'expression', 'roll_up', 'roll_down', 'comments')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'duration': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '12'}),
            'is_global': forms.CheckboxInput(),
            'expression': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}),
            'roll_up': forms.CheckboxInput(),
            'roll_down': forms.CheckboxInput(),
            'comments': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'})
        }

