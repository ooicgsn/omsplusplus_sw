from django import forms
from ..models.plot import Plot, PlotType, PlotClass, PlotTimeRange
from ..models.variable import Variable
from ..models.account import User
from ..models.asset import Asset, AssetTypes
from ..models.static import TimeChoices, BoolChoices, YAxisChoices


class PlotForm(forms.ModelForm):

    variables_x = forms.CharField(label='X-Variable(s)', widget=forms.HiddenInput())
    variables_y = forms.CharField(label='Y-Variable(s)', widget=forms.HiddenInput())

    #timeframe = forms.CharField(label='Time Frame', widget=forms.Select(
        #choices=[(e.value, e.name.title().replace('_',' ')) for e in TimeChoices]))

    owner = forms.ChoiceField(
        choices=[],
        required=False
    )
    
    plot_type = forms.ModelChoiceField(queryset=PlotType.objects.all(), label='Plot Type', empty_label=None)
    plot_class = forms.ModelChoiceField(queryset=PlotClass.objects.all(), label='Plot Class', empty_label=None,
                                        widget=forms.HiddenInput())


    start_date = forms.CharField(label='Start Date', required=False, widget=forms.DateInput(attrs={'class': 'datepicker start_date'}))
    end_date = forms.CharField(label='End Date', required=False, widget=forms.DateInput(attrs={'class': 'datepicker end_date'}))

    timeframe = forms.ModelChoiceField(queryset=PlotTimeRange.objects.all(), label='Time Frame', empty_label=None, widget=forms.Select(attrs={'class': 'time-window'}),)

    yaxis_orientation = forms.CharField(label='Y-Axis Orientation', widget=forms.Select(
        choices=[(e.value, e.name.title()) for e in YAxisChoices]))


    _platformList = list(Asset.objects.filter(type_id=AssetTypes.PLATFORM.value).values_list("name", flat=True).order_by("name").distinct())
    default_on_platforms = forms.MultipleChoiceField(choices=zip(_platformList, _platformList), required=False, label='Default on Platform', 
        widget=forms.SelectMultiple(attrs={'class': 'select2_multiple'}))

    def __init__(self, *args, **kwargs):
        super(PlotForm, self).__init__(*args, **kwargs)

        users = User.objects.filter()
        users_list = []
        users_list.append(['', 'Please select an owner'])
        for user in users:
            try:
                if user.id == self.initial['owner'] or user.is_active:
                    if not user.first_name and not user.last_name:
                        entry = [user.id, user.username]
                        users_list.append(entry)
                    else:
                        entry = [user.id, user.first_name + ' ' + user.last_name]
                        users_list.append(entry)
            except:
                if user.is_active:
                    if not user.first_name and not user.last_name:
                        entry = [user.id, user.username]
                        users_list.append(entry)
                    else:
                        entry = [user.id, user.first_name + ' ' + user.last_name]
                        users_list.append(entry)

        self.fields['owner'].choices = tuple(users_list)


    class Meta:
        model = Plot
        fields = ['name', 'is_global']
        labels = {
            "name": "Name",
            "is_global": "Available to Everyone",
        }

        widgets = {
            'is_global': forms.Select(choices=[(e.value, e.name.title()) for e in BoolChoices])
        }