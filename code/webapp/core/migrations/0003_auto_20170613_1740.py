# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-13 17:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20170612_0017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='code',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='code',
            field=models.CharField(max_length=30),
        ),
    ]
