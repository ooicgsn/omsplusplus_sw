# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-11 19:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_profile_redmine_api_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='alert',
            name='name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.RunSQL("DROP FUNCTION public.get_hotlist_alerts(integer);"),
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.get_hotlist_alerts(IN user_id integer)
              RETURNS TABLE(alert_notification_id integer, alert_id integer, trigger_id integer, asset_id integer, asset_code text, 
            deployment_code text, trigger_name text, severity_name text, details text, alert_date timestamp with time zone, platform_id integer, platform_code text,
            is_manual boolean, name character varying) AS
            $BODY$
                                    
                                            SELECT      alert_notification.id,
                                                        alert.id,
                                                        alert_trigger.id,
                                                        asset.id,
                                                        asset.code,
                                                        deployment.code,
                                                        alert_trigger.name,
                                                        alert_severity.name,
                                                        alert.details,
                                                        alert.created,
                                                        COALESCE(platform.id, asset.id) AS platform_id,
                                                        COALESCE(platform.code, asset.code) AS platform_code,
                                                        alert.is_manual,
                                                        alert.name
                                            FROM        alert_notification
                                            INNER JOIN  alert
                                ON alert_id = alert.id
                                            INNER JOIN  asset
                                                        ON asset_id = asset.id
                                            INNER JOIN  deployment
                                                        ON deployment_id = deployment.id
                                            LEFT JOIN  alert_trigger
                                                        ON trigger_id = alert_trigger.id
                                            INNER JOIN  alert_severity
                                                        ON alert.severity_id = alert_severity.id
                                            LEFT JOIN	asset platform
                                    ON platform.id = asset.platform_id
                                        WHERE	alert_notification.user_id = $1
                                            AND alert_notification.has_acknowledged = FALSE
                                            AND	alert.status_id <> 4 -- Resolved
                                        ORDER BY	alert.created DESC
                        
                                            $BODY$
              LANGUAGE sql VOLATILE
              COST 100
              ROWS 1000;
        """)
    ]
