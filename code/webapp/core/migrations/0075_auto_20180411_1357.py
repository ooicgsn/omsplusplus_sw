# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-04-11 13:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0074_auto_20180404_2110'),
    ]

    operations = [
        migrations.RunSQL("""
            CREATE OR REPLACE FUNCTION public.add_alert_notifications_for_new_user(user_id integer)
              RETURNS void AS
                $BODY$ 
                                                
                            INSERT INTO 	alert_notification (alert_id, user_id, created, modified, has_acknowledged)
                            SELECT		alert.id, $1, now(), now(), FALSE
                            FROM		alert
                                            WHERE		status_id = 1
                                                
                $BODY$
              LANGUAGE sql VOLATILE
              COST 100;
            ALTER FUNCTION public.add_alert_notifications_for_new_user(integer)
              OWNER TO omsplusplus;
        """)
    ]
