import pandas as pd
from numpy import dtype


def validate_expression(test_expr, variable_names, validate_dtype):
    """
    Validates an expression for a model.
    :param str test_expr: An expression to be evaluated by numexpr
    :param list variable_names: A list of strings containing the names of
                                the variables
    :param numpy.dtype validate_dtype: If supplied a numpy dtype, ensure
                                        evaluated expression returns the
                                        given dtype. If None is supplied,
                                        do not check the dtype
    :returns: A 2-tuple. The first element indicates whether the expression
                is valid or not.  The second element contains a string with
                a reason for failure if the expression failed, or None if it
                was valid.
    :rtype: (bool, str)
    """

    # create an empty dataframe with only the columns to test
    dummy_df = pd.DataFrame(columns=variable_names, dtype=dtype('float64'))
    try:
        res = dummy_df.eval(test_expr)
    except Exception as e:
        return False, str(e)
    if validate_dtype is not None:
        if res.dtype != validate_dtype:
            return (False,
                    "Expression must return {}".format(validate_dtype)
                    )
    # Fall through for correct dtype or when no dtype is specified.
    # In this case, the expression is valid.
    return True, None
