CREATE DATABASE omsplusplus;
CREATE USER omsplusplus WITH PASSWORD 'password' CREATEDB;
ALTER ROLE omsplusplus SET client_encoding TO 'utf8';
ALTER ROLE omsplusplus SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE omsplusplus TO omsplusplus;