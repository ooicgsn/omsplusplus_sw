# This this will clear out any database migration files and is only used for the initial project development. Should
# never be run in production. Note that you also have to drop and recreate the database (which is why you'd never
# run this in prod)

find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete
#./manage.py makemigrations
#./manage.py migrate
