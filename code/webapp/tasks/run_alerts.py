from django import db
from core.models.alert_trigger import Trigger, TriggerVariable, TriggerUser
from core.models.alert import Alert, Status, Occurrence, OccurrenceItem, Statuses
from core.models.asset import AssetDispositions
from core.alerts import recalculate_asset_status
from core.erddap import Erddap, interpolate_continuous_series
from core.db import exec_void
from pytz import timezone
import pandas as pd
import logging
from core.alerts import send_user_alert_notification
import multiprocessing

logger = logging.getLogger('django')


def create_alert(trigger, failures):
    """Creates an alert, alert_occurrence(s), and alert notifications"""

    # If there are no failures (which should not happen) there's no reason to create an alert
    if failures.empty:
        return

    # check if there's already an existing alert
    alert, is_new = Alert.objects.get_or_create(
        trigger=trigger,
        status_id=Statuses.OPEN.value,
        severity=trigger.severity,
        deployment=trigger.asset.deployment,
        asset=trigger.asset,
        roll_up=trigger.roll_up,
        roll_down=trigger.roll_down
    )

    # (badams) this should possibly be in a transaction
    occurrence = Occurrence.objects.create(created=pd.Timestamp.utcnow(), alert=alert)
    timestamp_utc = None

    for timestamp, failure in failures.iteritems():
        # Pandas appears to parse CSV TZ-naive even if ISO8601 is used. Convert each TS from naive to UTC here,
        # as Django doesn't like it naive TS being used in the model fields
        timestamp_utc = timestamp.to_pydatetime().replace(tzinfo=timezone('UTC'))

        OccurrenceItem.objects.create(occurrence=occurrence, occurred_at=timestamp_utc, value=failure)

    alert.created = timestamp_utc if is_new else alert.created
    alert.last_occurrence = timestamp
    alert.save()

    # raise new alert notifications
    exec_void('add_alert_notifications', (alert.id,))

    # update the asset status table if there was a new alert created
    if is_new:
        log_info(trigger, "Updating status for linked alert asset id {}".format(alert.asset.id))

        recalculate_asset_status(alert.asset.id, alert.details, None, False, alert.id)

        # send email notification if there are any users subscribed
        send_alert_notifications(trigger, alert)

    return alert


def send_alert_notifications(trigger, alert):
    try:
        trigger_users = TriggerUser.objects.filter(trigger_id=trigger.id)

        if len(trigger_users) > 0:
            for trigger_user in trigger_users:
                send_user_alert_notification(trigger_user.user, alert, trigger_user.delivery_method_id)

    except Exception as e:
        log_info(trigger, "  [!] Error sending alert notifications: " + str(e))


def get_windowed_data(raw_data, interval):
    """
    Takes raw data and a string interval and resamples data within a window
    to the given interval.  The next window is spaced one half the interval
    length away, causing 50% overlap between adjacent windows.
    Returns a resampled time series indexed pandas DataFrame or Series.
    """

    # NB: (badams) This might not be the most efficient implementation, but it
    #              works reasonably well for an out of the box Pandas
    #              implementation
    # TODO: (badams) prefer pandas.Timedelta intervals to string specifications
    #                since the former appear more flexible
    resamp = raw_data.resample(interval).mean()
    half_interval = pd.Timedelta(interval) / 2

    # shift back by half the time interval, resample on original interval, and
    # then shift forward another 15 minutes on this result.
    # Discard the first data point as it is missing some data.
    # This results in times that overlap with the original resampling.
    # NB: .ix[1:] won't throw an error if the Series or DataFrame is empty
    # have to shift by pd.Timedelta object, string has odd results
    resamp_shift = raw_data.tshift(freq=half_interval)\
                       .resample(interval, loffset=half_interval)\
                       .mean()\
                       .tshift(freq=-half_interval*2).ix[1:]

    # combine the two series and sort in chronolgoically ascending order
    resamp_combined = pd.concat([resamp, resamp_shift]).sort_index()

    return resamp_combined


def fetch_data_trigger(trigger, time_span_days=-2):
    """
    trigger: A trigger object to process
    time_span_days: a negative number indicating the number of days from the current time to search for observations
    """

    trig_vars = TriggerVariable.objects.filter(trigger=trigger)
    tvar_dict = {tv.name: tv.variable for tv in trig_vars}

    # any interpolation must be performed prior to evaluating the predicate
    try:
        expr, pred = trigger.split_expression().groups()
    except Exception as e:
        log_info(trigger, "  [!] Error parsing trigger expression: " + (trigger.expression if trigger.expression else ""))
        return pd.DataFrame()

    # combined may initially be either a dataframe or a series depending on
    # if it had an expression associated with it
    try:
        combined = Erddap().fetch_data(tvar_dict, expr, trigger.name,
                                       time_span_days=time_span_days,
                                       deployment_id=trigger.asset.deployment.code,
                                       interp_interval=trigger.interpolation_amount,
                                       change_interval=trigger.change_interval)
    except Exception as e:
        log_info(trigger, "  [!] Exception connecting to ERDDAP: " + str(e))
        return pd.DataFrame()

    if combined.empty:
        log_info(trigger, "  [!] No data returned from ERDDAP")
        return pd.DataFrame()

    # return an empty series for the trigger if the result is empty.
    # often this will be due to having time out of range for the data fetch
    if combined.empty:
        return pd.Series(dtype=bool)

    formula_remaps = {"{}_{}".format(tv.variable.asset.erddap_id,
                                    tv.variable.erddap_id): tv.name
                                    for tv in trig_vars}
    combined.rename(columns=formula_remaps, inplace=True)
    window_duration = trigger.get_duration()

    # if there's windowed data, we need to apply the window prior to
    # applying the predicate
    if window_duration:
        windowed_results = get_windowed_data(combined, window_duration)
        # evaluate the windowed results against the predicate
        windowed_results.name = 'val'
        fails = windowed_results.to_frame('val').eval("val {}".format(pred),
                                                       engine='numexpr')
        return windowed_results[fails]
    # just return the evaluated expression if there is no windowing
    else:
        # ensure that combined is a dataframe so we can call eval
        res = combined.to_frame('val')
        fails = res.eval("val {}".format(pred), engine='numexpr')
        return res[fails].val


def process_trigger(trigger):
    trigger.log_entries = []

    """Run the tests specified by the alert trigger condition"""
    log_info(trigger, "[+] Processing trigger {} ({}) for {} on {} ({})".format(
        trigger.name,
        trigger.id,
        trigger.asset if trigger.asset else "",
        trigger.asset.platform if trigger.asset else "",
        trigger.asset.deployment.code if trigger.asset and trigger.asset.deployment else ""
    ))

    try:
        # raise exception if there is no asset associated with this trigger
        if trigger.asset is None:
            raise ValueError('  [!] Alert trigger {} must have an associated asset'.format(trigger.name))

        failures = fetch_data_trigger(trigger)
        if not failures.empty:
            log_info(trigger, '  [+] Alert raised for "{}"'.format(trigger.expression))
            alert = create_alert(trigger, failures)
            flush_logs(trigger)

            return alert

        flush_logs(trigger)
        return None

    # we want to continue and log any exceptions, not kill the process should an error arise
    except Exception as e:
        log_info(trigger, str(e))
        flush_logs(trigger)
        return None


def log_info(trigger, msg):
    trigger.log_entries.append(msg)


def flush_logs(trigger):
    logger.info("\n" + "\n".join(trigger.log_entries))


def process_triggers(parallel=True):
    """
    Simple method for running all triggers in the database that have
    a deployed or burn-in asset disposition

    Args: parallel - Use multiprocessing to evaluate the alert trigger conditions in parallel
    """
    enabled_dispositions = [
        AssetDispositions.BURN_IN.value,
        AssetDispositions.DEPLOYED.value
    ]
    
    triggers = Trigger.objects\
        .exclude(asset__is_deleted=True)\
        .filter(asset__platform__disposition__id__in=enabled_dispositions)

    if parallel:
        db.connections.close_all()
        pool = multiprocessing.Pool()
        pool.map(process_trigger, triggers)
        return

    for trigger in triggers:
        process_trigger(trigger)
