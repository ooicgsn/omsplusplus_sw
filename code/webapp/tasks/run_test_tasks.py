
import os, sys

import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("projectLocation")
# parser.add_argument("user")
# parser.add_argument("host")
# parser.add_argument("password")

args = parser.parse_args()

#begin web service gateway
proj_path = args.projectLocation

# pointer to django settings.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")
sys.path.append(proj_path)

# Load Local settings
os.chdir(proj_path)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

#end webservice gateway

from tasks import test_tasks

# def runSetUpTask():
#     t = test_tasks.TestTasks()
#     t.setUp()
#     return;

def runTest_Window():
    t = test_tasks.TestTasks()
    t.setUp()
    t.test_window()
    t.test_no_window()
    return;


runTest_Window()