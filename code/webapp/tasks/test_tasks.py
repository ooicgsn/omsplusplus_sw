from django.test import TestCase
# this is rather long, consider namespaced?
from core.models.alert import (Alert, Status, Notification, Occurrence,
                               OccurrenceItem)
from core.models.alert_trigger import (DurationCode, Severity, Trigger,
                                       TriggerVariable)
from core.models.asset import Asset, AssetType, AssetStatus, Group, Deployment
from core.models.variable import Variable
from pandas import Timestamp
from django.contrib.auth.models import User
from urllib.parse import urljoin
from tasks import run_alerts
import pandas as pd
import requests_mock


# (badams): possibly split into a windowed and non-windowed case inheriting base
#           setup of variables, etc.
class TestTasks(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('TestUser', 'test123@test.xyzzyx',
                                             'b@dpa55')
        self.user.save()
        deployment = Deployment(name='Test Deployment', code='test_deploy')
        deployment.save()

        self.assetPlatform = Asset(name='Test Platform', code='test-platform',
                      type=AssetType.objects.get(name='Platform'),
                      group=Group.objects.get(name='Coastal - Pioneer'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=deployment, erddap_id='test-platform')

        self.assetPlatform.save()

        self.asset = Asset(name='Test Asset', code='test-asset',
                      type=AssetType.objects.get(name='Instrument'),
                      group=Group.objects.get(name='Coastal - Pioneer'),
                      status=AssetStatus.objects.get(name='Ok'),
                      deployment=deployment, erddap_id='test-asset',
                      platform_id=self.assetPlatform.id)

        self.asset.save()

        self.variable = Variable(name='voltage', asset=self.asset,
                                 erddap_id='voltage')
        self.variable.save()
        minute_duration = DurationCode.objects.get(name='Minutes')
        # create a trigger with no windowing
        self.trig_no_window = Trigger(name='trig-test-no-window', duration=0,
                                      duration_code=DurationCode.objects.get(name='Current Deployment'),
                                      expression='voltage < 13',
                                      asset=self.asset, created_by=self.user,
                                      severity=Severity.objects.get(name='Medium'))
        self.trig_no_window.save()
        self.trig_no_window_var = TriggerVariable(trigger=self.trig_no_window,
                                                  variable=self.variable,
                                                  name='voltage')
        self.trig_no_window_var.save()
        # rate of change trigger without window
        self.trig_no_window_roc = Trigger(name='trig-test-no-window-roc', duration=0,
                                      duration_code=DurationCode.objects.get(name='Current Deployment'),
                                      # this is a rate of change
                                      expression='voltage < -1',
                                      asset=self.asset, created_by=self.user,
                                      severity=Severity.objects.get(name='Medium'),
                                      change_interval=pd.Timedelta(minutes=1))
        self.trig_no_window_roc.save()
        self.trig_no_window_roc_var = TriggerVariable(trigger=self.trig_no_window_roc,
                                                  variable=self.variable,
                                                  name='voltage')
        self.trig_no_window_roc_var.save()


        self.trig_window = Trigger(name='trig-test-no-window', duration=2,
                                   asset=self.asset, created_by=self.user,
                                   expression='voltage < 13',
                                   duration_code=minute_duration,
                                   severity=Severity.objects.get(name='Medium'))
        self.trig_window.save()
        self.trig_window_var = TriggerVariable(trigger=self.trig_window,
                                                  variable=self.variable,
                                                  name='voltage')
        self.trig_window_var.save()

        test_url = 'https://cgoms.coas.oregonstate.edu/erddap/'
        # Rely on default ERDDAP URL in livesettings for the time being.
        # A bit of a hack, perhaps, but there didn't seem to be an easy way to
        # alter livesettings in Python
        self.ds_url = urljoin(test_url, 'tabledap/test-asset.csv?time,voltage&time%3E=now-2days')
        # mock response for the CSV
        self.csv_mock = '''time,voltage
UTC,V
2017-03-20T00:00:00Z,14.01
2017-03-20T00:01:00Z,14.0
2017-03-20T00:02:00Z,12.7
2017-03-20T00:03:00Z,14.0
2017-03-20T00:04:00Z,13.99998
2017-03-20T00:05:00Z,13.99998
2017-03-20T00:06:00Z,13.9543'''

    def test_window(self):
        """
        Tests whether a window test will function properly.  Should not raise
        alert, due to averaging of window on outlier.
        """
        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)
            alert = run_alerts.process_trigger(self.trig_window)
            # when the window smooths out the alert, the result
            self.assertIsNone(alert)
            # FIXME (badams): have count reflect new
            self.assertEqual(Occurrence.objects.count(), 0)
            self.assertEqual(Notification.objects.count(), 0)

    def test_no_window(self):
        """
        Tests whether a no window test will function properly.  Should raise an
        alert due to outlier under threshold.
        """
        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)
            alert = run_alerts.process_trigger(self.trig_no_window)
            self.assertIsInstance(alert, Alert)
            self.assertEqual(alert.last_occurrence,
                             Timestamp('2017-03-20T00:02:00Z'))
            # set alert status to resolved
            # 3 pre-existing + 1 new occurrence
            self.assertEqual(Occurrence.objects.count(), 1)
            self.assertEqual(OccurrenceItem.objects.count(), 1)
            # for now, new alert should create as many notifications as there
            # are users
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # rerun alert with same status
            alert_same = run_alerts.process_trigger(self.trig_no_window)
            self.assertEqual(alert_same.id, alert.id)

            # new run should create a new Occurrence object
            self.assertEqual(Occurrence.objects.count(), 2)
            self.assertEqual(OccurrenceItem.objects.count(), 2)


            # more notifications should not be raised, as the alert already
            # exists and has not been cleared
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # update alert to resolved status
            alert.status = Status.objects.get(name='Resolved')
            alert.save()
            # simulate another run of the alerts after the first one has been
            # resolved.  Same time window in mock, so this isn't very realistic,
            # but the occurences count should remain
            # the same.
            alert2 = run_alerts.process_trigger(self.trig_no_window)
            # should have created a new alert
            self.assertNotEqual(alert.id, alert2.id)
            self.assertIsInstance(alert2, Alert)
            # set alert status to resolved
            # +1 occurences
            self.assertEqual(Occurrence.objects.count(), 3)
            self.assertEqual(OccurrenceItem.objects.count(), 3)
            # There should be exactly one alert history entry corresponding
            # to the alert condition just created
            self.assertEqual(Notification.objects.filter(alert=alert2).count(),
                             User.objects.count())

    def test_no_window_rate_of_change(self):
        """
        Tests whether a no window test will function properly when supplied
        a rate of change.  Should raise an
        alert due to outlier under threshold.
        """
        with requests_mock.mock() as m:
            m.get(self.ds_url, text=self.csv_mock)
            alert = run_alerts.process_trigger(self.trig_no_window_roc)
            self.assertIsInstance(alert, Alert)
            # shifted back an hour
            self.assertEqual(alert.last_occurrence,
                             Timestamp('2017-03-20T00:01:00Z'))
            # set alert status to resolved
            # 3 pre-existing + 1 new occurrence
            self.assertEqual(Occurrence.objects.count(), 1)
            self.assertEqual(OccurrenceItem.objects.count(), 1)
            # for now, new alert should create as many notifications as there
            # are users
            assert OccurrenceItem.objects.get().value < -1
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # rerun alert with same status
            alert_same = run_alerts.process_trigger(self.trig_no_window_roc)
            self.assertEqual(alert_same.id, alert.id)

            # new run should create a new Occurrence object
            self.assertEqual(Occurrence.objects.count(), 2)
            self.assertEqual(OccurrenceItem.objects.count(), 2)


            # more notifications should not be raised, as the alert already
            # exists and has not been cleared
            self.assertEqual(Notification.objects.filter(alert=alert).count(),
                             User.objects.count())
            # update alert to resolved status
            alert.status = Status.objects.get(name='Resolved')
            alert.save()
            # simulate another run of the alerts after the first one has been
            # resolved.  Same time window in mock, so this isn't very realistic,
            # but the occurences count should remain
            # the same.
            alert2 = run_alerts.process_trigger(self.trig_no_window)
            # should have created a new alert
            self.assertNotEqual(alert.id, alert2.id)
            self.assertIsInstance(alert2, Alert)
            # set alert status to resolved
            # +1 occurences
            self.assertEqual(Occurrence.objects.count(), 3)
            self.assertEqual(OccurrenceItem.objects.count(), 3)
            # There should be exactly one alert history entry corresponding
            # to the alert condition just created
            self.assertEqual(Notification.objects.filter(alert=alert2).count(),
                             User.objects.count())

# TODO (badams): add a test case for duplicated data
