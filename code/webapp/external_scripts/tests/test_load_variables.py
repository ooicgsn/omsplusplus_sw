from external_scripts import load_assets as la
from unittest import TestCase
import pandas as pd
import os.path

class TestVariables(TestCase):
    def setUp(self):
        """Load an ERDDAP 'info CSV'"""
        self.data = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data',
                            'CE04OSSM-BUOY-00-METBKA_info.csv'))

    def test_blacklisted_names(self):
        """
        Check that variable names return the proper boolean
        result when compared against a regex blacklist
        """
        self.assertFalse(la.check_blacklisted_variables('deploy_id'))
        self.assertFalse(la.check_blacklisted_variables('http_location'))
        self.assertFalse(la.check_blacklisted_variables('feature_type'))
        self.assertTrue(la.check_blacklisted_variables('conductivity'))
        self.assertTrue(la.check_blacklisted_variables('density'))
        self.assertTrue(la.check_blacklisted_variables('salinity'))

    def test_get_valid_variables(self):
        """Check that our input file only has expected variables"""
        var_pivot = la.get_valid_variables(self.data)
        # units happens to be in this file. If there's really bad metadata
        # or units aren't applicable, this might not always be the case
        self.assertTrue('units' in var_pivot.columns)
        # same as for above comment, but WRT long_name instead of units
        self.assertTrue('long_name' in var_pivot.columns)
        # check that the indexes/var names correspond to what's expected
        # should have physical variables along with coordinate variables
        expected_vars = {"air_temperature",
                         "barometric_pressure",
                         "eastward_wind_velocity",
                         "feature_type_instance",
                         "time",
                         "latitude",
                         "longitude",
                         "longwave_irradiance",
                         "northward_wind_velocity",
                         "precipitation_level",
                         "psu",
                         "relative_humidity",
                         "rho",
                         "sea_surface_conductivity",
                         "sea_surface_temperature",
                         "shortwave_irradiance" }
        self.assertTrue(expected_vars == set(var_pivot.index))
