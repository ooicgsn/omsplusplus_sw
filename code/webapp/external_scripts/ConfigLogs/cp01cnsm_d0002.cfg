 ######################################################
# OOI/CGSN Buoy Platform configuration file
# Format: Name = value
#
# Comments begin with #
#
# History:
#    Date      Who    Description
# ----------   ---    ----------------------------------------
# 08/30/2012   SL     Create
# 03/27/2013   SL/SW  NUTNR and SPKIR on chan 7/8 have been moved 
#                     from DCL 27 to DCL 26
# 07/18/2013   SL/KVH Added rda6 (nsif) and rda7 (mfn)
# 09/11/2013   SL     Updated pwr.cfg (pwr lo/hi) for new DCL firmware
#                     Added port pwr and pwr.sched to DCL section
# 09/18/2013   SL/ME  Updated phsen/pco2w dlog driver to sunburst
#                     Added port pwr and pwr.sched to DCL section
# 09/18/2013   SL/ME  Updated phsen/pco2w dlog driver to sunburst
# 11/05/2013   SL/KDH Moved dcl11 to dcl13 due to bad console on slot1
#              ME     Added nortek -o param
# 11/06/2013   ME/SL  dl_wave name change and updated inst schedules & dl params,
# 11/08/2013   ME/AP  keep pco2a, wavss, velpt and phsens always on (and manage
#                     sched via data logger), turned-off
#                     fdchp and acomm (not currently on platform)
#                     methtr set to 24v and turned-off until available,
#                     clim for dosta/ctdbp/optaa -> 2500
# 11/11/2013   ME/SL  Added portN.pwroff.delay.  High power ADCP
# 11/13/2013   SL     Set deploy dir to D0001, updated irid phone number
# 11/14/2013   ME/SL  Updated delay values and increased clim for adcp
#              AP     Changed all mfn inst schedule to daily/hour
# 11/16/2013   ME/AP  Inceased sched for presf/sbe26 to 7min (takes 5.5min)
# 11/17/2013   ME     Added additional power on delays for starting some instruments.
# 11/22/2013   SL/AP  Updated deploy lat, lon, wdepth, wcircle
# 09/30/2014   BJK    Updated for 2014 Deployment
# 10/03/2014   SL     Added Platform.cpm#.psc_type and ldet_cfg
# 10/17/2014   ME     Updated for batteries and new configuration files. 
# 10/23/2014   MP/SL  Updated acomm to clim 2500, sstart, and hipwr
# 10/28/2014   ME     New SPKIR and DOSTA timing 
# 10/31/2014   ME     Correcting Dlog arguments add MOPAK
# 11/03/2014   ME     OPTAA soft start and PCO2A arguments.
# 11/06/2014   SL/JL  Added irid mtip and mtports
# 11/06/2014   ME     Fixed poll_delay argument, pc02a on continuous
# 11/11/2014   BJK/SL Removed RTE Data Logger and setup methtr1 to be on
# 11/13/2014   JML    Updated Xeos MELO/KILO IMEI numbers
# 11/14/2014   ME     Changed times to deal with battery drift
# 11/14/2014   JML    Corrected Xeos3 IMEI number
# 11/18/2014   ME     Added extra delay to wavss
# 12/03/2014   SL     Fixed fb250_2 ip address
# 12/04/2014   ME     Increased pco2a clim to 2500
# 12/04/2014   ME     Updated VELPT to remove batteries.  Timing on CTDBP, ADCP, and PRESF
# 12/08/2014          to allow instrument to start 5 after hour.  2500 ma on PCO2A and ADCP.
# 12/08/2014   BJK    Updated SBD IMEI numbers
#              BJK    was Main=300234060356360  Backup=300234060459280
#              BJK    Corrected to Main=300234060459280 Backup=300234060356360
# 12/16/2014   ME     Reduced PCO2A back to 1500 (pump) issue
# 03/02/2015   SL/DGI Added ZPLSC to dcl37 port 7
# 04/14/2015   ME/SL  Updated for new dataloggers - eliminated cmd-line args
#                     for non-binary instruments, set pcfg to 0 for battery instruments,
#                     and added -c cmd-line arg for all applicable dataloggers.
######################################################

#-------------- Platform Deployment Info Section -------------
Deploy.status    = Deployed #deployed, recovered, test, n/a
Deploy.id        = D0003
Deploy.sdate     = 2015/04/28
Deploy.edate     = 2015/10/15
Deploy.location  = Coastal Pioneer
Deploy.lat       =  40.1368  #40deg 08.207N
Deploy.lon       = -70.76978 #70deg 46.187W
Deploy.wdepth    = 133
Deploy.wc_radius = 125
Deploy.wc_zoom   = 15 # Default 15
Deploy.vessel    = R/V Atlantis
Deploy.cruise_id = 
Deploy.chief_sci = Al Plueddemann
Deploy.synopsis  = Deploy Pioneer Surface Mooring
#-----------------------------------------------------


#-------------- Platform Configuration Section -------------
Platform.id      = cp01cnsm
Platform.desc    = Coastal Pioneer EM Surface Mooring
Platform.type    = SM #surface_mooring, profiler_mooring, global_mooring, inshore_mooring
Platform.drawing = 3604-40001_CP01CNSM.png
#
Platform.buoy.hotel_cfg  = cpm1,dcl11,dcl12,gps,fb250,irid_isu,irid_sbd,wifi,freewave,ethsw,dsl,rda6,rda7
Platform.buoy.inst_cfg   = rte,mopak,hyd1,hyd2,metbk1,methtr1,metbk2,methtr2,fdchp,pco2a,wavss
Platform.nsif.hotel_cfg  = cpm2,dcl26,dcl27,ethsw
Platform.nsif.inst_cfg   = acomm,ctdbp1,phsen1,dosta1,flort,optaa1,nutnr,spkir,velpt1
Platform.mfn.hotel_cfg   = cpm3,dcl35,dcl37,dsl
Platform.mfn.inst_cfg    = ctdbp2,pco2w,phsen2,presf,dosta2,optaa2,velpt2,adcp,zplsc
Platform.imm.inst_cfg    = # comma separated list of two_digit_modem_ID:inst_tag:dir_name with no spaces
Platform.wfp.inst_cfg    = 
#
Platform.cpm1.psc_type   = hipwr  # valid types: none, mpea, stdpwr, hipwr
Platform.cpm2.psc_type   = none   # valid types: none, mpea, stdpwr, hipwr
Platform.cpm3.psc_type   = mpea   # valid types: none, mpea, stdpwr, hipwr
#
Platform.cpm1.ldet_cfg  = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.cpm2.ldet_cfg  = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.cpm3.ldet_cfg  = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl11.ldet_cfg = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl12.ldet_cfg = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl26.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl27.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl35.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl37.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
#-----------------------------------------------------


#-------------- Power Configuration Section -------------
#Available power cfg options: solar, wind, fuelcell, battery
Platform.power_cfg = solar,wind,battery,fuelcell
PowerCfg.SolarPwr.mfg = kyocera
PowerCfg.WindPwr.mfg  = superwind
PowerCfg.BattPwr.mfg  = lifeline
PowerCfg.FuelCell.mfg =
#-----------------------------------------------------


#-------------- Telem Configuration Section -------------
Shore.server_ip   = 199.92.168.170  # fb250  Losos ooi-cgpss1.whoi.net
Shore.irid.phone1 = 00881600005259  # rudics Joubeh Losos ooi-cgpss1.whoi.net
Shore.irid.phone2 = 0015082894941   # analog Losos ooi-cgpss1.whoi.net
Shore.irid.phone3 = 0015082894942   # analog Losos ooi-cgpss1.whoi.net
Shore.irid.mtip   = 12.47.179.51
Shore.irid.mtports = 2256-2260

Telem.fb1.mfg   = Thrane & Thrane
Telem.fb1.model = fb250
Telem.fb1.ip    = 216.86.245.120
Telem.fb1.antenna.bootloader.ver = 1.0?
Telem.fb1.antenna.software.ver = 1.30?
Telem.fb1.antenna.serial.number =  80670063
Telem.fb1.sim   = 898709912414175320
Telem.fb1.imei  = 351624020264117
Telem.fb1.unit.serial.number = 12436066

Telem.fb2.mfg   = Thrane & Thrane
Telem.fb2.model = fb250
Telem.fb2.ip    = 216.86.246.75
Telem.fb2.antenna.bootloader.ver  = 1.0?
Telem.fb2.antenna.software.ver  = 1.10?
Telem.fb2.antenna.serial.number  = 80620262
Telem.fb2.sim   = 898709912414175321
Telem.fb2.imei  = 351624020240257
Telem.fb2.unit.serial.number = 80620262

Telem.isu1.mfg   = Motorola
Telem.isu1.model = 9522B
Telem.isu1.sim   = 8988169234000089706
Telem.isu1.phone = 8816-234-66898
Telem.isu1.imei  = 300025010036340

Telem.isu2.mfg   = Motorola
Telem.isu2.model = 9522B
Telem.isu2.sim   = 8988169234000089987
Telem.isu2.phone = 8816-234-66897
Telem.isu2.imei  = 300025010039340

Telem.sbd1.mfg   = NAL
Telem.sbd1.model = 9602-N
Telem.sbd1.imei  = 300234060459280


Telem.sbd2.mfg   = NAL
Telem.sbd2.model = 9602-N
Telem.sbd2.imei  = 300234060356360

Telem.fw.trans   =

Telem.xeos1.imei = 300234061722760 # MELO Tower 1
Telem.xeos2.imei = 300234061723430 # MELO Tower 2
Telem.xeos3.imei = 300234060529280 # Buoy
Telem.xeos4.imei = 300234060015550 # MFN 1
Telem.xeos5.imei = 300234060424880 # MFN 2
#-----------------------------------------------------v


#----------------- DCL Port Config  Section ---------------
dcl11.port1.inst            = mopak           # Instrument name - OOI Tag
dcl11.port1.inst.mfg        = Microstrain     # Manufacturer
dcl11.port1.inst.model      = 3dmgx3-gx3-25   # Model
dcl11.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port1.pwr.cfg         = 1 900 0 0 1     # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port1.pwr.sched       = 1:0-23:0:20     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port1.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port1.dlog.driver     = bin/dl_3dmgx -a 3 -c Cfg/InstCfg/3dmgx3.default.cfg

dcl11.port2.inst            = hyd1            # Instrument name - OOI Tag
dcl11.port2.inst.mfg        = RKI Inst DGH    # Manufacturer
dcl11.port2.inst.model      = M2A D5200/H     # Model
dcl11.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port2.pwr.cfg         = 2 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port2.pwr.sched       = 1:0-23:0:5      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port2.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port2.dlog.driver     = bin/dl_dgh -c Cfg/InstCfg/hyd.default.cfg

dcl11.port3.inst            = rte             # Instrument name - OOI Tag
dcl11.port3.inst.mfg        = Radar Reflector # Manufacturer
dcl11.port3.inst.model      = Radar Reflector # Model
dcl11.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port3.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port3.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port3.dlog.driver     = 

dcl11.port6.inst            = metbk1          # Instrument name - OOI Tag
dcl11.port6.inst.mfg        = ASIMET          # Manufacturer
dcl11.port6.inst.model      = metbk           # Model
dcl11.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port6.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port6.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port6.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port6.dlog.driver     = bin/dl_metbk -c Cfg/InstCfg/metbk.default.cfg

dcl11.port7.inst            = methtr1         # Instrument name - OOI Tag
dcl11.port7.inst.mfg        = ASIMET          # Manufacturer
dcl11.port7.inst.model      = methtr          # Model
dcl11.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port7.pwr.cfg         = 2 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port7.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port7.pwroff.delay    = 2               # seconds for dlog to gracefully stop before power-off
dcl11.port7.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port7.dlog.driver     = 

dcl12.port3.inst            = hyd2            # Instrument name - OOI Tag
dcl12.port3.inst.mfg        = RKI Inst DGH    # Manufacturer
dcl12.port3.inst.model      = M2A D5200/H     # Model
dcl12.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port3.pwr.cfg         = 2 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port3.pwr.sched       = 1:0-23:0:5      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port3.dlog.driver     = bin/dl_dgh -c Cfg/InstCfg/hyd.default.cfg

dcl12.port4.inst            = pco2a           # Instrument name - OOI Tag
dcl12.port4.inst.mfg        = Pro Oceanus     # Manufacturer
dcl12.port4.inst.model      = CO2-Pro         # Model
dcl12.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port4.pwr.sched       = 0:0-23:0:40     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port4.serial_cfg      = 19200 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port4.dlog.driver     = bin/dl_pco2a -c Cfg/InstCfg/pco2a.default.cfg

dcl12.port5.inst            = wavss           # Instrument name - OOI Tag
dcl12.port5.inst.mfg        = Triaxys         # Manufacturer
dcl12.port5.inst.model      = Wave Sensor 2343# Model
dcl12.port5.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port5.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port5.pwr.sched       = 1:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port5.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port5.dlog.driver     = bin/dl_wave -c Cfg/InstCfg/wavss.default.cfg

dcl12.port6.inst            = metbk2          # Instrument name - OOI Tag
dcl12.port6.inst.mfg        = ASIMET          # Manufacturer
dcl12.port6.inst.model      = metbk           # Model
dcl12.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port6.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port6.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port6.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port6.dlog.driver     = bin/dl_metbk -c Cfg/InstCfg/metbk.default.cfg

dcl12.port7.inst            = methtr2         # Instrument name - OOI Tag
dcl12.port7.inst.mfg        = ASIMET          # Manufacturer
dcl12.port7.inst.model      = methtr          # Model
dcl12.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port7.pwr.cfg         = 2 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port7.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port7.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port7.dlog.driver     = 

dcl12.port8.inst            = fdchp           # Instrument name - OOI Tag
dcl12.port8.inst.mfg        = WHOI            # Manufacturer
dcl12.port8.inst.model      = HP-DCFS         # Model
dcl12.port8.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port8.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port8.pwr.sched       = 1:0-23:58:25    # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port8.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port8.dlog.driver     = bin/dl_fdchp -c Cfg/InstCfg/fdchp.default.cfg
dcl12.port8.eth_cfg         = 192.168.1.100:4100  # IP address[:port] - note overrides serial_cfg

dcl26.port4.inst            = velpt1          # Instrument name - OOI Tag
dcl26.port4.inst.mfg        = Nortek          # Manufacturer
dcl26.port4.inst.model      = Aquadopp        # Model
dcl26.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port4.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port4.pwr.isched      = 1:15:7          # 0-disable, 1-enable : interval(min) : duration(min)
dcl26.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port4.dlog.driver     = bin/dl_nortek -a 1 -v 40 -O 10

dcl26.port5.inst            = acomm           # Instrument name - OOI Tag
dcl26.port5.inst.mfg        = Manuf           # Manufacturer
dcl26.port5.inst.model      = Model           # Model
dcl26.port5.pwr             = 0               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port5.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port5.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port5.pwroff.delay    = 2               # seconds for dlog to gracefully stop before power-off
dcl26.port5.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port5.dlog.driver     = 

dcl26.port6.inst            = phsen1          # Instrument name - OOI Tag
dcl26.port6.inst.mfg        = Sunburst        # Manufacturer
dcl26.port6.inst.model      = pH Sensor       # Model
dcl26.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port6.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port6.pwr.sched       = 1:0-23:0:14     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port6.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port6.dlog.driver     = bin/dl_sunburst -c Cfg/InstCfg/phsen.def_bat.cfg

dcl26.port7.inst            = nutnr           # Instrument name - OOI Tag
dcl26.port7.inst.mfg        = Satlantic       # Manufacturer
dcl26.port7.inst.model      = ISUS Nitrate    # Model
dcl26.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port7.pwr.cfg         = 0 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port7.pwr.sched       = 1:0-23:0:3      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port7.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port7.serial_cfg      = 38400 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port7.dlog.driver     = bin/dl_nutnr -c Cfg/InstCfg/nutnr.def_bat.cfg

dcl26.port8.inst            = spkir           # Instrument name - OOI Tag
dcl26.port8.inst.mfg        = Satlantic       # Manufacturer
dcl26.port8.inst.model      = OCR-507         # Model
dcl26.port8.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port8.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port8.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl26.port8.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port8.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port8.dlog.driver     = bin/dl_spkir -c Cfg/InstCfg/spkir.default.cfg -O 10 -p 150000

dcl27.port1.inst            = optaa1          # Instrument name - OOI Tag
dcl27.port1.inst.mfg        = Wetlabs         # Manufacturer
dcl27.port1.inst.model      = ACS Spectrophotometer  # Model
dcl27.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port1.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port1.pwr.sched       = 1:0-23:0:1      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl27.port1.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl27.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port1.dlog.driver     = bin/dl_optaa

dcl27.port2.inst            = flort           # Instrument name - OOI Tag
dcl27.port2.inst.mfg        = Wetlabs         # Manufacturer
dcl27.port2.inst.model      = ECO 3           # Model
dcl27.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port2.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port2.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port2.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port2.serial_cfg      = 19200 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port2.dlog.driver     = bin/dl_ecotrip -c Cfg/InstCfg/flort.default.cfg

dcl27.port3.inst            = ctdbp1          # Instrument name - OOI Tag
dcl27.port3.inst.mfg        = Seabird         # Manufacturer
dcl27.port3.inst.model      = SBE-16 Plus     # Model
dcl27.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port3.pwr.cfg         = 0 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port3.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port3.dlog.driver     = bin/dl_sbe16 -c Cfg/InstCfg/ctdbp.def_bat.cfg

dcl27.port4.inst            = dosta1          # Instrument name - OOI Tag
dcl27.port4.inst.mfg        = Aanderra        # Manufacturer
dcl27.port4.inst.model      = Oxygen Optode 3975  # Model
dcl27.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port4.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port4.dlog.driver     = bin/dl_dosta -c Cfg/InstCfg/dosta.default.cfg

dcl35.port1.inst            = adcp            # Instrument name - OOI Tag
dcl35.port1.inst.mfg        = Teledyne RDI    # Manufacturer
dcl35.port1.inst.model      = RDI Workhorse   # Model
dcl35.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port1.pwr.cfg         = 0 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port1.pwr.sched       = 1:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port1.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port1.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port1.dlog.driver     = bin/dl_adcp -c Cfg/InstCfg/adcp_cnsm_bat.cfg

dcl35.port2.inst            = presf           # Instrument name - OOI Tag
dcl35.port2.inst.mfg        = Seabird         # Manufacturer
dcl35.port2.inst.model      = SBE-26          # Model
dcl35.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port2.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port2.pwr.sched       = 1:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port2.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port2.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port2.dlog.driver     = bin/dl_sbe26 -c Cfg/InstCfg/presf.def_bat.cfg

dcl35.port4.inst            = velpt2          # Instrument name - OOI Tag
dcl35.port4.inst.mfg        = Nortek          # Manufacturer
dcl35.port4.inst.model      = Aquadopp        # Model
dcl35.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port4.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port4.pwr.sched       = 1:0-23:0:7      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port4.dlog.driver     = bin/dl_nortek -a 1 -v 40 -O 10

dcl35.port5.inst            = pco2w           # Instrument name - OOI Tag
dcl35.port5.inst.mfg        = Sunburst        # Manufacturer
dcl35.port5.inst.model      = SAMI-PCO2       # Model
dcl35.port5.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port5.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port5.pwr.sched       = 1:0,2,4,6,8,10,12,14,16,18,20,22:0:14      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port5.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port5.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port5.dlog.driver     = bin/dl_sunburst -c Cfg/InstCfg/pco2w.def_bat.cfg

dcl35.port6.inst            = phsen2          # Instrument name - OOI Tag
dcl35.port6.inst.mfg        = Sunburst        # Manufacturer
dcl35.port6.inst.model      = pH Sensor       # Model
dcl35.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port6.pwr.cfg         = 0 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port6.pwr.sched       = 1:0-23:0:14     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port6.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port6.dlog.driver     = bin/dl_sunburst -c Cfg/InstCfg/phsen.def_bat.cfg

dcl37.port1.inst            = optaa2          # Instrument name - OOI Tag
dcl37.port1.inst.mfg        = Wetlabs         # Manufacturer
dcl37.port1.inst.model      = ACS Spectrophotometer # Model
dcl37.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port1.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port1.pwr.sched       = 1:0-23:0:1      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port1.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl37.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port1.dlog.driver     = bin/dl_optaa

dcl37.port3.inst            = ctdbp2          # Instrument name - OOI Tag
dcl37.port3.inst.mfg        = Seabird         # Manufacturer
dcl37.port3.inst.model      = SBE-16 Plus     # Model
dcl37.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port3.pwr.cfg         = 0 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port3.pwr.sched       = 1:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl37.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port3.dlog.driver     = bin/dl_sbe16 -c Cfg/InstCfg/ctdbp.def_bat.cfg

dcl37.port4.inst            = dosta2          # Instrument name - OOI Tag
dcl37.port4.inst.mfg        = Aanderra        # Manufacturer
dcl37.port4.inst.model      = Oxygen Optode 3975 # Model
dcl37.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port4.pwr.sched       = 1:0-23:0:3      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl37.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port4.dlog.driver     = bin/dl_dosta -c Cfg/InstCfg/dosta.default.cfg

dcl37.port7.inst            = zplsc           # Instrument name - OOI Tag
dcl37.port7.inst.mfg        = ASL Environmental Science # Manufacturer
dcl37.port7.inst.model      = AZFP 38/125/200/455 kHz   # Model
dcl37.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port7.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port7.pwr.sched       = 0:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port7.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl37.port7.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port7.dlog.driver     = bin/dl_zplsc -c Cfg/InstCfg/zplsc.default.cfg 
#--------------------------------------------------------
