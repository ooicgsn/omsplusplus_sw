 ######################################################
# OOI/CGSN Buoy Platform configuration file
# Format: Name = value
#
# Comments begin with #
#
# History:
#    Date      Who    Description
# ----------   ---    ----------------------------------------
# 08/30/2012   SL     Create
# 03/27/2013   SL/SW  NUTNR and SPKIR on chan 7/8 have been moved 
#                     from DCL 27 to DCL 26
# 09/11/2013   SL     Updated pwr.cfg (pwr lo/hi) for new DCL firmware
#                     Added port pwr and pwr.sched to DCL section
# 09/18/2013   SL/ME  Updated phsen/pco2w dlog driver to sunburst
# 09/30/2014   BJK    Updated for 2014 Deployment
# 10/03/2014   SL     Added Platform.cpm#.psc_type and ldet_cfg
# 10/07/2014   BJK    Updated FBB IMEI and IP Addresses
# 10/17/2014   ME     Updated for batteries and new configuration files
# 10/23/2014  MP/SL   Removed metbk2, methr2, fdchp, wavss. Updated acomm
#                     to clim 2500, sstart, and hipwr
# 10/28/2014   ME     New SPKIR and DOSTA timing  
# 10/31/2014   ME     Correcting dlog arguments MOPAK
# 11/03/2014   ME     OPTAA Softstart and pco2a
# 11/06/2014   SL/JL  Added irid mtip and mtports
# 11/06/2014   ME     Fixed poll_delay argument, pc02a on continuous
# 11/11/2014   BJK/SL Removed RTE Data Logger, setup methtr to be on
#                     and renamed metbk1 to metbk as only one on platform
# 11/13/2014   JML    Updated Xeos MELO/KILO IMEI numbers
# 11/14/2014   ME/SL  Made Dosta change, added acomm driver dl_spp_acom 
# 11/14/2014   ME     Changes to support battery drift and OPTAA
# 11/14/2014   JML    Corrected Xeos4 IMEI number
# 12/04/2014   ME     Updated VELPT to remove batteries.  Timing on CTDBP, ADCP, and PRESF
# 12/08/2014          to allow instrument to start 5 after hour.  2500 ma on PCO2A and ADCP.
# 12/16/2014   ME     Reduced PCO2A back to 1500 (pump) issue
# 03/02/2015   SL/DGI Added ZPLSC to dcl37 port 7
# 04/15/2015   ME/SL  Updated for new dataloggers - eliminated cmd-line args
#                     for non-binary instruments, set pcfg to 0 for battery instruments,
#                     and added -c cmd-line arg for all applicable dataloggers.
# 04/29/2015   BJK    Updated Freewave Numbers
# 09/18/2015   BJK    Updated numbers for D0003 deployment!
# 09/23/2015   AF     Updated FBB numbers
# 10/08/2015   SL     Restored all cmd-line args (backed-out changes made on 4/15/2015 to
#                     D0002 settings)
# 10/15/2015   SL     Added Platform.fc_present field
# 03/30/2016   SMP    Made generic to reduce chances of improper deployment configuration 
#                     by clearing some Deploy info (id, sdate, vessel, cruise_id, chief_sci).
# 03/20/2017   AF     Updated for P8
######################################################

#-------------- Platform Deployment Info Section -------------
Deploy.status    = test #deployed, recovered, test, n/a
Deploy.id        = D0006
Deploy.sdate     = yyyy/mm/dd
Deploy.edate     = yyyy/mm/dd
Deploy.location  = Coastal Pioneer Inshore
Deploy.lat       = 40.3596
Deploy.lon       = -70.8854
Deploy.wdepth    = 95
Deploy.wc_radius = 125
Deploy.wc_zoom   = 17 # Default 15
Deploy.vessel    = R/V TBD
Deploy.cruise_id = TBD
Deploy.chief_sci = TBD
Deploy.synopsis  = Pioneer Surface Mooring Deployment
#-----------------------------------------------------


#-------------- Platform Configuration Section -------------
Platform.id      = cp03issm
Platform.desc    = Coastal Pioneer Inshore Surface Mooring
Platform.type    = SM #surface_mooring, profiler_mooring, global_mooring, inshore_mooring
#Platform.drawing = 3604-20022_CP03ISSM.png
Platform.drawing = 3605-40010_CE01ISSM.png
#
Platform.buoy.hotel_cfg  = cpm1,dcl11,dcl12,gps,fb250,irid_isu,irid_sbd,wifi,freewave,ethsw,dsl,rda6,rda7
Platform.buoy.inst_cfg   = rte,mopak,hyd1,hyd2,metbk,methtr,pco2a
Platform.nsif.hotel_cfg  = cpm2,dcl26,dcl27,ethsw
Platform.nsif.inst_cfg   = acomm,ctdbp1,phsen1,dosta1,flort,optaa1,nutnr,spkir,velpt1
Platform.mfn.hotel_cfg   = cpm3,dcl35,dcl37,dsl
Platform.mfn.inst_cfg    = ctdbp2,pco2w,phsen2,presf,dosta2,optaa2,velpt2,adcp,zplsc
Platform.imm.inst_cfg    = # comma separated list of two_digit_modem_ID:inst_tag:dir_name with no spaces
Platform.wfp.inst_cfg    = 
#
Platform.cpm1.psc_type   = hipwr  # valid types: none, mpea, stdpwr, hipwr
Platform.cpm2.psc_type   = none   # valid types: none, mpea, stdpwr, hipwr
Platform.cpm3.psc_type   = mpea   # valid types: none, mpea, stdpwr, hipwr
#
Platform.fc_present      = 0   # 0-no fc,   1-have fuel cell
Platform.dock_present    = 0   # 0-no dock, 1-have auv dock
#
Platform.cpm1.ldet_cfg  = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.cpm2.ldet_cfg  = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.cpm3.ldet_cfg  = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl11.ldet_cfg = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl12.ldet_cfg = 0 300 0 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl26.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl27.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl35.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
Platform.dcl37.ldet_cfg = 1 300 1 300 # enable1 mv_limit1 enable2 mv_limit2
#-----------------------------------------------------


#-------------- Power Configuration Section -------------
#Available power cfg options: solar, wind, fuelcell, battery
Platform.power_cfg = solar,wind,battery,fuelcell
PowerCfg.SolarPwr.mfg = kyocera
PowerCfg.WindPwr.mfg  = superwind
PowerCfg.BattPwr.mfg  = lifeline
PowerCfg.FuelCell.mfg =
#-----------------------------------------------------


#-------------- Telem Configuration Section -------------
Shore.server_ip   = 199.92.168.170  # fb250  Losos ooi-cgpss1.whoi.net
Shore.irid.phone1 = 00881600005259  # rudics Joubeh Losos ooi-cgpss1.whoi.net
Shore.irid.phone2 = 0015082894941   # analog Losos ooi-cgpss1.whoi.net
Shore.irid.phone3 = 0015082894942   # analog Losos ooi-cgpss1.whoi.net
Shore.irid.mtip   = 12.47.179.51
Shore.irid.mtports = 2256-2260

Telem.fb1.mfg   = Thrane & Thrane
Telem.fb1.model = fb250
Telem.fb1.ip    = 216.86.248.126
Telem.fb1.antenna.bootloader.ver = 1.0
Telem.fb1.antenna.software.ver = 1.30
Telem.fb1.antenna serial.number = 80827488
Telem.fb1.sim   = 898709912414175318
Telem.fb1.imei  = 35162402-022628-0
Telem.fb1.unit.serial.number = 1449530

Telem.fb2.mfg   = Thrane & Thrane
Telem.fb2.model = fb250
Telem.fb2.ip    = 216.86.245.131
Telem.fb2.antenna.bootloader.ver  = 1.0
Telem.fb2.antenna.software.ver  = 1.10
Telem.fb2.antenna.serial.number  = 80827476
Telem.fb2.sim   = 898709912414175308
Telem.fb2.imei  = 35162402-026586-6
Telem.fb2.unit.serial.number = 12436458

Telem.isu1.mfg   = Motorola
Telem.isu1.model = 9522B
Telem.isu1.sim   = 8988169234000089714
Telem.isu1.phone = 8816-927-66901
Telem.isu1.imei  = 300025010031350

Telem.isu2.mfg   = Motorola
Telem.isu2.model = 9522B
Telem.isu2.sim   = 8988169224000838665
Telem.isu2.phone = 8816-927-66899
Telem.isu2.imei  = 300025010033360

Telem.sbd1.mfg   = NAL
Telem.sbd1.model = 9602
Telem.sbd1.imei  = 300234060352340

Telem.sbd2.mfg   = NAL
Telem.sbd2.model = 9602
Telem.sbd2.imei  = 300234060354270

Telem.rfm1.trans = 860-2985
Telem.rfm2.trans = 862-4429

Telem.wifi.trans = 24:A4:3C:76:D5:61

Telem.xeos1.imei = 300234060347410 # MELO Tower 1
Telem.xeos2.imei = 300234060244230 # MELO Tower 2
Telem.xeos3.imei = 300234062646340 # Buoy
Telem.xeos4.imei = 300234062543980 # MFN 1
Telem.xeos5.imei = 300234062646310 # MFN 2
#-----------------------------------------------------


#----------------- DCL Port Config  Section ---------------
dcl11.port1.inst            = mopak           # Instrument name - OOI Tag
dcl11.port1.inst.mfg        = Microstrain     # Manufacturer
dcl11.port1.inst.model      = 3dmgx3-gx3-25   # Model
dcl11.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port1.pwr.cfg         = 1 900 0 0 1     # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port1.pwr.sched       = 1:0-23:0:20     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port1.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port1.dlog.driver     = bin/dl_3dmgx -a 3 -c Cfg/InstCfg/3dmgx3.default.cfg

dcl11.port2.inst            = hyd1            # Instrument name - OOI Tag
dcl11.port2.inst.mfg        = RKI Inst DGH    # Manufacturer
dcl11.port2.inst.model      = M2A D5200/H     # Model
dcl11.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port2.pwr.cfg         = 2 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port2.pwr.sched       = 1:0-23:0:5      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port2.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port2.dlog.driver     = bin/dl_dgh -a 3 -c Cfg/InstCfg/hyd.default.cfg -f 10 -O 5 -P 90

dcl11.port3.inst            = rte             # Instrument name - OOI Tag
dcl11.port3.inst.mfg        = Radar Reflector # Manufacturer
dcl11.port3.inst.model      = Radar Reflector # Model
dcl11.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port3.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port3.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port3.dlog.driver     = 

dcl11.port6.inst            = metbk           # Instrument name - OOI Tag
dcl11.port6.inst.mfg        = ASIMET          # Manufacturer
dcl11.port6.inst.model      = metbk           # Model
dcl11.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port6.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port6.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl11.port6.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port6.dlog.driver     = bin/dl_metbk -f 30 -v 40 -O 15 -P 1

dcl11.port7.inst            = methtr          # Instrument name - OOI Tag
dcl11.port7.inst.mfg        = ASIMET          # Manufacturer
dcl11.port7.inst.model      = methtr          # Model
dcl11.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl11.port7.pwr.cfg         = 2 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl11.port7.pwr.sched       = 0:0-23:0:30     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl11.port7.pwroff.delay    = 2               # seconds for dlog to gracefully stop before power-off
dcl11.port7.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl11.port7.dlog.driver     = 

dcl12.port3.inst            = hyd2            # Instrument name - OOI Tag
dcl12.port3.inst.mfg        = RKI Inst DGH    # Manufacturer
dcl12.port3.inst.model      = M2A D5200/H     # Model
dcl12.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port3.pwr.cfg         = 2 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port3.pwr.sched       = 1:0-23:0:5      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port3.dlog.driver     = bin/dl_dgh -a 3 -c Cfg/InstCfg/hyd.default.cfg -f 10 -O 5 -P 90

dcl12.port4.inst            = pco2a           # Instrument name - OOI Tag
dcl12.port4.inst.mfg        = Pro Oceanus     # Manufacturer
dcl12.port4.inst.model      = CO2-Pro         # Model
dcl12.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl12.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl12.port4.pwr.sched       = 1:0-23:25:55    # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl12.port4.serial_cfg      = 19200 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl12.port4.dlog.driver     = bin/dl_pco2a -c Cfg/InstCfg/pco2a.default.cfg -p 200000 -v 30
 
dcl26.port4.inst            = velpt1          # Instrument name - OOI Tag
dcl26.port4.inst.mfg        = Nortek          # Manufacturer
dcl26.port4.inst.model      = Aquadopp        # Model
dcl26.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port4.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port4.pwr.isched      = 1:15:7          # 0-disable, 1-enable : interval(min) : duration(min)
dcl26.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port4.dlog.driver     = bin/dl_nortek -b -a 1 -v 40 -O 10

dcl26.port5.inst            = acomm           # Instrument name - OOI Tag
dcl26.port5.inst.mfg        = Manuf           # Manufacturer
dcl26.port5.inst.model      = Model           # Model
dcl26.port5.pwr             = 0               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port5.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port5.pwr.sched       = 0:0,6,12,18:0:5 # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port5.pwroff.delay    = 2               # seconds for dlog to gracefully stop before power-off
dcl26.port5.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port5.dlog.driver     = bin/dl_spp_acom

dcl26.port6.inst            = phsen1          # Instrument name - OOI Tag
dcl26.port6.inst.mfg        = Sunburst        # Manufacturer
dcl26.port6.inst.model      = pH Sensor       # Model
dcl26.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port6.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port6.pwr.sched       = 1:0-23:58:16    # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port6.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port6.dlog.driver     = bin/dl_sunburst -a 2 -b

dcl26.port7.inst            = nutnr           # Instrument name - OOI Tag
dcl26.port7.inst.mfg        = Satlantic       # Manufacturer
dcl26.port7.inst.model      = ISUS Nitrate    # Model
dcl26.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port7.pwr.cfg         = 2 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port7.pwr.sched       = 1:0-23:59:5     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl26.port7.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port7.serial_cfg      = 38400 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port7.dlog.driver     = bin/dl_nutnr -b -c Cfg/InstCfg/nutnr.def_bat.cfg -v 40

dcl26.port8.inst            = spkir           # Instrument name - OOI Tag
dcl26.port8.inst.mfg        = Satlantic       # Manufacturer
dcl26.port8.inst.model      = OCR-507         # Model
dcl26.port8.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl26.port8.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl26.port8.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl26.port8.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl26.port8.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl26.port8.dlog.driver     = bin/dl_spkir -c Cfg/InstCfg/spkir.default.cfg -O 10 -p 150000

dcl27.port1.inst            = optaa1          # Instrument name - OOI Tag
dcl27.port1.inst.mfg        = Wetlabs         # Manufacturer
dcl27.port1.inst.model      = ACS Spectrophotometer  # Model
dcl27.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port1.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port1.pwr.sched       = 1:0-23:0:2      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl27.port1.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl27.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port1.dlog.driver     = bin/dl_optaa

dcl27.port2.inst            = flort           # Instrument name - OOI Tag
dcl27.port2.inst.mfg        = Wetlabs         # Manufacturer
dcl27.port2.inst.model      = ECO 3           # Model
dcl27.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port2.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port2.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port2.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port2.serial_cfg      = 19200 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port2.dlog.driver     = bin/dl_ecotrip -c Cfg/InstCfg/flort.default.cfg -O 10

dcl27.port3.inst            = ctdbp1          # Instrument name - OOI Tag
dcl27.port3.inst.mfg        = Seabird         # Manufacturer
dcl27.port3.inst.model      = SBE-16 Plus     # Model
dcl27.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port3.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port3.pwr.isched      = 0:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port3.dlog.driver     = bin/dl_sbe16 -b -a 1 -c Cfg/InstCfg/ctdbp.default.cfg

dcl27.port4.inst            = dosta1          # Instrument name - OOI Tag
dcl27.port4.inst.mfg        = Aanderra        # Manufacturer
dcl27.port4.inst.model      = Oxygen Optode 3975  # Model
dcl27.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl27.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl27.port4.pwr.isched      = 1:15:3          # 0-disable, 1-enable : interval(min) : duration(min)
dcl27.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl27.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl27.port4.dlog.driver     = bin/dl_dosta -c Cfg/InstCfg/dosta.default.cfg -f 5000000 -O 5 -v 60

dcl35.port1.inst            = adcp            # Instrument name - OOI Tag
dcl35.port1.inst.mfg        = Teledyne RDI    # Manufacturer
dcl35.port1.inst.model      = RDI Workhorse   # Model
dcl35.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port1.pwr.cfg         = 2 2500 0 0 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port1.pwr.sched       = 1:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port1.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port1.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port1.dlog.driver     = bin/dl_adcp -a 1 -b -c Cfg/InstCfg/adcp_issm_bat.cfg

dcl35.port2.inst            = presf           # Instrument name - OOI Tag
dcl35.port2.inst.mfg        = Seabird         # Manufacturer
dcl35.port2.inst.model      = SBE-26          # Model
dcl35.port2.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port2.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port2.pwr.sched       = 1:0-23:58:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port2.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port2.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port2.dlog.driver     = bin/dl_sbe26 -b -c Cfg/InstCfg/presf.def_bat.cfg

dcl35.port4.inst            = velpt2          # Instrument name - OOI Tag
dcl35.port4.inst.mfg        = Nortek          # Manufacturer
dcl35.port4.inst.model      = Aquadopp        # Model
dcl35.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port4.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port4.pwr.sched       = 1:0-23:0:7      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port4.dlog.driver     = bin/dl_nortek -b -a 1 -v 40 -O 10

dcl35.port5.inst            = pco2w           # Instrument name - OOI Tag
dcl35.port5.inst.mfg        = Sunburst        # Manufacturer
dcl35.port5.inst.model      = SAMI-PCO2       # Model
dcl35.port5.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port5.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port5.pwr.sched       = 1:0-23:58:16    # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port5.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port5.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port5.dlog.driver     = bin/dl_sunburst -a 1 -b

dcl35.port6.inst            = phsen2          # Instrument name - OOI Tag
dcl35.port6.inst.mfg        = Sunburst        # Manufacturer
dcl35.port6.inst.model      = pH Sensor       # Model
dcl35.port6.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl35.port6.pwr.cfg         = 1 1500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl35.port6.pwr.sched       = 1:0-23:58:16    # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl35.port6.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl35.port6.serial_cfg      = 57600 N 8 1 0   # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl35.port6.dlog.driver     = bin/dl_sunburst -a 2 -b

dcl37.port1.inst            = optaa2          # Instrument name - OOI Tag
dcl37.port1.inst.mfg        = Wetlabs         # Manufacturer
dcl37.port1.inst.model      = ACS Spectrophotometer # Model
dcl37.port1.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port1.pwr.cfg         = 1 2500 0 1 2    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port1.pwr.sched       = 1:0-23:0:2      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port1.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl37.port1.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port1.dlog.driver     = bin/dl_optaa

dcl37.port3.inst            = ctdbp2          # Instrument name - OOI Tag
dcl37.port3.inst.mfg        = Seabird         # Manufacturer
dcl37.port3.inst.model      = SBE-16 Plus     # Model
dcl37.port3.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port3.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port3.pwr.sched       = 1:0-23:58:5     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port3.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl37.port3.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port3.dlog.driver     = bin/dl_sbe16 -a 1 -b -c Cfg/InstCfg/ctdbp.def_bat.cfg

dcl37.port4.inst            = dosta2          # Instrument name - OOI Tag
dcl37.port4.inst.mfg        = Aanderra        # Manufacturer
dcl37.port4.inst.model      = Oxygen Optode 3975 # Model
dcl37.port4.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port4.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port4.pwr.sched       = 1:0-23:0:3      # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port4.pwroff.delay    = 15              # seconds for dlog to gracefully stop before power-off
dcl37.port4.serial_cfg      = 9600 N 8 1 0    # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port4.dlog.driver     = bin/dl_dosta -c Cfg/InstCfg/dosta.default.cfg -f 5000000 -O 5 -v 60

dcl37.port7.inst            = zplsc           # Instrument name - OOI Tag
dcl37.port7.inst.mfg        = ASL Environmental Science # Manufacturer
dcl37.port7.inst.model      = AZFP 38/125/200/455 kHz   # Model
dcl37.port7.pwr             = 1               # Initial Power State: 0-off, 1-on, t-toggle
dcl37.port7.pwr.cfg         = 1 2500 0 0 1    # voltage_sel(0-off,1-12v,2-24v), curr_limit(ma), protocol(0-rs232,1-rs232F,2-485,3-422),sstart,pwr(1-lo,2-hi)
dcl37.port7.pwr.sched       = 0:0-23:0:10     # 0-disable, 1-enable : hours(0-23) : offset(min) : duration(min)
dcl37.port7.pwroff.delay    = 0               # seconds for dlog to gracefully stop before power-off
dcl37.port7.serial_cfg      = 115200 N 8 1 0  # baud, parity, nbits, stopbits, flowctl 0-none,1-xonxoff,2-rtscts
dcl37.port7.dlog.driver     = bin/dl_zplsc -c Cfg/InstCfg/zplsc.default.cfg 
#--------------------------------------------------------
