import psycopg2 as psy
from datetime import datetime as dt
from webapp.settings import DATABASES
from warnings import warn

def load_plot_type(conn, type_ids, erddap_var_ids, erddap_id_like_string=None,
                   asset_code=None):
    """
    Loads the plots and the plot variables
    """
    cur = conn.cursor()
    try:
        if erddap_id_like_string is not None:
            cur.execute("""SELECT id, name, code, erddap_id, type_id
                           FROM asset
                           WHERE erddap_id LIKE %s
                             AND type_id IN %s
                             """,
                        (erddap_id_like_string, type_ids))
        elif asset_code is not None and type_ids is None:
            # code is case insensitive for now
            cur.execute("""SELECT id, name, code, erddap_id, type_id
                           FROM asset
                           WHERE lower(code) = lower(%s)
                             """,
                        (asset_code,))
        elif type_ids is not None:
            cur.execute("""SELECT id, name, code, erddap_id, type_id
                           FROM asset
                           WHERE type_id IN %s
                        """, (type_ids,))
        else:
            raise ValueError("Invalid data selection")

        assets = cur.fetchall()
        for asset in assets:
            # find all associated variables for this asset
            cur.execute("""SELECT id, name, units, erddap_id FROM variable WHERE asset_id = %s
                            AND erddap_id IN %s""", (asset[0], erddap_var_ids))
            asset_variables = cur.fetchall()
            for var in asset_variables:
                # insert the plots and plot variables

                # might be able to refactor this into using a single CTE
                # to do two inserts in one statement

                # BWA: hack to avoid dupes -- we will eventually want to handle
                # this otherwise
                cur.execute("""SELECT COUNT(*) FROM plot p
                                JOIN plot_variable pv ON pv.plot_id = p.id
                                WHERE pv.variable_id = %s AND p.asset_id = %s""",
                            (var[0], asset[0]))
                count = cur.fetchone()[0]
                if count == 0:
                    cur.execute("""INSERT INTO plot (created, modified, name, asset_id, user_id, is_global,
                                    plot_type_id, plot_class_id) VALUES (now(),
                                    now(), %s, %s, 1, TRUE, 1, 1)
                                    RETURNING id""", (var[1], asset[0]))
                    plot_id = cur.fetchone()[0]
                    cur.execute("""INSERT INTO plot_variable (plot_id, variable_id,
                                                            axis, reverse_axis)
                                    VALUES (%s, %s, 'y', FALSE)""",
                                    (plot_id, var[0]))
    except Exception as e:
        warn(str(e))
        conn.rollback()
    else:
        conn.commit()
    finally:
        cur.close()

def fetch_port_data(conn):
    cur = conn.cursor()

    try:
        cur.execute("""
                    SELECT id, name, code, erddap_id, type_id, parent_id
                    FROM asset WHERE type_id = 10
                    """)
        filt_rows = cur.fetchall()

        for row in filt_rows:

            port_variable_list = ['%s_current'%row[1].lower(),'%s_voltage'%row[1].lower()]
            # fetch parent asset
            cur.execute("SELECT id, name, code, erddap_id, type_id, parent_id FROM asset WHERE id = '%s'",(row[5],))

            parent_assets = cur.fetchall()
            for found in parent_assets:
                cur.execute("SELECT id, name, units,erddap_id FROM variable WHERE asset_id = %s",(found[0],))
                asset_variables = cur.fetchall()
                for var in asset_variables:
                    if var[3] in port_variable_list:
                        cur.execute("""SELECT COUNT(*) FROM plot p
                                       JOIN plot_variable pv ON pv.plot_id = p.id
                                       WHERE pv.variable_id = %s AND p.asset_id = %s""",
                                       (var[0], row[0]))
                        count = cur.fetchone()[0]
                        if count == 0:
                            cur.execute("""INSERT INTO plot (created, modified, name, asset_id, user_id, is_global,
                                        plot_type_id, plot_class_id)
                                        VALUES (now(), now(), %s, %s, 1, TRUE, 1, 1) RETURNING id""",
                                        (var[1], row[0]))
                            plot_id = cur.fetchone()[0]
                            cur.execute("""
                                        INSERT INTO plot_variable (plot_id, variable_id, axis, reverse_axis)
                                        VALUES ('%s', '%s', 'y', FALSE)
                                        """, (plot_id, var[0]))
    except Exception as e:
        warn(str(e))
        conn.rollback()
    else:
        conn.commit()
    finally:
        cur.close()

def fetch_buoy_nsif_mfn(conn):
    cur = conn.cursor()
    try:
        # BUOY, NSIF, MFN Plot Load
        #Define the variables desired for BUOY, NSIF, and MFN plots
        buoy_nsif_mfn_variable_list = ['humidity','ground_fault_main', 'leak_detect_voltage1', 'leak_detect_voltage2']
        #Adds the extension for Buoy NSIF, and MFN datasets
        buoy_nsif_mfn_erddap_id = "-001-SUPERV"
        cur.execute("SELECT id, name, code, erddap_id, type_id, parent_id FROM asset")
        rows = cur.fetchall()

        for row in rows:
            #This assumes asset_types 7,8, and 9 are Buoys NSIFs and MFNs
            if row[4] in [7,8,9]:
                cur.execute("SELECT code, deployment_id FROM asset WHERE id = %s"%row[5])
                parent = cur.fetchone()
                print(parent)
                # BWA (Only returns one result? not sure?  If so, why use
                # like
                cur.execute("""SELECT id, name, code, erddap_id, type_id,
                                      parent_id
                                     FROM asset
                                   WHERE erddap_id LIKE %s
                                            AND deployment_id = %s""",
                             ('%' + str(parent[0]).upper() + '-' +
                              str(row[2]).upper() + buoy_nsif_mfn_erddap_id, parent[1]))
                found_vals = cur.fetchall()
                for found in found_vals:
                    print(found[3], found[4])
                    cur.execute("SELECT id, name, units, erddap_id FROM variable WHERE asset_id = %s" % found[0])
                    #varz is used because the plural 'vars' is a reserved word
                    varz = cur.fetchall()
                    for var in varz:
                        if var[3] in buoy_nsif_mfn_variable_list:
                            cur.execute("""SELECT COUNT(*) FROM plot p
                                            JOIN plot_variable pv ON
                                                    pv.plot_id = p.id
                                            WHERE pv.variable_id = %s
                                                AND p.asset_id = %s
                                        """,
                                        (var[0], row[0]))
                            count = cur.fetchone()[0]
                            if count == 0:
                                cur.execute("INSERT INTO plot (created, modified, name, asset_id, user_id, is_global, plot_type_id, plot_class_id) VALUES (now(), now(), '%s', '%s', 1, TRUE, 1, 1) "%(var[1], row[0]))
                                cur.execute("SELECT LASTVAL()")
                                plot_id = cur.fetchone()[0]
                                cur.execute("INSERT INTO plot_variable (plot_id, variable_id, axis, reverse_axis) VALUES ('%s', '%s', 'y', FALSE)"%(plot_id, var[0]))
    except Exception as e:
        warn(str(e))
        conn.rollback()
    else:
        conn.commit()
    finally:
        cur.close()

## FIXME (BWA): doesn't operate the same as above, can't figure out why
#def fetch_buoy_nsif_mfn(conn):
#
#    #Define the variables desired for BUOY, NSIF, and MFN plots
#    buoy_nsif_mfn_variable_list = ('humidity', 'ground_fault_main',
#                                   'leak_detect_voltage1',
#                                   'leak_detect_voltage2')
#
#    cur = conn.cursor()
#    try:
#        #This assumes asset_types 7,8, and 9 are Buoys NSIFs and MFNs
#        cur.execute("""WITH t AS (SELECT id, '%' || code || '%-001-SUPERV' as filt_string FROM asset WHERE type_id IN (7, 8, 9))
#                       SELECT a.id, t.id ancestor_id FROM asset a, t WHERE a.erddap_id LIKE t.filt_string""")
#        assets = cur.fetchall()
#        for found in assets:
#            cur.execute("""
#                        SELECT id, name, units, erddap_id FROM variable WHERE
#                        asset_id = %s AND erddap_id IN %s
#                        """, (found[0], buoy_nsif_mfn_variable_list))
#            asset_vars = cur.fetchall()
#            for var in asset_vars:
#                # note that the asset plot saved corresponds to the
#                # NSIF/BUOY/MFN variables
#                cur.execute("""INSERT INTO plot (created, modified, name,
#                                                asset_id, user_id, is_global,
#                                                plot_type_id, plot_class_id)
#                                VALUES (now(), now(), %s, %s, 1, TRUE,
#                                        1, 1) RETURNING id""",
#                            (var[1], found[1]))
#                plot_id = cur.fetchone()[0]
#                print(plot_id, found[1])
#                cur.execute("INSERT INTO plot_variable (plot_id, variable_id, axis, reverse_axis) VALUES (%s, %s, 'y', FALSE)",
#                            (plot_id, var[0]))
#    except Exception as e:
#        warn(str(e))
#        conn.rollback()
#    else:
#        conn.commit()
#    finally:
#        cur.close()


def fetch_instrument_data(conn):
    #Need to add more as the datasets are careated in ERDDAP
    #Dict is used so that we can key-pair match instruments to their variable lists
    instrument_var_dict = {
      'metbk'  : ['air_temperature', 'sea_surface_temperature', 'eastward_wind_velocity','northward_wind_velocity'],
      'ctd'    : ['psu', 'temperature', 'pressure'],
      'dosta'  : ['estimated_oxygen_concentration', 'estimated_oxygen_concentration'],
      'wavss'  : ['significant_wave_height', 'significant_wave_period', 'mean_directional_spread'],
      'mopak'  : ['acceleration_x', 'acceleration_y', 'acceleration_z'],
      'gps'    : ['latitude', 'longitude'],
      'flort'  : ['estimated_chlorophyll', 'fluorometric_cdom'],
      'nutnr'  : ['nitrate_concentration'],
      'pco2a'  : ['pCO2', 'measured_co2'],
      'presf'  : ['depth', 'seawater_temperature', 'absolute_pressure'],
      'spkir'  : ['irradiance1', 'irradiance2', 'irradiance3', 'irradiance4', 'irradiance5', 'irradiance6', 'irradiance7'],
      'phsen1' : ['pH', 'voltage_battery'],
      'phsen2' : ['pH', 'voltage_battery'],
      'hyd1'   : ['hydrogen_concentration'],
      'hyd2'   : ['hydrogen_concentration'],
      'ctdpfk' : ['conductivity', 'temperature', 'depth'],
      'ctdbp1' : ['conductivity', 'temperature', 'depth'],
      'ctdbp2' : ['conductivity', 'temperature', 'depth'],
      'vel3dk' : ['beam_0_velocity', 'beam_1_velocity', 'beam_2_velocity'],
      'velpt1' : ['velocity_east', 'velocity_north', 'velocity_vertical'],
      'velpt2' : ['velocity_east', 'velocity_north', 'velocity_vertical']

    }
    cur = conn.cursor()

    try:
        cur.execute("""SELECT id, name, code, erddap_id, type_id, parent_id FROM asset WHERE type_id = 4 AND
                    erddap_id IS NOT NULL""")
        filt_rows = cur.fetchall()

        for row in filt_rows:
            #this assumes asset_type 4 is instrument.  also checks for instruments without ERDDAP data
            #if row[4] == 4 and row[3] != None:
            cur.execute("SELECT id, name, units, erddap_id FROM variable WHERE asset_id = %s",
                        (row[0],))
            asset_vars = cur.fetchall()
            for var in asset_vars:
                key = row[2].lower()
                if not key in instrument_var_dict:
                    print("Unhandled instrument name: " + key)
                    continue

                if var[3] in instrument_var_dict[key]:
                    cur.execute("""SELECT COUNT(*) FROM plot p
                                    JOIN plot_variable pv ON
                                            pv.plot_id = p.id
                                    WHERE pv.variable_id = %s
                                        AND p.asset_id = %s
                                """,
                                (var[0], row[0]))
                    count = cur.fetchone()[0]
                    if count == 0:
                        cur.execute("""
                                    INSERT INTO plot (created, modified, name,
                                                        asset_id, user_id,
                                                        is_global, plot_type_id,
                                                        plot_class_id)
                                        VALUES (now(), now(), %s, %s, 1, TRUE, 1, 1)
                                        RETURNING id""",
                                        (var[1], row[0]))
                        plot_id = cur.fetchone()[0]
                        cur.execute("""INSERT INTO plot_variable
                                            (plot_id, variable_id, axis, reverse_axis)
                                            VALUES ('%s', '%s', 'y', FALSE)""",
                                    (plot_id, var[0]))
    except Exception as e:
        warn(str(e))
        conn.rollback()
    else:
        conn.commit()
    finally:
        cur.close()

def load_plots():
    db_settings = DATABASES['default']

    conn = psy.connect(host=db_settings['HOST'],
                       port=db_settings['PORT'] or '5432',
                       dbname=db_settings['NAME'],
                       user=db_settings['USER'],
                       password=db_settings['PASSWORD'])

    cur = conn.cursor()

    cur.execute("SELECT id, name, code, erddap_id, type_id, parent_id FROM asset")
    rows = cur.fetchall()

    # power sys plots
    load_plot_type(conn, (1,), ('main_voltage', 'main_current',
                                'percent_charge'), "%-BUOY-001-PWRSYS")
    # fetch mfn/nsif/buoy plots
    fetch_buoy_nsif_mfn(conn)

    # DCL, CPM, and STC plot load -- voltage
    # kind of inefficient loop here
    load_plot_type(conn, (2,3,11), ('main_current', 'main_voltage',
                                    'ground_fault_main', 'leak_detect_voltage1',
                                    'leak_detect_voltage2'))
    # WFP sensors
    load_plot_type(conn, None, ('battery_voltage', 'motor_current', 'depth'),
                   asset_code='WFP')

    # port data
    fetch_port_data(conn)

    # other instruments
    fetch_instrument_data(conn)
