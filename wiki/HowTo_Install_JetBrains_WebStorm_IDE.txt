To install a free 30 day trial of JetBrains WebStorm IDE on Ubuntu go to:
https://www.jetbrains.com/webstorm/

Click the Download button for Free 30-day trial
Save the archive (Note - current version is 2016.3.2)
Move the Webstorm-*.tar.gz file to /opt

Detailed installation instructions are available by clicking the Quickstart Guide from the Download page.
Then select the Installing and Launching link on left
Note that their directions first say to unpack in your home dir, but then later recommend /opt

For example, to install:
sudo tar xf WebStorm-*.tar.gz -C /opt/

To run:
cd /opt/WebStorm-*/bin
./webstorm.sh

There are also directions for creating a desktop shortcut.

